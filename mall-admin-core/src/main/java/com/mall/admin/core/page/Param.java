package com.mall.admin.core.page;

import lombok.Data;

/**
 * @author jzz
 * @create 2022/2/23
 */
@Data
public class Param {
    private String name;
    private String value;
}
