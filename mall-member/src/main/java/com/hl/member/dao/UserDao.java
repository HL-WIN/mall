package com.hl.member.dao;

import com.hl.member.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
