package com.hl.member.dao;

import com.hl.member.entity.UserCollectShopEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 关注店铺表
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@Mapper
public interface UserCollectShopDao extends BaseMapper<UserCollectShopEntity> {
	
}
