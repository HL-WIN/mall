package com.hl.member.dao;

import com.hl.member.entity.UserAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 收货地址表
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@Mapper
public interface UserAddressDao extends BaseMapper<UserAddressEntity> {
	
}
