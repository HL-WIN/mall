package com.hl.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.member.entity.UserEntity;

import java.util.Map;

/**
 * 用户表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

