package com.hl.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.member.entity.UserLoginLogEntity;

import java.util.Map;

/**
 * 用户登陆记录表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
public interface UserLoginLogService extends IService<UserLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

