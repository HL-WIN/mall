package com.hl.member.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.member.entity.UserLevelEntity;
import com.hl.member.service.UserLevelService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 会员等级表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@RestController
@RequestMapping("member/userlevel")
public class UserLevelController {
    @Autowired
    private UserLevelService userLevelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = userLevelService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		UserLevelEntity userLevel = userLevelService.getById(id);
        HashMap<String, UserLevelEntity> hashMap=new HashMap<>();
        hashMap.put("userLevel", userLevel);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody UserLevelEntity userLevel){
		userLevelService.save(userLevel);

        return CommonResult.success("保存userLevel成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody UserLevelEntity userLevel){
		userLevelService.updateById(userLevel);

        return CommonResult.success("修改userLevel成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		userLevelService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除userLevel成功！");
    }

}
