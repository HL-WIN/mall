package com.hl.member.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.member.entity.UserAddressEntity;
import com.hl.member.service.UserAddressService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 收货地址表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@RestController
@RequestMapping("member/useraddress")
public class UserAddressController {
    @Autowired
    private UserAddressService userAddressService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = userAddressService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		UserAddressEntity userAddress = userAddressService.getById(id);
        HashMap<String, UserAddressEntity> hashMap=new HashMap<>();
        hashMap.put("userAddress", userAddress);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody UserAddressEntity userAddress){
		userAddressService.save(userAddress);

        return CommonResult.success("保存userAddress成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody UserAddressEntity userAddress){
		userAddressService.updateById(userAddress);

        return CommonResult.success("修改userAddress成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		userAddressService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除userAddress成功！");
    }

}
