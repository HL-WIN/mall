package com.hl.member.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.member.entity.UserStatisticsEntity;
import com.hl.member.service.UserStatisticsService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 统计信息表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@RestController
@RequestMapping("member/userstatistics")
public class UserStatisticsController {
    @Autowired
    private UserStatisticsService userStatisticsService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = userStatisticsService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		UserStatisticsEntity userStatistics = userStatisticsService.getById(id);
        HashMap<String, UserStatisticsEntity> hashMap=new HashMap<>();
        hashMap.put("userStatistics", userStatistics);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody UserStatisticsEntity userStatistics){
		userStatisticsService.save(userStatistics);

        return CommonResult.success("保存userStatistics成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody UserStatisticsEntity userStatistics){
		userStatisticsService.updateById(userStatistics);

        return CommonResult.success("修改userStatistics成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		userStatisticsService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除userStatistics成功！");
    }

}
