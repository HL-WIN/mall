package com.hl.member.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.member.entity.UserLoginLogEntity;
import com.hl.member.service.UserLoginLogService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 用户登陆记录表
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-13 22:04:44
 */
@RestController
@RequestMapping("member/userloginlog")
public class UserLoginLogController {
    @Autowired
    private UserLoginLogService userLoginLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = userLoginLogService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		UserLoginLogEntity userLoginLog = userLoginLogService.getById(id);
        HashMap<String, UserLoginLogEntity> hashMap=new HashMap<>();
        hashMap.put("userLoginLog", userLoginLog);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody UserLoginLogEntity userLoginLog){
		userLoginLogService.save(userLoginLog);

        return CommonResult.success("保存userLoginLog成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody UserLoginLogEntity userLoginLog){
		userLoginLogService.updateById(userLoginLog);

        return CommonResult.success("修改userLoginLog成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		userLoginLogService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除userLoginLog成功！");
    }

}
