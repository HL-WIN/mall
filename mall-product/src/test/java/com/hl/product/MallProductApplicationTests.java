package com.hl.product;


import com.hl.product.dao.AttributeGroupDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.UUID;
@RunWith(SpringRunner.class)
@SpringBootTest
public class MallProductApplicationTests {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedissonClient redissonClient;
    @Resource
    AttributeGroupDao attributeGroupDao;
    @Test
    public void testRedis()
    {
        ValueOperations<String, String> opsForValue = stringRedisTemplate.opsForValue();
        opsForValue.set("hello","world_"+ UUID.randomUUID().toString());
        System.out.println(opsForValue.get("hello"));
    }
    @Test
    public void test()
    {
        RLock testLock = redissonClient.getLock("testLock");
        testLock.lock();//锁可以自动续期
        try
        {
            Thread.sleep(30000);
        }catch (Exception e)
        {

        }finally {
            testLock.unlock();
        }
        System.out.println("####");
    }
    @Test
    public void test2()
    {
        System.out.println(attributeGroupDao.getDetailSpecsBySpuId(7L,225L));
    }

}
