package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 品牌分类关联
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("category_brand_relation")
public class CategoryBrandRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * 品牌id
	 */
	@NotNull(message = "新增必须指定brandId",groups = {AddGroup.class})
	private Long brandId;
	/**
	 * 分类id
	 */
	@NotNull(message = "新增必须指定categoryId",groups = {AddGroup.class})
	private Long categoryId;
	/**
	 * 品牌名称
	 */
//	@NotNull(message = "新增必须指定brandName",groups = {AddGroup.class})
	private String brandName;
	/**
	 * 分类名称
	 */
//	@NotNull(message = "新增必须指定categoryName",groups = {AddGroup.class})
	private String categoryName;

}
