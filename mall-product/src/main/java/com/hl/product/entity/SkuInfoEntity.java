package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * sku信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("product_sku_info")
public class SkuInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * skuId
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * spuId
	 */
	@NotNull(message = "新增必须指定spuId",groups = {AddGroup.class})
	private Long spuId;
	/**
	 * sku名称
	 */
	@NotNull(message = "新增必须指定skuName",groups = {AddGroup.class})
	private String skuName;
	/**
	 * 所属分类id
	 */
	@NotNull(message = "新增必须指定catagoryId",groups = {AddGroup.class})
	private Long catagoryId;
	/**
	 * 品牌id
	 */
	@NotNull(message = "新增必须指定brandId",groups = {AddGroup.class})
	private Long brandId;
	/**
	 * 默认图片
	 */
	private String defaultImage;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 副标题
	 */
	private String subtitle;
	/**
	 * 价格
	 */
	private BigDecimal price;
	/**
	 * 重量（克）
	 */
	private BigDecimal weight;

}
