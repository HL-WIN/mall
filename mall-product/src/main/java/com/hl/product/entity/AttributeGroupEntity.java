package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 属性分组
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("product_attribute_group")
public class AttributeGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 分组id
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * 组名
	 */
	@NotBlank(message = "组名必须提交",groups = {AddGroup.class,UpdateGroup.class})
	private String groupName;
	/**
	 * 排序
	 */
	@NotNull(groups={AddGroup.class})
	@Min(value = 0,message = "排序必须大于等于0",groups={AddGroup.class,UpdateGroup.class})
	private Integer sort;
	/**
	 * 组图标
	 */
	@URL(message = "icon必须是一个合法的url地址",groups={AddGroup.class,UpdateGroup.class})
	private String icon;
	/**
	 * 所属分类id
	 */
	@NotNull(message = "所属分类Id必须提交",groups = {AddGroup.class})
	private Long categoryId;
	/**
	 * 备注
	 */
	private String remarks;

	@TableField(exist = false)
	private Long [] categoryPath;

}
