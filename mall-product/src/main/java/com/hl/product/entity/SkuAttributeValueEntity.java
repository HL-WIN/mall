package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * sku销售属性&值
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("sku_attribute_value")
public class SkuAttributeValueEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * sku_id
	 */
	@NotNull(message = "新增必须指定skuId",groups = {AddGroup.class})
	private Long skuId;
	/**
	 * attr_id
	 */
	@NotNull(message = "新增必须指定attributeId",groups = {AddGroup.class})
	private Long attributeId;
	/**
	 * 销售属性名
	 */
	private String attributeName;
	/**
	 * 销售属性值
	 */
	private String attributeValue;
	/**
	 * 顺序
	 */
	private Integer sort;

}
