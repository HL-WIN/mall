package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * spu信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("product_spu_info")
public class SpuInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * spuid
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * 商品名称
	 */
	private String name;
	/**
	 * 所属分类id
	 */
	@NotNull(message = "新增必须指定categoryId",groups = {AddGroup.class})
	private Long categoryId;
	/**
	 * 品牌id
	 */
	@NotNull(message = "新增必须指定brandId",groups = {AddGroup.class})
	private Long brandId;
	/**
	 * 上架状态[0 - 下架，1 - 上架]
	 */
	private Integer shelfStatus;
	/**
	 * 商品介绍
	 */
	private String decript;


	private String image;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
