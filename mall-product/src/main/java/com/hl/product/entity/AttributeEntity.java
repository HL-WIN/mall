package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 商品属性
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Data
@TableName("product_attribute")
public class AttributeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 属性id
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * 属性名
	 */
	@NotBlank(message = "属性名必须提交",groups = {AddGroup.class})
	private String attributeName;
	/**
	 * 是否需要检索[0-不需要，1-需要]
	 */
	private Integer searchType;
	/**
	 * 属性图标
	 */
	@URL(message = "icon必须是一个合法的url地址",groups={AddGroup.class,UpdateGroup.class})
	private String attributeIcon;
	/**
	 * 可选值列表[用逗号分隔]
	 */
	private String attributeValue;
	/**
	 * 属性类型[0-销售属性，1-基本属性，2-既是销售属性又是基本属性]
	 */
	@NotNull(message = "属性类型必须提交",groups = {AddGroup.class})
	private Integer attributeType;
	/**
	 * 启用状态[0 - 禁用，1 - 启用]
	 */
	private Long enableStatus;
	/**
	 * 快速展示【是否展示在介绍上；0-否 1-是】
	 */
	private Integer quickShow;
	/**
	 * 所属分类
	 */
	@NotNull(message = "所属分类Id必须提交",groups = {AddGroup.class})
	private Long categoryId;
	/**
	 * 规格分组id
	 */
	@NotNull(message = "所属分类Id必须提交",groups = {AddGroup.class})
	private Long groupId;

}
