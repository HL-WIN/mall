package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 商品三级分类
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:54
 */
@Data
@TableName("product_category")
public class CategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 分类id
	 */
	@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
	@TableId
	private Long id;
	/**
	 * 分类名称
	 */
	@NotBlank(message = "品牌名必须提交",groups = {AddGroup.class,UpdateGroup.class})
	private String categoryName;
	/**
	 * 父分类id
	 */
	@NotNull(message = "父分类必须提交",groups = {AddGroup.class})
	private Long parentId;
	/**
	 * 是否显示[0-不显示，1显示]
	 */
	@TableLogic(value = "1",delval = "0")//代表逻辑删除字段
	private Integer showStatus;
	/**
	 * 排序
	 */
	@NotNull(groups={AddGroup.class},message = "新增sort不能为空")
	@Min(value = 0,message = "排序必须大于等于0",groups={AddGroup.class,UpdateGroup.class})
	private Integer sort;
	/**
	 * 图标地址
	 */
	@URL(message = "icon必须是一个合法的url地址",groups={AddGroup.class,UpdateGroup.class})
	private String icon;
	/**
	 * 计量单位
	 */
	@NotBlank(groups = {AddGroup.class},message = "计量单位不能为空")
	private String unit;

}
