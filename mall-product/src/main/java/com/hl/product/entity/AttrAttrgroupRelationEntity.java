package com.hl.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 属性&属性分组关联
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-22 16:31:57
 */
@Data
@TableName("attr_attrgroup_relation")
public class AttrAttrgroupRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
		@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
@TableId
	private Long id;
	/**
	 * 属性id
	 */
	private Long attributeId;
	/**
	 * 属性分组id
	 */
	private Long attributeGroupId;
	/**
	 * 属性组内排序
	 */
	private Integer sort;

}
