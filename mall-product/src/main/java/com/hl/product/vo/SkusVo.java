package com.hl.product.vo;

import com.mall.commons.valid.AddGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/18:05
 */
@Data
public class SkusVo {
    @NotNull(message = "属性必须提交",groups = {AddGroup.class})
    private List<AttributeVo> attributeVos;
    @NotNull(message = "skuName必须提交",groups = {AddGroup.class})
    private String skuName;
    private BigDecimal price;
    private String title;
    private String subtitle;
    @NotNull(message = "sku图片必须提交",groups = {AddGroup.class})
    private List<ImagesVo> images;
//    private List<String> descar;
//    private int fullCount;
//    private BigDecimal discount;
//    private int countStatus;
//    private BigDecimal fullPrice;
//    private BigDecimal reducePrice;
//    private int priceStatus;
    private BigDecimal weight;
//    private List<MemberPriceVo> memberPrice;
}
