package com.hl.product.vo;

import com.hl.product.entity.SkuImagesEntity;
import com.hl.product.entity.SkuInfoEntity;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/03/07/16:26
 */
@Data
public class ProductDetailVo {
    /**
     *  sku基本信息
     */
    SkuInfoEntity skuInfo;
    List<SkuImagesEntity> skuImages;

    /**
     * 销售属性
     */
    List<SaleAttributeVo> saleAttributeVos;

    /**
     * 规格参数
     */
    List<DetailSpecsVo> productDetailGroupVos;
}
