package com.hl.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/15/15:37
 */
@Data
public class BoundsVo {
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
