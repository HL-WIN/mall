package com.hl.product.vo;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/18:01
 */
@Data
public class ProductVo {
    @NotBlank(message = "商品名必须提交",groups = {AddGroup.class, UpdateGroup.class})
    private String name;
//    private String spuDescription;
    @NotNull(message = "catagoryId必须提交",groups = {AddGroup.class})
    private Long catagoryId;
    @NotNull(message = "brandId必须提交",groups = {AddGroup.class})
    private Long brandId;
//    @NotNull(message = "shelfStatus必须提交",groups = {AddGroup.class})
    private int shelfStatus;
    private String decript;
    @NotBlank(groups = {AddGroup.class},message = "icon必须要url")
    @URL(message = "image必须是一个合法的url地址",groups={AddGroup.class,UpdateGroup.class})
    private  String image;
//    private BoundsVo boundsVo;
    @NotNull(message = "基本属性必须提交",groups = {AddGroup.class})
    private List<BaseAttributesVo> baseAttrs;
    @NotNull(message = "sku必须提交",groups = {AddGroup.class})
    private List<SkusVo> skus;
}
