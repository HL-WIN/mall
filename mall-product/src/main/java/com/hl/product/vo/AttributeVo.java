package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/18:08
 */
@Data
public class AttributeVo {
    private Long attributeId;
    private String attributeName;
    private String attributeValue;
}
