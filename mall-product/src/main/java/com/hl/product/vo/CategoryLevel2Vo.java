package com.hl.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/03/04/13:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryLevel2Vo {
    /**
     * 一级父分类的id
     */
    private String catagoryLevel1Id;

    private String id;

    private String categoryName;

    /**
     * 三级子分类
     */
    private List<CategoryLevel3Vo> catagory3List;
}
