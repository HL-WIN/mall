package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/23/16:36
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
