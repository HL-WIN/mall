package com.hl.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/03/04/13:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryLevel3Vo {
    /**
     * 父分类、二级分类id
     */
    private String catagoryLevel2Id;

    private String id;

    private String categoryName;
}
