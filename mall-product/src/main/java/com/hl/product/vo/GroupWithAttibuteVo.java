package com.hl.product.vo;

import com.hl.product.entity.AttributeEntity;
import com.hl.product.entity.AttributeGroupEntity;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/23/15:06
 */
@Data
public class GroupWithAttibuteVo extends AttributeGroupEntity {

    private List<AttributeEntity> attributeEntities;
}
