package com.hl.product.vo;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/03/07/16:46
 */
@Data
public class DetailSpecsVo {
    private String groupName;

    private List<AttributeVo> attributeVos;
}
