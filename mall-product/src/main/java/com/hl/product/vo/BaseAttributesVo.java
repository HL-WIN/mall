package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/18:04
 */
@Data
public class BaseAttributesVo {
    private Long attributeId;
    private String attributeValues;
//    private int showDesc;
}
