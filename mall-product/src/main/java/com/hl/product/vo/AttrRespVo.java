package com.hl.product.vo;

import com.hl.product.entity.AttributeEntity;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/22/16:16
 */
@Data
public class AttrRespVo extends AttributeEntity {
    private String categoryName;

    private String groupName;

    private String attributeName;

    private Long[] categoryPath;
}
