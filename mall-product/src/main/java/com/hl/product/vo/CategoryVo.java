package com.hl.product.vo;

import com.hl.product.entity.CategoryEntity;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/18/17:12
 */
@Data
public class CategoryVo extends CategoryEntity {
    private List<CategoryVo> listChildrenCategory;
}
