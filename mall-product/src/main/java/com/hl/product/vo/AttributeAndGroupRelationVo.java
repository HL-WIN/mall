package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/23/15:03
 */
@Data
public class AttributeAndGroupRelationVo {
    private Long attributeId;
    private Long attributeGroupId;
}
