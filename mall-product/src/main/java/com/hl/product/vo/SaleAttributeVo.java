package com.hl.product.vo;

import lombok.Data;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/03/07/16:42
 */
@Data
public class SaleAttributeVo {
    private Long attributeId;
    private String atriibuteName;
    private List<String> atrributeValues;
}
