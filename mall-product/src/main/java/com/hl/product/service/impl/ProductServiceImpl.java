package com.hl.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hl.product.dao.SkuAttributeValueDao;
import com.hl.product.dao.SkuImagesDao;
import com.hl.product.entity.SkuImagesEntity;
import com.hl.product.entity.SkuInfoEntity;
import com.hl.product.entity.SpuInfoEntity;
import com.hl.product.service.*;
import com.hl.product.vo.DetailSpecsVo;
import com.hl.product.vo.ProductDetailVo;
import com.hl.product.vo.ProductVo;
import com.hl.product.vo.SaleAttributeVo;
import com.mall.commons.utils.Constant;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/17:57
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {
    
    @Resource
    SpuInfoService spuInfoService;
    @Resource
    SkuInfoService skuInfoService;
    @Resource
    SpuAttributeValueService spuAttributeValueService;
    @Resource
    SkuImagesDao skuImagesDao;
    @Resource
    AttributeGroupService attributeGroupService;
    @Resource
    SkuAttributeValueDao skuAttributeValueDao;

    @Override
    public Boolean productAdd(ProductVo productVo) {
        boolean result=false;
        boolean saveBaseInfo=false;
        boolean saveSku=false;
        //保存spu基本信息
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        spuInfoEntity.setShelfStatus(Constant.ProductStatusEnum.NEW_SPU.getCode());
        BeanUtils.copyProperties(productVo,spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        boolean save = spuInfoService.save(spuInfoEntity);

        Long spuId=spuInfoEntity.getId();
        //保存
        if (save)
        {
            saveBaseInfo=spuAttributeValueService.saveFromProductAdd(productVo.getBaseAttrs(),spuId);
        }
        //保存sku
        if (saveBaseInfo)
        {
            saveSku=skuInfoService.saveFromProductAdd(productVo.getSkus(),productVo.getBrandId(),productVo.getCatagoryId(),spuId);
        }
        if (saveSku)
        {
            result=true;
        }
        return result;
    }

    @Override
    public ProductDetailVo getProductDeatilBySkuId(Long skuId) {
        ProductDetailVo productDetailVo = new ProductDetailVo();
        //sku
        SkuInfoEntity skuInfo = skuInfoService.getById(skuId);
        productDetailVo.setSkuInfo(skuInfo);
        List<SkuImagesEntity> imagesEntities = skuImagesDao.selectList(new QueryWrapper<SkuImagesEntity>().eq("sku_id", skuId));
        if (imagesEntities.size()>0)
        {
            productDetailVo.setSkuImages(imagesEntities);
        }
        else {
            productDetailVo.setSkuImages(null);
        }
        //spu
        List<DetailSpecsVo> detailSpecsVos=attributeGroupService.getDetailSpecsBySpuId(skuInfo.getSpuId(),skuInfo.getCatagoryId());
        productDetailVo.setProductDetailGroupVos(detailSpecsVos);
        List<SaleAttributeVo> saleAttributeVos=skuAttributeValueDao.getSaleAttributeVoBySpuId(skuInfo.getSpuId());
        productDetailVo.setSaleAttributeVos(saleAttributeVos);
        return productDetailVo;
    }
}
