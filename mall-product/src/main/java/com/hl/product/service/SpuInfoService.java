package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    String productUp(Long spuId, String type);
}

