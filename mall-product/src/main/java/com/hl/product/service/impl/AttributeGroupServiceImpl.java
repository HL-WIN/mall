package com.hl.product.service.impl;

import com.hl.product.dao.AttrAttrgroupRelationDao;
import com.hl.product.entity.AttrAttrgroupRelationEntity;
import com.hl.product.entity.AttributeEntity;
import com.hl.product.service.AttributeService;
import com.hl.product.vo.AttributeAndGroupRelationVo;
import com.hl.product.vo.DetailSpecsVo;
import com.hl.product.vo.GroupWithAttibuteVo;
import com.mall.commons.utils.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.AttributeGroupDao;
import com.hl.product.entity.AttributeGroupEntity;
import com.hl.product.service.AttributeGroupService;

import javax.annotation.Resource;


@Service("attributeGroupService")
public class AttributeGroupServiceImpl extends ServiceImpl<AttributeGroupDao, AttributeGroupEntity> implements AttributeGroupService {

    @Resource
    AttrAttrgroupRelationDao attrAttrgroupRelationDao;
    @Resource
    AttributeService attributeService;
    @Resource
    AttributeGroupDao attributeGroupDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttributeGroupEntity> page = this.page(
                new Query<AttributeGroupEntity>().getPage(params),
                new QueryWrapper<AttributeGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCategoryId(Map<String, Object> params, Long categoryId) {
        String key = (String) params.get("key");
        QueryWrapper<AttributeGroupEntity> wrapper = new QueryWrapper<AttributeGroupEntity>();
        if (!StringUtils.isEmpty(key)) {
            wrapper.and((obj) -> {
                obj.eq("id", key).or().like("group_name", key);
            });
        }
        if (categoryId == 0) {
            IPage<AttributeGroupEntity> page = this.page(
                    new Query<AttributeGroupEntity>().getPage(params),
                    wrapper
            );
            return new PageUtils(page);
        } else {
            wrapper.eq("category_id", categoryId);
            IPage<AttributeGroupEntity> page = this.page(new Query<AttributeGroupEntity>().getPage(params), wrapper);
            return new PageUtils(page);
        }
    }

    @Override
    public void deleteRelation(AttributeAndGroupRelationVo[] attributeAndGroupRelationVos) {
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = Arrays.asList(attributeAndGroupRelationVos).stream().map(attrGroupRelationVo -> {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(attrGroupRelationVo, attrAttrgroupRelationEntity);
            return attrAttrgroupRelationEntity;
        }).collect(Collectors.toList());
        attrAttrgroupRelationDao.deleteBatchRelation(attrAttrgroupRelationEntities);
    }

    @Override
    public PageUtils getNoattrRelation(Map<String, Object> params, Long attrGroupId) {
        AttributeGroupEntity attrGroupEntity = this.baseMapper.selectById(attrGroupId);
        Long categoryId = attrGroupEntity.getCategoryId();

        //找出当前分类所有其他分组
        List<AttributeGroupEntity> groupEntities = this.baseMapper.selectList(new QueryWrapper<AttributeGroupEntity>()
                .eq("category_id", categoryId));

        List<Long> grouIds = groupEntities.stream().map(AttributeGroupEntity::getId).collect(Collectors.toList());

        //上面查出的分组所关联的属性
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>()
                .in("attribute_group_id", grouIds));

        //从当前分类移除这些属性
        List<Long> attrIds = attrAttrgroupRelationEntities.stream().map(AttrAttrgroupRelationEntity::getAttributeId).collect(Collectors.toList());
        QueryWrapper<AttributeEntity> attrEntityQueryWrapper = new QueryWrapper<AttributeEntity>()
                .eq("category_id", categoryId).eq("attribute_type", Constant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (attrIds.size() > 0)
        {
            attrEntityQueryWrapper.notIn("id", attrIds);
        }

        String key= (String) params.get("key");
        //模糊查询
        if (!StringUtils.isEmpty(key))
        {
            attrEntityQueryWrapper.and(attrEntityQueryWrapper1 -> {
                attrEntityQueryWrapper1.eq("id",key).or().like("attribute_name",key);
            });
        }

        IPage<AttributeEntity> page = attributeService.page(new Query<AttributeEntity>().getPage(params), attrEntityQueryWrapper);
        return new PageUtils(page);
    }

    @Override
    public List<GroupWithAttibuteVo> getCatelogIdWithAttr(Long categoryId) {
        List<GroupWithAttibuteVo> attrGroupWithAttrsVos=null;
        //所有分组
        List<AttributeGroupEntity> attrGroupEntities = this.list(new QueryWrapper<AttributeGroupEntity>().eq("category_Id", categoryId));
        //所有属性
        attrGroupWithAttrsVos = attrGroupEntities.stream().map(attrGroupEntity -> {
            GroupWithAttibuteVo attrGroupWithAttrsVo = new GroupWithAttibuteVo();
            BeanUtils.copyProperties(attrGroupEntity,attrGroupWithAttrsVo);
            List<AttributeEntity> attrEntities = attributeService.getAttrRelation(attrGroupWithAttrsVo.getId());
            attrGroupWithAttrsVo.setAttributeEntities(attrEntities);
            return attrGroupWithAttrsVo;
        }).collect(Collectors.toList());
        return attrGroupWithAttrsVos;
    }

    @Override
    public List<DetailSpecsVo> getDetailSpecsBySpuId(Long spuId, Long catagoryId) {
        return attributeGroupDao.getDetailSpecsBySpuId(spuId, catagoryId);
    }

}