package com.hl.product.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.SkuAttributeValueDao;
import com.hl.product.entity.SkuAttributeValueEntity;
import com.hl.product.service.SkuAttributeValueService;


@Service("skuAttributeValueService")
public class SkuAttributeValueServiceImpl extends ServiceImpl<SkuAttributeValueDao, SkuAttributeValueEntity> implements SkuAttributeValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuAttributeValueEntity> page = this.page(
                new Query<SkuAttributeValueEntity>().getPage(params),
                new QueryWrapper<SkuAttributeValueEntity>()
        );

        return new PageUtils(page);
    }

}