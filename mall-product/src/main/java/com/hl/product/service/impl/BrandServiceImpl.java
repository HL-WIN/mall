package com.hl.product.service.impl;

import com.hl.product.service.CategoryBrandRelationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.BrandDao;
import com.hl.product.entity.BrandEntity;
import com.hl.product.service.BrandService;

import javax.annotation.Resource;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Resource
    CategoryBrandRelationService categoryBrandRelationService;

    @Cacheable(value = {"brand"},key = "#root.methodName",sync = true)
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> brandEntityQueryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(key)) {
            brandEntityQueryWrapper.eq("id",key).or().like("brand_name",key);
        }
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                brandEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @CacheEvict(value = "brand",allEntries = true)
    @Override
    public void updateRelation(BrandEntity brand) {
        //先更新自己
        this.updateById(brand);
        if (!StringUtils.isEmpty(brand.getBrandName())) {
            //如果有更新名字,同步更新
            categoryBrandRelationService.updateFromBrand(brand.getId(),brand.getBrandName());
        }
    }

}