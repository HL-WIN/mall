package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateRelation(BrandEntity brand);
}

