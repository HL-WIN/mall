package com.hl.product.service.impl;

import com.mall.commons.utils.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.SpuInfoDao;
import com.hl.product.entity.SpuInfoEntity;
import com.hl.product.service.SpuInfoService;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> spuInfoEntityQueryWrapper = new QueryWrapper<>();

        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key))
        {
            spuInfoEntityQueryWrapper.and(w->{
                w.eq("id",key).or().like("name",key);
            });
        }
        String categoryId= (String) params.get("categoryId");
        if (!StringUtils.isEmpty(categoryId)&&!"0".equalsIgnoreCase(categoryId))
        {
            spuInfoEntityQueryWrapper.eq("category_id",categoryId);
        }
        String brandId= (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(brandId))
        {
            spuInfoEntityQueryWrapper.eq("brand_id",brandId);
        }
        String status= (String) params.get("status");
        if (!StringUtils.isEmpty(status))
        {
            spuInfoEntityQueryWrapper.eq("shelf_status",status);
        }
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                spuInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public String productUp(Long spuId, String type) {
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        String re=null;
        SpuInfoEntity byId = this.getById(spuId);
        if (byId==null)
        {
            return "该商品不存在";
        }
        if ("up".equals(type)) {
            spuInfoEntity.setShelfStatus(Constant.ProductStatusEnum.SPU_UP.getCode());
            spuInfoEntity.setUpdateTime(new Date());
            this.update(spuInfoEntity,new QueryWrapper<SpuInfoEntity>().eq("id",spuId));
            re="上架成功";
        }
        else if ("down".equals(type))
        {
            spuInfoEntity.setShelfStatus(Constant.ProductStatusEnum.SPU_DOWN.getCode());
            spuInfoEntity.setUpdateTime(new Date());
            this.update(spuInfoEntity,new QueryWrapper<SpuInfoEntity>().eq("id",spuId));
            re= "下架成功";
        }
        return re;
    }

}