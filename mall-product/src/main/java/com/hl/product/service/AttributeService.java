package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.AttrRespVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.AttributeEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface AttributeService extends IService<AttributeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryBasePage(Map<String, Object> params, Long categoryId, String type);

    AttrRespVo getInfoVo(Long attibuteId);

    void saveAttribute(AttributeEntity attribute);

    void updateAttribute(AttributeEntity attribute);
    List<AttributeEntity> getAttrRelation(Long groupId);
}

