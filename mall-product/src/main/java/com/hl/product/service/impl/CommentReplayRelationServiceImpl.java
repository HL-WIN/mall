package com.hl.product.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.CommentReplayRelationDao;
import com.hl.product.entity.CommentReplayRelationEntity;
import com.hl.product.service.CommentReplayRelationService;


@Service("commentReplayRelationService")
public class CommentReplayRelationServiceImpl extends ServiceImpl<CommentReplayRelationDao, CommentReplayRelationEntity> implements CommentReplayRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CommentReplayRelationEntity> page = this.page(
                new Query<CommentReplayRelationEntity>().getPage(params),
                new QueryWrapper<CommentReplayRelationEntity>()
        );

        return new PageUtils(page);
    }

}