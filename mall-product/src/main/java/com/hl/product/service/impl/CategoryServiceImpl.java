package com.hl.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hl.product.service.CategoryBrandRelationService;
import com.hl.product.vo.CategoryLevel2Vo;
import com.hl.product.vo.CategoryLevel3Vo;
import com.hl.product.vo.CategoryVo;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.CategoryDao;
import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Resource
    CategoryBrandRelationService categoryBrandRelationService;
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Cacheable(value = {"category"},key = "#root.methodName",sync = true)
    @Override
    public List<CategoryVo> listWithTree() {
        //查询所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        List<CategoryVo> categoryVos = entities.stream().map(categoryEntity -> {
            CategoryVo categoryVo = new CategoryVo();
            BeanUtils.copyProperties(categoryEntity, categoryVo);
            return categoryVo;
        }).collect(Collectors.toList());
        entities=null;
        //组装成父子结构
        //1级别分类
        List<CategoryVo> categoryVoList = categoryVos.stream().filter(categoryVo -> categoryVo.getParentId() == 0)
                .map((menu) -> {
                    menu.setListChildrenCategory(getListChildren(menu, categoryVos));
                    return menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                }).collect(Collectors.toList());
        return categoryVoList;
    }

    @Transactional(rollbackFor = Exception.class)
//    @Caching(evict = {
//            @CacheEvict(value = {"category"},key = "'listLevel23'"),
//            @CacheEvict(value = {"category"},key = "'listLevelOne'")
//    })
    @CacheEvict(value = "category",allEntries = true)
    @Override
    public void updateRelation(CategoryEntity category) {
        //先更新自己
        this.updateById(category);
        if (!StringUtils.isEmpty(category.getCategoryName())) {
            //如果有更新名字,同步更新
            categoryBrandRelationService.updateFromCategory(category.getId(),category.getCategoryName());
        }
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        //TODO 检测引用
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long id) {
        List<Long> objects = new ArrayList<>();
        List<Long> parentPath = findParentPath(id,objects);
        Collections.reverse(parentPath);
        return  parentPath.toArray(new Long[parentPath.size()]);
    }


    @Override
    @Cacheable(value = {"category"},key = "#root.method.name",sync = true)
    public List<CategoryEntity> listLevelOne() {
        return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_id",0));

    }


    //使用SpringCache
    @Cacheable(value = {"category"},key = "#root.methodName",sync = true)
    @Override
    public Map<String, List<CategoryLevel2Vo>> listLevel23()
    {
        /**
         * 性能优化
         * 法一：只查一次数据库
         */
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);
        List<CategoryEntity> categoryEntities = getByParentId(categoryEntityList, 0L);
        Map<String, List<CategoryLevel2Vo>> stringListMap = categoryEntities.stream().collect(Collectors.toMap(m -> m.getId().toString(), l1 -> {
            List<CategoryEntity> categoryEntities1 = getByParentId(categoryEntityList, l1.getId());
            List<CategoryLevel2Vo> categoryLevel2VoList = null;
            if (categoryEntities1.size() > 0) {
                categoryLevel2VoList = categoryEntities1.stream().map(l2 -> {
                    CategoryLevel2Vo categoryLevel2Vo = new CategoryLevel2Vo(l1.getId().toString(), l2.getId().toString(), l2.getCategoryName(), null);
                    List<CategoryEntity> level3 = getByParentId(categoryEntityList, l2.getId());
                    if (level3.size() > 0)//设置第三级
                    {
                        List<CategoryLevel3Vo> categoryLevel3Vos = level3.stream().map(l3 -> {
                            CategoryLevel3Vo categoryLevel3Vo = new CategoryLevel3Vo(l2.getId().toString(), l3.getId().toString(), l3.getCategoryName());
                            return categoryLevel3Vo;
                        }).collect(Collectors.toList());
                        categoryLevel2Vo.setCatagory3List(categoryLevel3Vos);
                    }
                    return categoryLevel2Vo;
                }).collect(Collectors.toList());
            }
            return categoryLevel2VoList;
        }));
        return stringListMap;
    }


    //TODO 有堆外内存溢出
    //1、使用jedis
    //2、升级lettuce客户端
    //使用springCache之前
    public Map<String, List<CategoryLevel2Vo>> listLevel23Bak()
    {
        //加入缓存,存的都是json字符串,json跨平台兼容
        String categoryList = stringRedisTemplate.opsForValue().get("categoryList");
        if (StringUtils.isEmpty(categoryList))
        {
            //缓存没有数据
            return listLevel23BySqlWithRedissonLock();
        }
        //转成指定对象
        return JSON.parseObject(categoryList, new TypeReference<Map<String, List<CategoryLevel2Vo>>>(){});
    }


    //从数据库查，使用分布式锁Redisson
    private Map<String, List<CategoryLevel2Vo>> listLevel23BySqlWithRedissonLock() {

        RLock lock = redissonClient.getLock("categoryList-lock");
        lock.lock();
        Map<String, List<CategoryLevel2Vo>> dataFromDb = null;
            try {
                dataFromDb=getDataFromDb();
            }finally {
                lock.unlock();
            }
            return dataFromDb;
    }

    //从数据库查，使用分布式锁
    private Map<String, List<CategoryLevel2Vo>> listLevel23BySqlWithRedisLock() {
        //占位
        String uuid = UUID.randomUUID().toString();
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock", uuid,300,TimeUnit.SECONDS);
        if (lock)
        {
            //加锁成功
            //设置过期时间,避免死锁
//            stringRedisTemplate.expire("lock",30,TimeUnit.SECONDS);
            Map<String, List<CategoryLevel2Vo>> dataFromDb = null;
            try {
                dataFromDb=getDataFromDb();
            }finally {
                //脚本
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                //删除锁
                stringRedisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);
            }
            return dataFromDb;
        }else
        {
            //失败则重试
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return listLevel23BySqlWithRedisLock();
        }
    }

    //从数据库查，使用本地锁
    private Map<String, List<CategoryLevel2Vo>> listLevel23BySqlWithLocalLock() {
        //只要是同一把锁，就能锁住需要这个锁的所有线程
        //synchronized (this) SpringBoot所有的组件在容器中都是单例的。
        //本地锁只能锁一个容器的进程，分布式情况下则不会只能一个进程通过，想锁住所有的，则要使用分布式锁
        synchronized (this)
        {
            //一个进程过去后，其他进程进来就查看有没有缓存
            return getDataFromDb();
        }
    }

    //递归查找所有分类的子分类
    private List<CategoryVo> getListChildren(CategoryVo menu, List<CategoryVo> categoryVos) {
        List<CategoryVo> childrenCategory = categoryVos.stream().filter(categoryVo -> {
            return categoryVo.getParentId().equals(menu.getId());
        }).map(categoryVo -> {
            //找子菜单
            categoryVo.setListChildrenCategory(getListChildren(categoryVo,categoryVos));
            return categoryVo;
        }).sorted((menu1,menu2)->{
            return (menu1.getSort()==null?0:menu1.getSort())-(menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());
        return childrenCategory;
    }

    /**
     * 递归获取层级路径
     * @param categoryId
     * @param objects
     * @return
     */
    private List<Long> findParentPath(Long categoryId, List<Long> objects) {
        objects.add(categoryId);
        CategoryEntity byId = this.getById(categoryId);
        if (byId.getParentId()!=0)
        {
            findParentPath(byId.getParentId(),objects);
        }
        return objects;
    }

    private Map<String, List<CategoryLevel2Vo>> getDataFromDb() {
        //一个进程过去后，其他进程进来就查看有没有缓存
        String categoryList = stringRedisTemplate.opsForValue().get("categoryList");
        if (!StringUtils.isEmpty(categoryList)) {
            return JSON.parseObject(categoryList, new TypeReference<Map<String, List<CategoryLevel2Vo>>>() {
            });
        }
        /**
         * 性能优化
         * 法一：只查一次数据库
         */
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);
//        List<CategoryEntity> categoryEntities = listLevelOne();
        List<CategoryEntity> categoryEntities = getByParentId(categoryEntityList, 0L);
        Map<String, List<CategoryLevel2Vo>> stringListMap = categoryEntities.stream().collect(Collectors.toMap(m -> m.getId().toString(), l1 -> {
//            List<CategoryEntity> categoryEntities1 = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_id", l1.getId()));
            List<CategoryEntity> categoryEntities1 = getByParentId(categoryEntityList, l1.getId());
            List<CategoryLevel2Vo> categoryLevel2VoList = null;
            if (categoryEntities1.size() > 0) {
                categoryLevel2VoList = categoryEntities1.stream().map(l2 -> {
                    CategoryLevel2Vo categoryLevel2Vo = new CategoryLevel2Vo(l1.getId().toString(), l2.getId().toString(), l2.getCategoryName(), null);
//                    List<CategoryEntity> level3 = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_id", l2.getId()));
                    List<CategoryEntity> level3 = getByParentId(categoryEntityList, l2.getId());
                    if (level3.size() > 0)//设置第三级
                    {
                        List<CategoryLevel3Vo> categoryLevel3Vos = level3.stream().map(l3 -> {
                            CategoryLevel3Vo categoryLevel3Vo = new CategoryLevel3Vo(l2.getId().toString(), l3.getId().toString(), l3.getCategoryName());
                            return categoryLevel3Vo;
                        }).collect(Collectors.toList());
                        categoryLevel2Vo.setCatagory3List(categoryLevel3Vos);
                    }
                    return categoryLevel2Vo;
                }).collect(Collectors.toList());
            }
            return categoryLevel2VoList;
        }));

        //从数据库查到的数据放入缓存,转json字符串
        String jsonString = JSON.toJSONString(stringListMap);
        stringRedisTemplate.opsForValue().set("categoryList", jsonString, 1, TimeUnit.DAYS);
        //一天过期,防止雪崩
        return stringListMap;
    }

    //优化数据库查询
    private List<CategoryEntity> getByParentId(List<CategoryEntity> categoryEntities,Long parentId)
    {
        return categoryEntities.stream().filter(categoryEntity -> categoryEntity.getParentId().equals(parentId)).collect(Collectors.toList());
    }
}