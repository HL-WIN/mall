package com.hl.product.service.impl;

import com.hl.product.entity.SkuAttributeValueEntity;
import com.hl.product.entity.SkuImagesEntity;
import com.hl.product.service.SkuAttributeValueService;
import com.hl.product.service.SkuImagesService;
import com.hl.product.vo.ImagesVo;
import com.hl.product.vo.SkusVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.SkuInfoDao;
import com.hl.product.entity.SkuInfoEntity;
import com.hl.product.service.SkuInfoService;

import javax.annotation.Resource;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Resource
    SkuImagesService skuImagesService;
    @Resource
    SkuAttributeValueService skuAttributeValueService;

    /**
     * key: 检索关键字
     * catelogId: 0,
     * brandId: 0,
     * min: 0,
     * max: 0
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SkuInfoEntity> skuInfoEntityQueryWrapper = new QueryWrapper<>();

        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key))
        {
            skuInfoEntityQueryWrapper.and(w->{
                w.eq("id",key).or().like("sku_name",key);
            });
        }
        String categoryId= (String) params.get("categoryId");
        if (!StringUtils.isEmpty(categoryId)&& !"0".equalsIgnoreCase(categoryId))
        {
            skuInfoEntityQueryWrapper.eq("category_id",categoryId);
        }
        String brandId= (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(brandId))
        {
            skuInfoEntityQueryWrapper.eq("brand_id",brandId);
        }
        String min= (String) params.get("min");
        if (!StringUtils.isEmpty(min))
        {
            skuInfoEntityQueryWrapper.ge("price",min);
        }
        String max= (String) params.get("max");
        if (!StringUtils.isEmpty(max))
        {
            try {
                BigDecimal bigDecimal = new BigDecimal(max);
                if (bigDecimal.compareTo(new BigDecimal("0"))==1) {
                    skuInfoEntityQueryWrapper.le("price",max);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                skuInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public boolean saveFromProductAdd(List<SkusVo> skus, Long brandId,Long categoryId, Long spuId) {
        boolean result=false;
        if (skus.size()!=0)
        {
            skus.forEach(skusVo -> {
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(skusVo,skuInfoEntity);
                skuInfoEntity.setBrandId(brandId);
                skuInfoEntity.setCatagoryId(categoryId);
                skuInfoEntity.setSpuId(spuId);
                for (ImagesVo images:skusVo.getImages()) {
                    if (images.getDefaultImg()==1)
                    {
                        skuInfoEntity.setDefaultImage(images.getImgUrl());
                    }
                }
                this.save(skuInfoEntity);
                //保存sku图片信息
                List<SkuImagesEntity> collect = skusVo.getImages().stream().map(imagesVo -> {
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                    skuImagesEntity.setSkuId(skuInfoEntity.getId());
                    skuImagesEntity.setUrl(imagesVo.getImgUrl());
                    skuImagesEntity.setDefaultStatus(imagesVo.getDefaultImg());
                    return skuImagesEntity;
                }).collect(Collectors.toList());
                skuImagesService.saveBatch(collect);
                //保存sku销售信息
                List<SkuAttributeValueEntity> skuSaleAttrValueEntities = skusVo.getAttributeVos().stream().map(attributeVo -> {
                    SkuAttributeValueEntity skuAttributeValueEntity = new SkuAttributeValueEntity();
                    BeanUtils.copyProperties(attributeVo, skuAttributeValueEntity);
                    skuAttributeValueEntity.setSkuId(skuInfoEntity.getId());
                    return skuAttributeValueEntity;
                }).collect(Collectors.toList());
                skuAttributeValueService.saveBatch(skuSaleAttrValueEntities);
            });

        }
        return true;
    }

}