package com.hl.product.service.impl;


import com.hl.product.dao.AttrAttrgroupRelationDao;
import com.hl.product.dao.AttributeGroupDao;
import com.hl.product.dao.CategoryDao;
import com.hl.product.entity.AttrAttrgroupRelationEntity;
import com.hl.product.entity.AttributeGroupEntity;
import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.CategoryService;
import com.hl.product.vo.AttrRespVo;
import com.mall.commons.utils.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.AttributeDao;
import com.hl.product.entity.AttributeEntity;
import com.hl.product.service.AttributeService;

import javax.annotation.Resource;


@Service("attributeService")
public class AttributeServiceImpl extends ServiceImpl<AttributeDao, AttributeEntity> implements AttributeService {

    @Resource
    CategoryService categoryService;
    @Resource
    CategoryDao categoryDao;
    @Resource
    AttributeGroupDao attributeGroupDao;
    @Resource
    AttrAttrgroupRelationDao attrAttrgroupRelationDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttributeEntity> page = this.page(
                new Query<AttributeEntity>().getPage(params),
                new QueryWrapper<AttributeEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryBasePage(Map<String, Object> params,Long categoryId, String type) {
        QueryWrapper<AttributeEntity> attributeEntityQueryWrapper = new QueryWrapper<AttributeEntity>()
                .eq("attribute_type","base".equalsIgnoreCase(type)? Constant.AttrEnum.ATTR_TYPE_BASE.getCode():Constant.AttrEnum.ATTR_TYPE_SALE.getCode());
        if (categoryId!=0) {
            attributeEntityQueryWrapper.eq("category_id",categoryId);
        }
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            attributeEntityQueryWrapper.and((attributeEntityQueryWrapper1)-> {
                attributeEntityQueryWrapper1.eq("id", key).or().like("attribute_name", key);
            });
        }
        IPage<AttributeEntity> page = this.page(
                new Query<AttributeEntity>().getPage(params),
                attributeEntityQueryWrapper
        );

        PageUtils pageUtils = new PageUtils(page);
        List<AttributeEntity> records = page.getRecords();
        List<AttrRespVo> respVos = records.stream().map((attributeEntity) -> {
            AttrRespVo attrRespVo = new AttrRespVo();
            BeanUtils.copyProperties(attributeEntity, attrRespVo);
            attrRespVo.setAttributeName(attributeEntity.getAttributeName());
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(
                    new QueryWrapper<AttrAttrgroupRelationEntity>()
                            .eq("attribute_id", attributeEntity.getId()));
            if (attrAttrgroupRelationEntity != null&&attrAttrgroupRelationEntity.getAttributeGroupId()!=null) {
                AttributeGroupEntity attrGroupEntity = attributeGroupDao.selectById(attrAttrgroupRelationEntity.getAttributeGroupId());
                attrRespVo.setGroupName(attrGroupEntity.getGroupName());
            }else {
                attrRespVo.setGroupName((key==null?"":key)+"还没有和属性分组关联，请设置关联！");
            }
            CategoryEntity categoryEntity = categoryDao.selectById(attributeEntity.getCategoryId());
            if (categoryEntity != null) {
                attrRespVo.setCategoryName(categoryEntity.getCategoryName());
            }
            attrRespVo.setCategoryPath(categoryService.findCatelogPath(categoryEntity.getId()));
            return attrRespVo;
        }).collect(Collectors.toList());
        pageUtils.setList(respVos);
        return pageUtils;
    }

    @Override
    public AttrRespVo getInfoVo(Long attributeId) {
        AttrRespVo attrRespVo = new AttrRespVo();
        AttributeEntity attributeEntity = this.getById(attributeId);
        BeanUtils.copyProperties(attributeEntity,attrRespVo);

        //分组信息
        if (attributeEntity.getAttributeType()==Constant.AttrEnum.ATTR_TYPE_BASE.getCode())
        {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq("attribute_id", attributeId));
            if (attrAttrgroupRelationEntity!=null) {
                AttributeGroupEntity attributeGroupEntity = attributeGroupDao.selectById(attrAttrgroupRelationEntity.getAttributeGroupId());
                attrRespVo.setGroupId(attrAttrgroupRelationEntity.getAttributeGroupId());
                if (attributeGroupEntity!=null) {
                    attrRespVo.setGroupName(attributeGroupEntity.getGroupName());
                }
            }

        }

        //分类信息
        Long catelogId = attributeEntity.getCategoryId();
        Long[] catelogPath = categoryService.findCatelogPath(catelogId);
        attrRespVo.setCategoryPath(catelogPath);
        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        if (categoryEntity!=null)
        {
            attrRespVo.setCategoryName(categoryEntity.getCategoryName());
        }
        return attrRespVo;
    }

    @Override
    public void saveAttribute(AttributeEntity attribute) {
        this.save(attribute);
        //保存关联关系
        if (attribute.getAttributeType()== Constant.AttrEnum.ATTR_TYPE_BASE.getCode()&&attribute.getGroupId()!=null)
        {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity=new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttributeGroupId(attribute.getGroupId());
            attrAttrgroupRelationEntity.setAttributeId(attribute.getId());
            attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);
        }
    }

    @Override
    public void updateAttribute(AttributeEntity attribute) {
        this.updateById(attribute);
        if (attribute.getAttributeType()==Constant.AttrEnum.ATTR_TYPE_BASE.getCode())
        {
            //分组关联
            Integer integer = attrAttrgroupRelationDao.selectCount(new QueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq("attribute_id", attribute.getId()));
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttributeGroupId(attribute.getGroupId());
            attrAttrgroupRelationEntity.setAttributeId(attribute.getId());
            if (integer>0)
            {
                attrAttrgroupRelationDao.update(attrAttrgroupRelationEntity,new QueryWrapper<AttrAttrgroupRelationEntity>()
                        .eq("attribute_id",attribute.getId()));
            }else
            {
                attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);
            }
        }
    }

    @Override
    public List<AttributeEntity> getAttrRelation(Long groupId) {
        List<AttributeEntity> attrEntities=null;
        List<AttrAttrgroupRelationEntity> attrgroupRelationEntities = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>()
                .eq("attribute_group_id", groupId));
        if (attrgroupRelationEntities.size()!=0)
        {
            List<Long> attrIds = attrgroupRelationEntities.stream().map(AttrAttrgroupRelationEntity::getAttributeId)
                    .collect(Collectors.toList());
            attrEntities= this.listByIds(attrIds);
        }
        return attrEntities;

    }

}