package com.hl.product.service.impl;

import com.hl.product.dao.BrandDao;
import com.hl.product.dao.CategoryDao;
import com.hl.product.entity.BrandEntity;
import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.BrandService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.CategoryBrandRelationDao;
import com.hl.product.entity.CategoryBrandRelationEntity;
import com.hl.product.service.CategoryBrandRelationService;

import javax.annotation.Resource;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {
    @Resource
    BrandDao brandDao;
    @Resource
    CategoryDao categoryDao;
    @Resource
    CategoryBrandRelationDao categoryBrandRelationDao;
    @Resource
    BrandService brandService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    //级联更新
    @Override
    public void updateFromCategory(Long categoryid, String categoryName) {
        this.baseMapper.updateAllCategory(categoryid,categoryName);
    }

    @Override
    public void updateFromBrand(Long brandId, String brandName) {
        CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
        categoryBrandRelationEntity.setBrandId(brandId);
        categoryBrandRelationEntity.setBrandName(brandName);
        this.update(categoryBrandRelationEntity,
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id",brandId));
    }

    @Override
    public List<BrandEntity> getBrandsListByCatId(Long categoryId) {
        List<BrandEntity> brandEntities = null;
        List<CategoryBrandRelationEntity> categoryBrandRelationEntities = categoryBrandRelationDao.selectList(new QueryWrapper<CategoryBrandRelationEntity>().eq("category_id", categoryId));
        brandEntities = categoryBrandRelationEntities.stream().map(categoryBrandRelationEntity -> {
            Long brandId=categoryBrandRelationEntity.getBrandId();
            return brandService.getById(brandId);
        }).filter(Objects::nonNull).collect(Collectors.toList());
        return brandEntities;
    }

    @Override
    public CategoryBrandRelationEntity searchName(CategoryBrandRelationEntity categoryBrandRelation) {
        Long brandId = categoryBrandRelation.getBrandId();
        Long categoryId = categoryBrandRelation.getCategoryId();
        BrandEntity brandEntity = brandDao.selectById(brandId);
        CategoryEntity categoryEntity = categoryDao.selectById(categoryId);
        if (brandEntity==null||categoryEntity==null)
        {
            return null;
        }
        categoryBrandRelation.setBrandName(brandEntity.getBrandName());
        categoryBrandRelation.setCategoryName(categoryEntity.getCategoryName());
        return categoryBrandRelation;
    }

}