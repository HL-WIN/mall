package com.hl.product.service;

import com.hl.product.vo.ProductDetailVo;
import com.hl.product.vo.ProductVo;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/17:58
 */

public interface ProductService {

    Boolean productAdd(ProductVo productVo);

    ProductDetailVo getProductDeatilBySkuId(Long skuId);
}
