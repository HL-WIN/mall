package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.BaseAttributesVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SpuAttributeValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface SpuAttributeValueService extends IService<SpuAttributeValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SpuAttributeValueEntity> listBySpuId(Long spuId);

    boolean updateBySpuId(Long spuId, List<SpuAttributeValueEntity> entities);

    boolean saveFromProductAdd(List<BaseAttributesVo> baseAttrs, Long spuId);
}

