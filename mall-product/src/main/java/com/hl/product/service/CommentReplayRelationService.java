package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.CommentReplayRelationEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface CommentReplayRelationService extends IService<CommentReplayRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

