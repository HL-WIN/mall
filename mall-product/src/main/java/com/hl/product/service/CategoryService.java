package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.CategoryLevel2Vo;
import com.hl.product.vo.CategoryVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:54
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryVo> listWithTree();

    void updateRelation(CategoryEntity category);

    void removeMenusByIds(List<Long> asList);

    Long[] findCatelogPath(Long id);

    List<CategoryEntity> listLevelOne();

    Map<String, List<CategoryLevel2Vo>> listLevel23();
}

