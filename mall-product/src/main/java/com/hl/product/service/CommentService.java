package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.CommentEntity;

import java.util.Map;

/**
 * 商品评价
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface CommentService extends IService<CommentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

