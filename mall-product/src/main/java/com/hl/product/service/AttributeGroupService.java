package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.AttributeAndGroupRelationVo;
import com.hl.product.vo.DetailSpecsVo;
import com.hl.product.vo.GroupWithAttibuteVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.AttributeGroupEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface AttributeGroupService extends IService<AttributeGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCategoryId(Map<String, Object> params, Long categoryId);

    void deleteRelation(AttributeAndGroupRelationVo[] attributeAndGroupRelationVos);

    PageUtils getNoattrRelation(Map<String, Object> params, Long attrGroupId);

    List<GroupWithAttibuteVo> getCatelogIdWithAttr(Long categoryId);

    List<DetailSpecsVo> getDetailSpecsBySpuId(Long spuId, Long catagoryId);

}

