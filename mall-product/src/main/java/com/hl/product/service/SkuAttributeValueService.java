package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SkuAttributeValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
public interface SkuAttributeValueService extends IService<SkuAttributeValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

