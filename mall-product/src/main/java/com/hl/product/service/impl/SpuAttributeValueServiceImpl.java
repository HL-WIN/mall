package com.hl.product.service.impl;

import com.hl.product.entity.AttributeEntity;
import com.hl.product.service.AttributeService;
import com.hl.product.vo.BaseAttributesVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.product.dao.SpuAttributeValueDao;
import com.hl.product.entity.SpuAttributeValueEntity;
import com.hl.product.service.SpuAttributeValueService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("spuAttributeValueService")
public class SpuAttributeValueServiceImpl extends ServiceImpl<SpuAttributeValueDao, SpuAttributeValueEntity> implements SpuAttributeValueService {

    @Resource
    AttributeService attributeService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuAttributeValueEntity> page = this.page(
                new Query<SpuAttributeValueEntity>().getPage(params),
                new QueryWrapper<SpuAttributeValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SpuAttributeValueEntity> listBySpuId(Long spuId) {
        return this.baseMapper.selectList(new QueryWrapper<SpuAttributeValueEntity>().eq("spu_id", spuId));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateBySpuId(Long spuId, List<SpuAttributeValueEntity> entities) {
        List<SpuAttributeValueEntity> attributeValueEntities=null;
        //先查找出符合条件的spuAttributeValueEntity
         attributeValueEntities= entities.stream().map(spuAttributeValueEntity -> {
            SpuAttributeValueEntity attributeValueEntity = this.getOne(new QueryWrapper<SpuAttributeValueEntity>()
                    .eq("spu_id", spuId).eq("attribute_id", spuAttributeValueEntity.getAttributeId()));
            if (attributeValueEntity!=null)
            {
                attributeValueEntity.setAttributeName(spuAttributeValueEntity.getAttributeName());
                attributeValueEntity.setAttributeValue(spuAttributeValueEntity.getAttributeValue());
            }
            return attributeValueEntity;
        }).collect(Collectors.toList());

         //更新属性表
        if (attributeValueEntities.size()>0)
        {
            attributeValueEntities.forEach(spuAttributeValueEntity -> {
                AttributeEntity attributeEntity = new AttributeEntity();
                attributeEntity.setId(spuAttributeValueEntity.getAttributeId());
                attributeEntity.setAttributeName(spuAttributeValueEntity.getAttributeName());
                attributeEntity.setAttributeValue(spuAttributeValueEntity.getAttributeValue());
                SpuAttributeValueEntity attributeValueEntity = new SpuAttributeValueEntity();
                //同时更新其他spu的信息
                attributeValueEntity.setAttributeName(spuAttributeValueEntity.getAttributeName());
                attributeValueEntity.setAttributeValue(spuAttributeValueEntity.getAttributeValue());
                this.update(attributeValueEntity,new QueryWrapper<SpuAttributeValueEntity>().eq("attribute_id",spuAttributeValueEntity.getAttributeId()));

                attributeService.update(attributeEntity,new QueryWrapper<AttributeEntity>().eq("id",spuAttributeValueEntity.getAttributeId()));
            });
            this.updateBatchById(attributeValueEntities);
        }
        return attributeValueEntities.size()>0;
    }

    @Override
    public boolean saveFromProductAdd(List<BaseAttributesVo> baseAttrs, Long spuId) {
        List<SpuAttributeValueEntity> spuAttributeValueEntities = baseAttrs.stream().map(baseAttrsVo -> {
            SpuAttributeValueEntity spuAttributeValueEntity = new SpuAttributeValueEntity();
            spuAttributeValueEntity.setAttributeId(baseAttrsVo.getAttributeId());
            spuAttributeValueEntity.setAttributeName(attributeService.getById(baseAttrsVo.getAttributeId()).getAttributeName());
            spuAttributeValueEntity.setAttributeValue(baseAttrsVo.getAttributeValues());
//            spuAttributeValueEntity.setQuickShow(baseAttrsVo.getShowDesc());
            spuAttributeValueEntity.setSpuId(spuId);
            return spuAttributeValueEntity;
        }).collect(Collectors.toList());
        return this.saveBatch(spuAttributeValueEntities);
    }

}