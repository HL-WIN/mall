package com.hl.product.dao;

import com.hl.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:54
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
