package com.hl.product.dao;

import com.hl.product.entity.AttributeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface AttributeDao extends BaseMapper<AttributeEntity> {
	
}
