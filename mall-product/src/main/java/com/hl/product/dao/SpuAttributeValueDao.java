package com.hl.product.dao;

import com.hl.product.entity.SpuAttributeValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface SpuAttributeValueDao extends BaseMapper<SpuAttributeValueEntity> {
	
}
