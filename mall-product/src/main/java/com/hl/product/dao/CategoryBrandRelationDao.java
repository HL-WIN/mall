package com.hl.product.dao;

import com.hl.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    void updateAllCategory(@Param("catId") Long categoryId, @Param("name") String name);
}
