package com.hl.product.dao;

import com.hl.product.entity.CommentReplayRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价回复关系
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface CommentReplayRelationDao extends BaseMapper<CommentReplayRelationEntity> {
	
}
