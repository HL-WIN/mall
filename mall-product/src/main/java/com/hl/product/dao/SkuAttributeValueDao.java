package com.hl.product.dao;

import com.hl.product.entity.SkuAttributeValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.vo.SaleAttributeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku销售属性&值
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface SkuAttributeValueDao extends BaseMapper<SkuAttributeValueEntity> {

    List<SaleAttributeVo> getSaleAttributeVoBySpuId(@Param("spuId") Long spuId);
}
