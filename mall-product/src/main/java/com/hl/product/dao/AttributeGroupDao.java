package com.hl.product.dao;

import com.hl.product.entity.AttributeGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.vo.DetailSpecsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性分组
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Mapper
public interface AttributeGroupDao extends BaseMapper<AttributeGroupEntity> {

    List<DetailSpecsVo> getDetailSpecsBySpuId(@Param("spuId") Long spuId, @Param("catagoryId") Long catagoryId);
}
