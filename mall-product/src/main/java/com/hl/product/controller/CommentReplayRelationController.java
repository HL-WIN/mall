package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.CommentReplayRelationEntity;
import com.hl.product.service.CommentReplayRelationService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品评价回复关系
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/commentreplayrelation")
public class CommentReplayRelationController {
    @Autowired
    private CommentReplayRelationService commentReplayRelationService;

    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = commentReplayRelationService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CommentReplayRelationEntity commentReplayRelation = commentReplayRelationService.getById(id);
        HashMap<String, CommentReplayRelationEntity> hashMap=new HashMap<>();
        hashMap.put("commentReplayRelation", commentReplayRelation);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody CommentReplayRelationEntity commentReplayRelation){
		commentReplayRelationService.save(commentReplayRelation);

        return CommonResult.success("保存commentReplayRelation成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody CommentReplayRelationEntity commentReplayRelation){
		commentReplayRelationService.updateById(commentReplayRelation);

        return CommonResult.success("修改commentReplayRelation成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		commentReplayRelationService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除commentReplayRelation成功！");
    }

}
