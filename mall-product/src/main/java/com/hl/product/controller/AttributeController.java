package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import com.hl.product.vo.AttrRespVo;
import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.AttributeEntity;
import com.hl.product.service.AttributeService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品属性
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/attribute")
public class AttributeController {
    @Autowired
    private AttributeService attributeService;


    /**
     * 按照属性类型获取属性
     * @param params
     * @param categoryId
     * @param type
     * @return
     */
    @ApiOperation(value = "按照属性类型获取属性")
    @GetMapping("/{attrType}/list/{categoryId}")
    public CommonResult baseList(@RequestParam Map<String, Object> params,@PathVariable("categoryId") Long categoryId,
                                 @PathVariable("attrType")String type){

        PageUtils page = attributeService.queryBasePage(params,categoryId,type);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = attributeService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "按照attibuteId获取信息")
    @GetMapping("/info/{attibuteId}")
    public CommonResult info(@PathVariable("attibuteId") Long attibuteId){
//		AttributeEntity attribute = attributeService.getById(id);
        AttrRespVo attrRespVo=attributeService.getInfoVo(attibuteId);
        return new CommonResult(200,"信息",attrRespVo);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存attribute")
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody AttributeEntity attribute){
		attributeService.saveAttribute(attribute);
        return CommonResult.success("保存attribute成功！");
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改attribute")
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class)@RequestBody AttributeEntity attribute){
		attributeService.updateAttribute(attribute);
        return CommonResult.success("修改attribute成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		attributeService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除attribute成功！");
    }

}
