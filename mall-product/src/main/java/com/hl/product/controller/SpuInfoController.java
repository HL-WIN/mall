package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SpuInfoEntity;
import com.hl.product.service.SpuInfoService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * spu信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/spuinfo")
public class SpuInfoController {
    @Autowired
    private SpuInfoService spuInfoService;


    /**
     * 列表
     * params
     * 请求参数
     * {
     *    page: 1,//当前页码
     *    limit: 10,//每页记录数
     *    sidx: 'id',//排序字段
     *    order: 'asc/desc',//排序方式
     *    key: '华为',//检索关键字
     *    catelogId: 6,//三级分类id
     *    brandId: 1,//品牌id
     *    status: 0,//商品状态
     * }
     */
    @ApiOperation(value = "spu检索")
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = spuInfoService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }

    /**
     *
     * @param spuId
     * @param type up上架，down下架
     * @return
     */
    @ApiOperation(value = "商品上下架")
    @PostMapping("/{type}/{spuId}")
    public CommonResult productUp(@PathVariable("spuId") Long spuId,@PathVariable("type") String type)
    {
        String re=spuInfoService.productUp(spuId,type);
        return CommonResult.success(re);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SpuInfoEntity spuInfo = spuInfoService.getById(id);
        HashMap<String, SpuInfoEntity> hashMap=new HashMap<>();
        hashMap.put("spuInfo", spuInfo);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody SpuInfoEntity spuInfo){
		spuInfoService.save(spuInfo);

        return CommonResult.success("保存spuInfo成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody SpuInfoEntity spuInfo){
		spuInfoService.updateById(spuInfo);

        return CommonResult.success("修改spuInfo成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		spuInfoService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除spuInfo成功！");
    }

}
