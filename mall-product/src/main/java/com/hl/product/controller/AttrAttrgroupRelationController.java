package com.hl.product.controller;

import java.util.*;
import java.util.Map;

import com.hl.product.service.AttributeGroupService;
import com.hl.product.vo.AttributeAndGroupRelationVo;
import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.AttrAttrgroupRelationEntity;
import com.hl.product.service.AttrAttrgroupRelationService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import javax.annotation.Resource;


/**
 * 属性&属性分组关联
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-22 16:31:57
 */
@RestController
@RequestMapping("product/attrattrgrouprelation")
public class AttrAttrgroupRelationController {
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;
    @Resource
    AttributeGroupService attributeGroupService;

    /**
     * 添加属性与分组关联关系
     * @param attributeAndGroupRelationVos
     * @return
     */
    @ApiOperation(value = "添加属性与分组关联关系")
    @PostMapping("/attribute/relation/add")
    public CommonResult addRelation(@RequestBody List<AttributeAndGroupRelationVo> attributeAndGroupRelationVos)
    {
        attrAttrgroupRelationService.saveBatch(attributeAndGroupRelationVos);
        return CommonResult.success("添加属性与分组关联关系");
    }
    /**
     * 删除属性与分组的关联关系
     * @param attributeAndGroupRelationVos
     * @return
     */
    @ApiOperation(value = "删除属性与分组的关联关系")
    @PostMapping("/attr/relation/delete")
    public CommonResult deleteRelation(@RequestBody AttributeAndGroupRelationVo[] attributeAndGroupRelationVos)
    {
        attributeGroupService.deleteRelation(attributeAndGroupRelationVos);
        return CommonResult.success("删除属性与分组的关联关系成功");
    }
    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = attrAttrgroupRelationService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		AttrAttrgroupRelationEntity attrAttrgroupRelation = attrAttrgroupRelationService.getById(id);
        HashMap<String, AttrAttrgroupRelationEntity> hashMap=new HashMap<>();
        hashMap.put("attrAttrgroupRelation", attrAttrgroupRelation);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody AttrAttrgroupRelationEntity attrAttrgroupRelation){
		attrAttrgroupRelationService.save(attrAttrgroupRelation);

        return CommonResult.success("保存attrAttrgroupRelation成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class) @RequestBody AttrAttrgroupRelationEntity attrAttrgroupRelation){
		attrAttrgroupRelationService.updateById(attrAttrgroupRelation);

        return CommonResult.success("修改attrAttrgroupRelation成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		attrAttrgroupRelationService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除attrAttrgroupRelation成功！");
    }

}
