package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.CommentEntity;
import com.hl.product.service.CommentService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品评价
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = commentService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CommentEntity comment = commentService.getById(id);
        HashMap<String, CommentEntity> hashMap=new HashMap<>();
        hashMap.put("comment", comment);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody CommentEntity comment){
		commentService.save(comment);

        return CommonResult.success("保存comment成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody CommentEntity comment){
		commentService.updateById(comment);

        return CommonResult.success("修改comment成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		commentService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除comment成功！");
    }

}
