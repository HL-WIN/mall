package com.hl.product.controller;

import java.util.*;
import java.util.Map;

import com.hl.product.vo.CategoryLevel2Vo;
import com.hl.product.vo.CategoryVo;
import io.swagger.annotations.ApiOperation;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.CategoryService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品三级分类
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:54
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @ApiOperation(value = "单独返回三级分类第一级")
    @GetMapping("/list/l1")
    public CommonResult listOne()
    {
       List<CategoryEntity> categoryEntities= categoryService.listLevelOne();
       return new CommonResult(200,"单独返回三级分类第一级",categoryEntities);
    }
    @ApiOperation(value = "单独返回三级分类二三级")
    @GetMapping("/list/l23")
    public CommonResult listTwoAndT()
    {
        Map<String, List<CategoryLevel2Vo>> stringListMap = categoryService.listLevel23();
        return new CommonResult(200,"单独返回三级分类第二三级",stringListMap);
    }

    /**
     * 返回三级分类
     * @return
     */
    @ApiOperation(value = "返回三级分类层次结构")
    @GetMapping("/list/tree")
    public CommonResult listAll()
    {
        List<CategoryVo> entities=categoryService.listWithTree();
        if (entities.size()!=0)
        {
            return new CommonResult(HttpStatus.SC_OK,"查询分类树形结构成功！",entities);
        }
        else {
            return CommonResult.error("查询分类树形结构失败！");
        }
    }

    /**
     * 批量层级关系修改
     */
    @ApiOperation(value = "批量层级关系修改")
    @PostMapping("/update/relation")
    public CommonResult updateSort(@RequestBody CategoryEntity[] categoryEntities)
    {
        boolean result=categoryService.updateBatchById(Arrays.asList(categoryEntities));
        if (result) {
            return CommonResult.success("批量修改成功!");
        }
        else
        {
            return CommonResult.error("批量修改失败！");
        }
    }
    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CategoryEntity category = categoryService.getById(id);
        HashMap<String, CategoryEntity> hashMap=new HashMap<>();
        hashMap.put("category", category);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody CategoryEntity category){
		categoryService.save(category);

        return CommonResult.success("保存category成功！");
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改分类")
    @PostMapping("/update")
    public CommonResult update(@RequestBody CategoryEntity category){
        //同步修改其他表涉及的内容
        categoryService.updateRelation(category);
        return CommonResult.success("修改category成功！");
    }

    /**
     * 逻辑删除
     */
    @ApiOperation(value = "逻辑删除分类")
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] catIds){
        categoryService.removeMenusByIds(Arrays.asList(catIds));

        return CommonResult.success("删除category成功！");
    }

}
