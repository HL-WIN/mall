package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SkuImagesEntity;
import com.hl.product.service.SkuImagesService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * sku图片
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/skuimages")
public class SkuImagesController {
    @Autowired
    private SkuImagesService skuImagesService;

    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = skuImagesService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SkuImagesEntity skuImages = skuImagesService.getById(id);
        HashMap<String, SkuImagesEntity> hashMap=new HashMap<>();
        hashMap.put("skuImages", skuImages);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody SkuImagesEntity skuImages){
		skuImagesService.save(skuImages);

        return CommonResult.success("保存skuImages成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody SkuImagesEntity skuImages){
		skuImagesService.updateById(skuImages);

        return CommonResult.success("修改skuImages成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		skuImagesService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除skuImages成功！");
    }

}
