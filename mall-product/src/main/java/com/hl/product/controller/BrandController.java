package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;


import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import com.mall.commons.valid.UpdateStatusGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.BrandEntity;
import com.hl.product.service.BrandService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 品牌
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @ApiOperation(value = "可以模糊查询所有品牌信息")
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		BrandEntity brand = brandService.getById(id);
        HashMap<String, BrandEntity> hashMap=new HashMap<>();
        hashMap.put("brand", brand);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class)@RequestBody BrandEntity brand){
		brandService.save(brand);

        return CommonResult.success("保存brand成功！");
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改品牌信息")
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class) @RequestBody BrandEntity brand){
        //更新自己的同时更新其他含有本表信息的表
        brandService.updateRelation(brand);
        return CommonResult.success("修改brand成功！");
    }

    @ApiOperation(value = "修改品牌可视状态")
    @PostMapping("/update/status")
    public CommonResult updateStatus(@Validated(UpdateStatusGroup.class) @RequestBody BrandEntity brand){
        brandService.updateById(brand);
        return CommonResult.success("修改brand状态成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		brandService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除brand成功！");
    }

}
