package com.hl.product.controller;

import java.util.*;
import java.util.Map;

import com.hl.product.entity.AttributeEntity;
import com.hl.product.service.AttrAttrgroupRelationService;
import com.hl.product.service.AttributeService;
import com.hl.product.service.CategoryService;
import com.hl.product.vo.AttributeAndGroupRelationVo;
import com.hl.product.vo.GroupWithAttibuteVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.AttributeGroupEntity;
import com.hl.product.service.AttributeGroupService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import javax.annotation.Resource;


/**
 * 属性分组
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/attributegroup")
public class AttributeGroupController {
    @Autowired
    private AttributeGroupService attributeGroupService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private AttributeService attributeService;

    @Resource
    private AttrAttrgroupRelationService attrAttrgroupRelationService;
    /**
     * 获取属性分组的关联的所有属性
     */
    @ApiOperation(value = "获取属性分组的关联的所有属性")
    @GetMapping("/{attributeGroupId}/attribute/relation")
    public CommonResult getAttrRelation(@PathVariable("attributeGroupId")Long attributeGroupId)
    {
        List<AttributeEntity> attrEntities=attributeService.getAttrRelation(attributeGroupId);
        return new CommonResult(200,"获取属性分组的关联的所有属性",attrEntities);
    }
    /**
     * 根据分类Id模糊查询所有
     */
    @ApiOperation(value = "根据分类Id并且可以模糊查询所有分组信息")
    @GetMapping("/list/{categoryId}")
    public CommonResult list(@RequestParam Map<String, Object> params,@PathVariable("categoryId") Long categoryId){
        PageUtils page = attributeGroupService.queryPageByCategoryId(params,categoryId);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"分页信息",hashMap);
    }

    /**
     * 信息
     */
    @ApiOperation(value = "根据分类groupId查询分组信息")
    @GetMapping("/info/{groupId}")
    public CommonResult info(@PathVariable("groupId") Long attrGroupId){
        AttributeGroupEntity attrGroup = attributeGroupService.getById(attrGroupId);
        Long catelogId = attrGroup.getCategoryId();
        Long [] path =categoryService.findCatelogPath(catelogId);
        attrGroup.setCategoryPath(path);
        return new CommonResult(200,"信息",attrGroup);
    }
    
    /**
     * 获取属性分组没有关联的其他基础属性
     * @param params
     * params请求参数
     * {
     *    page: 1,//当前页码
     *    limit: 10,//每页记录数
     *    sidx: 'id',//排序字段
     *    order: 'asc/desc',//排序方式
     *    key: '华为'//检索关键字
     * }
     * @return
     */
    @ApiOperation(value = "获取属性分组没有关联的其他基础属性")
    @GetMapping("/{attrgroupId}/attribute/norelation")
    public CommonResult getNoattrRelation(@RequestParam Map<String, Object> params,
                                          @PathVariable("attrgroupId")Long attrGroupId)
    {
        PageUtils page=attributeGroupService.getNoattrRelation(params,attrGroupId);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"获取属性分组没有关联的其他属性",hashMap);
    }
    /**
     * 获取分类下所有分组&关联属性
     * @param categoryId
     * @return
     */
    @ApiOperation(value = "获取分类下所有分组&关联属性")
    @GetMapping("{categoryId}/withAttribute")
    public CommonResult getCatelogIdWithAttr(@PathVariable("categoryId")Long categoryId)
    {
        //查所有属性分组
        //查分组下所有属性
        List<GroupWithAttibuteVo> attrGroupWithAttrsVos=attributeGroupService.getCatelogIdWithAttr(categoryId);
        return new CommonResult(200,"获取分类下所有分组&关联属性",attrGroupWithAttrsVos);
    }
    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody AttributeGroupEntity attributeGroup){
		attributeGroupService.save(attributeGroup);

        return CommonResult.success("保存attributeGroup成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody AttributeGroupEntity attributeGroup){
		attributeGroupService.updateById(attributeGroup);

        return CommonResult.success("修改attributeGroup成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		attributeGroupService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除attributeGroup成功！");
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = attributeGroupService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }

}
