package com.hl.product.controller;

import java.util.*;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hl.product.entity.BrandEntity;
import com.hl.product.vo.BrandVo;
import com.mall.commons.valid.AddGroup;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.CategoryBrandRelationEntity;
import com.hl.product.service.CategoryBrandRelationService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 品牌分类关联
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@Slf4j
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * 查询品牌分类关联信息成功
     */
    @ApiOperation(value = "查询品牌分类关联信息")
    @GetMapping("/list")
    public CommonResult list(@RequestParam("brandId")Long brandId ){
        List<CategoryBrandRelationEntity> data = categoryBrandRelationService.list(
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id", brandId)
        );
        return new CommonResult(200,"查询品牌分类关联信息成功",data);
    }
    /**
     * 保存
     */
    @ApiOperation(value = "保存品牌分类关联信息")
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody CategoryBrandRelationEntity categoryBrandRelation){
        CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
        categoryBrandRelationEntity=categoryBrandRelationService.searchName(categoryBrandRelation);
        if (categoryBrandRelationEntity!=null&&categoryBrandRelationService.save(categoryBrandRelationEntity)) {
                return CommonResult.success("保存categoryBrandRelation成功！");
            }
            else {
                return CommonResult.error("保存categoryBrandRelation失败！");
            }
    }
    /**
     *获取分类关联的品牌
     * @return
     */
    @ApiOperation(value = "获取分类关联的品牌")
    @GetMapping("/brands/list")
    public CommonResult brandsList(@RequestParam(value = "categoryId",required = true) Long categoryId)
    {
        List<BrandEntity> brandEntities= categoryBrandRelationService.getBrandsListByCatId(categoryId);
        log.info(String.valueOf(brandEntities.size()));
        List<Object> vos = brandEntities.stream().map(brandEntity -> {
            BrandVo brandVo = new BrandVo();
            brandVo.setBrandId(brandEntity.getId());
            brandVo.setBrandName(brandEntity.getBrandName());
            return brandVo;
        }).collect(Collectors.toList());
        return new CommonResult(200,"获取分类关联的品牌",vos);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);
        HashMap<String, CategoryBrandRelationEntity> hashMap=new HashMap<>();
        hashMap.put("categoryBrandRelation", categoryBrandRelation);
        return new CommonResult(200,"信息",hashMap);
    }


    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return CommonResult.success("修改categoryBrandRelation成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除categoryBrandRelation成功！");
    }

}
