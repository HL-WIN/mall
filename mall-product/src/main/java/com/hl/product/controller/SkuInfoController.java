package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SkuInfoEntity;
import com.hl.product.service.SkuInfoService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * sku信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/skuinfo")
public class SkuInfoController {
    @Autowired
    private SkuInfoService skuInfoService;

    /**
     * 列表
     *params请求参数
     * {
     * page: 1,//当前页码
     * limit: 10,//每页记录数
     * sidx: 'id',//排序字段
     * order: 'asc/desc',//排序方式
     * key: '华为',//检索关键字
     * catelogId: 0,
     * brandId: 0,
     * min: 0,
     * max: 0
     * }
     */
    @ApiOperation(value = "sku检索")
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = skuInfoService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SkuInfoEntity skuInfo = skuInfoService.getById(id);
        return new CommonResult(200,"信息",skuInfo);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.save(skuInfo);

        return CommonResult.success("保存skuInfo成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.updateById(skuInfo);

        return CommonResult.success("修改skuInfo成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		skuInfoService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除skuInfo成功！");
    }

}
