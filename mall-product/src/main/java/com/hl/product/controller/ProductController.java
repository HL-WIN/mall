package com.hl.product.controller;

import com.hl.product.service.ProductService;
import com.hl.product.vo.ProductDetailVo;
import com.hl.product.vo.ProductVo;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.valid.AddGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/24/17:49
 */
@RestController
public class ProductController {
    @Resource
    private ProductService productService;

    @GetMapping("/")
    public CommonResult test()
    {
        return CommonResult.success();
    }

    @ApiOperation("商品详情")
    @GetMapping("product/detail/{skuId}")
    public CommonResult getProductDeatil(@PathVariable("skuId")Long skuId)
    {
        ProductDetailVo productDetailVo=productService.getProductDeatilBySkuId(skuId);
        return new CommonResult(200,"商品详情查询!",productDetailVo);
    }

    @ApiOperation("商品新增")
    @PostMapping("product/add")
    public CommonResult productAdd(@Validated(AddGroup.class) @RequestBody ProductVo productVo)
    {
        Boolean result = productService.productAdd(productVo);
        if (result) {
            return CommonResult.success("新增商品成功！");
        }
        else {
            return CommonResult.error("新增商品失败！");
        }
    }

}
