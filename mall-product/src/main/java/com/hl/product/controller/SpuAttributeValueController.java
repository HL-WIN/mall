package com.hl.product.controller;

import java.util.*;
import java.util.Map;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import com.sun.xml.internal.bind.v2.TODO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SpuAttributeValueEntity;
import com.hl.product.service.SpuAttributeValueService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * spu属性值
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/spuattributevalue")
public class SpuAttributeValueController {
    @Autowired
    private SpuAttributeValueService spuAttributeValueService;

    /**
     *  获取spu规格
     * @return
     */
    @ApiOperation(value = "获取spu规格")
    @GetMapping("/list/{spuId}")
    public CommonResult listBySpuId(@PathVariable("spuId") Long spuId){

        List<SpuAttributeValueEntity> entities = spuAttributeValueService.listBySpuId(spuId);
        return new CommonResult(200,"获取spu规格",entities);
    }


    @ApiOperation(value = "修改商品规格")
    @PostMapping("/update/{spuId}")
    public CommonResult updateSpu(@PathVariable("spuId") Long spuId,
                                      @RequestBody List<SpuAttributeValueEntity> entities){
        boolean updateBySpuId = spuAttributeValueService.updateBySpuId(spuId, entities);

        return CommonResult.success("修改商品规格"+updateBySpuId);
    }
    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = spuAttributeValueService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SpuAttributeValueEntity spuAttributeValue = spuAttributeValueService.getById(id);
        HashMap<String, SpuAttributeValueEntity> hashMap=new HashMap<>();
        hashMap.put("spuAttributeValue", spuAttributeValue);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody SpuAttributeValueEntity spuAttributeValue){
		spuAttributeValueService.save(spuAttributeValue);

        return CommonResult.success("保存spuAttributeValue成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class)@RequestBody SpuAttributeValueEntity spuAttributeValue){
		spuAttributeValueService.updateById(spuAttributeValue);

        return CommonResult.success("修改spuAttributeValue成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		spuAttributeValueService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除spuAttributeValue成功！");
    }

}
