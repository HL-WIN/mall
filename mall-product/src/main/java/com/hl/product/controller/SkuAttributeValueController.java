package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SkuAttributeValueEntity;
import com.hl.product.service.SkuAttributeValueService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * sku销售属性&值
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-18 16:56:53
 */
@RestController
@RequestMapping("product/skuattributevalue")
public class SkuAttributeValueController {
    @Autowired
    private SkuAttributeValueService skuAttributeValueService;

    /**
     * 列表
     */
    @GetMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = skuAttributeValueService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SkuAttributeValueEntity skuAttributeValue = skuAttributeValueService.getById(id);
        HashMap<String, SkuAttributeValueEntity> hashMap=new HashMap<>();
        hashMap.put("skuAttributeValue", skuAttributeValue);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody SkuAttributeValueEntity skuAttributeValue){
		skuAttributeValueService.save(skuAttributeValue);

        return CommonResult.success("保存skuAttributeValue成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody SkuAttributeValueEntity skuAttributeValue){
		skuAttributeValueService.updateById(skuAttributeValue);

        return CommonResult.success("修改skuAttributeValue成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		skuAttributeValueService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除skuAttributeValue成功！");
    }

}
