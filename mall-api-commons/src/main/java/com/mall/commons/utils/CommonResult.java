package com.mall.commons.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.HttpStatus;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/01/24/15:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T>{
    private Integer code;
    private String msg;
    private T data;
    public CommonResult(Integer code,String msg)
    {
        this(code,msg,null);
    }
    public static CommonResult error() {
        return new CommonResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
    }
    public static CommonResult error(String msg) {
        return new CommonResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
    }
    public static CommonResult error(String msg,Map map)
    {
        return new CommonResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg,map);
    }
    public static CommonResult error(Integer code,String msg) {
        return new CommonResult(code,msg);
    }
    public static CommonResult success(String msg) {
        return new CommonResult(HttpStatus.SC_OK, msg);
    }
    public static CommonResult success() {
        return new CommonResult(HttpStatus.SC_OK, "操作成功！");
    }
}
