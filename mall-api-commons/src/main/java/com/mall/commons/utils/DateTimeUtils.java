package com.mall.commons.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jzz
 * @create 2022/2/23
 */
public class DateTimeUtils {
    private static final String DATE_FORMAT="yyyy-mm-dd HH:mm:ss";

    public static String getDateTime() {
        return getDateTime(new Date());
    }

    public static String getDateTime(Date date) {
        return (new SimpleDateFormat(DATE_FORMAT)).format(date);
    }

}
