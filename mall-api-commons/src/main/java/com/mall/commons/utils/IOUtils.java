package com.mall.commons.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author jzz
 * @create 2022/2/23
 */
public class IOUtils {
    /**
     * 关闭对象，连接
     *
     * @param closeable 可关闭的流对象
     */
    public static void closeQuietly(final Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (final IOException ioe) {
            // ignore
        }
    }
}
