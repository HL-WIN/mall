package com.hl.ware.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 采购信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@Data
@TableName("purchase_order")
public class PurchaseOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 采购单id
	 */
		@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
@TableId
	private Long id;
	/**
	 * 采购人id
	 */
	@NotNull(message = "新增必须指定采购人id",groups = {AddGroup.class})
	private Long purchaserId;
	/**
	 * 采购人名
	 */
	private String purchaserName;
	/**
	 * 联系方式
	 */
	private String purchaserPhone;
	/**
	 * 状态[0新建，1已分配，2正在采购，3已完成，4采购失败]
	 */
	@NotNull(message = "新增必须指定采购人status",groups = {AddGroup.class})
	private Integer status;
	/**
	 * 仓库id
	 */
	@NotNull(message = "新增必须指定采购人wareId",groups = {AddGroup.class})
	private Long wareId;
	/**
	 * 总金额
	 */
	private BigDecimal expenditure;
	/**
	 * 采购商品id
	 */
	@NotNull(message = "新增必须指定商品skuId",groups = {AddGroup.class})
	private Long skuId;
	/**
	 * 采购数量
	 */
	@NotNull(message = "新增必须指定商品skuNumber",groups = {AddGroup.class})
	private Integer skuNumber;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 更新日期
	 */
	private Date updateTime;

}
