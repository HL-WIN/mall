package com.hl.ware.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 仓库信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@Data
@TableName("ware_info")
public class WareInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
		@NotNull(message = "修改必须指定id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGroup.class})
@TableId
	private Long id;
	/**
	 * 仓库名
	 */
	private String wareName;
	/**
	 * 仓库地址
	 */
	private String wareAddress;
	/**
	 * 区域编码
	 */
	private String wareAreacode;

}
