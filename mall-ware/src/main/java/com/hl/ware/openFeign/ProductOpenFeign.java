package com.hl.ware.openFeign;

import com.mall.commons.utils.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/25/15:57
 */
@FeignClient(value = "mall-product")
public interface ProductOpenFeign {
    @GetMapping("/product/category/list/tree")
    CommonResult list();
    @GetMapping("/product/skuinfo/info/{id}")
    CommonResult info(@PathVariable("id") Long id);
}
