package com.hl.ware.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/12/14:12
 */
@Configuration
@EnableTransactionManagement//开启事物
public class MybatisConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor()
    {
        PaginationInterceptor paginationInterceptor=new PaginationInterceptor();
        paginationInterceptor.setOverflow(true);//设置最大页后返回首页，fasle继续请求
        paginationInterceptor.setLimit(500);//设置单页最大条数，默认500,-1无限制
        return paginationInterceptor;
    }
}
