package com.hl.ware.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/25/17:06
 */
@Data
public class PurchaseOrderDoneVo {
    Long purchaseId;//采购单Id
    Integer status;//采购单状态
    String remark;//备注
}
