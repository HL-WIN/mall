package com.hl.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/25/16:49
 */
@Data
public class MergePurchaseOrderVo {
    private Long purchaseId;

    private List<Long> items;
}
