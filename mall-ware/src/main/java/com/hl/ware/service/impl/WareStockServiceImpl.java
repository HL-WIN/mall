package com.hl.ware.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hl.ware.openFeign.ProductOpenFeign;
import com.mall.commons.utils.CommonResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.ware.dao.WareStockDao;
import com.hl.ware.entity.WareStockEntity;
import com.hl.ware.service.WareStockService;

import javax.annotation.Resource;


@Service("wareStockService")
public class WareStockServiceImpl extends ServiceImpl<WareStockDao, WareStockEntity> implements WareStockService {

    @Resource
    ProductOpenFeign productOpenFeign;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareStockEntity> wareSkuEntityQueryWrapper = new QueryWrapper<WareStockEntity>();
        String skuId= (String) params.get("skuId");
        if (!StringUtils.isEmpty(skuId)) {
            wareSkuEntityQueryWrapper.eq("sku_id",skuId);
        }
        String wareId= (String) params.get("wareId");
        if (!StringUtils.isEmpty(wareId)) {
            wareSkuEntityQueryWrapper.eq("ware_id",wareId);
        }
        IPage<WareStockEntity> page = this.page(
                new Query<WareStockEntity>().getPage(params),
                wareSkuEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNumber) {
        //1、判读如果没有这个库存记录新增
        List<WareStockEntity> wareStockEntities = this.baseMapper.selectList(
                new QueryWrapper<WareStockEntity>().eq("sku_id", skuId).eq("ware_id", wareId));

        if (wareStockEntities == null || wareStockEntities.size() == 0) {
            WareStockEntity wareStockEntity = new WareStockEntity();
            wareStockEntity.setSkuId(skuId);
            wareStockEntity.setStock(skuNumber);
            wareStockEntity.setWareId(wareId);
            wareStockEntity.setStockLocked(0);
            //TODO 远程查询sku的名字，如果失败整个事务无需回滚
            //1、自己catch异常
            try{
                CommonResult commonResult = productOpenFeign.info(skuId);
                if (commonResult.getCode()==200&&commonResult.getData()!=null) {
                    Map<String,String > map= (Map<String, String>) commonResult.getData();
                    String skuName =  map.get("skuName");
                    wareStockEntity.setSkuName(skuName);
                }
            } catch (Exception e) {

            }
            //添加库存信息
            this.baseMapper.insert(wareStockEntity);
        } else {
            //修改库存信息
            this.baseMapper.addStock(skuId,wareId,skuNumber);
        }
    }

}