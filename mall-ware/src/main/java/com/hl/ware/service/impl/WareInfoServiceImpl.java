package com.hl.ware.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.ware.dao.WareInfoDao;
import com.hl.ware.entity.WareInfoEntity;
import com.hl.ware.service.WareInfoService;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareInfoEntity> queryWrapper=new QueryWrapper<WareInfoEntity>();
        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.eq("id",key).or().like("ware_name",key)
                    .or().like("ware_address",key)
                    .or().like("ware_areacode",key);
        }
        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}