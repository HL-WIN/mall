package com.hl.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.ware.vo.PurchaseOrderDoneVo;
import com.hl.ware.vo.PurchaseOrderVo;
import com.mall.commons.utils.PageUtils;
import com.hl.ware.entity.PurchaseOrderEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
public interface PurchaseOrderService extends IService<PurchaseOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnReList(Map<String, Object> params);

    void done(PurchaseOrderDoneVo[] purchaseOrderDoneVo);

    void receive(Long[] ids);

    void saveByVo(PurchaseOrderVo purchaseOrderVo);
}

