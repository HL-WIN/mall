package com.hl.ware.service.impl;

import com.hl.ware.service.WareStockService;
import com.hl.ware.vo.PurchaseOrderDoneVo;
import com.hl.ware.vo.PurchaseOrderVo;
import com.mall.commons.utils.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;
import com.hl.ware.dao.PurchaseOrderDao;
import com.hl.ware.entity.PurchaseOrderEntity;
import com.hl.ware.service.PurchaseOrderService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("purchaseOrderService")
public class PurchaseOrderServiceImpl extends ServiceImpl<PurchaseOrderDao, PurchaseOrderEntity> implements PurchaseOrderService {

    @Resource
    WareStockService wareStockService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<PurchaseOrderEntity> purchaseDetailEntityQueryWrapper = new QueryWrapper<>();
        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {

            purchaseDetailEntityQueryWrapper.and(purchaseDetailEntityQueryWrapper1 -> {
                purchaseDetailEntityQueryWrapper1.eq("purchase_id",key).or().eq("sku_id",key);
            });
        }
        String status= (String) params.get("status");
        if (!StringUtils.isEmpty(status)) {

            purchaseDetailEntityQueryWrapper.eq("status",status);
        }
        String wareId= (String) params.get("wareId");
        if (!StringUtils.isEmpty(status)) {

            purchaseDetailEntityQueryWrapper.eq("ware_id",wareId);
        }
        IPage<PurchaseOrderEntity> page = this.page(
                new Query<PurchaseOrderEntity>().getPage(params),
                purchaseDetailEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageUnReList(Map<String, Object> params) {
        IPage<PurchaseOrderEntity> page = this.page(
                new Query<PurchaseOrderEntity>().getPage(params),
                new QueryWrapper<PurchaseOrderEntity>().eq("status",0).or().eq("status",1)
        );
        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void done(PurchaseOrderDoneVo[] purchaseOrderDoneVo) {

        //1、改变采购项的状态
        Boolean flag = true;
        List<PurchaseOrderEntity> updates = new ArrayList<>();
        for (PurchaseOrderDoneVo item : purchaseOrderDoneVo) {
            PurchaseOrderEntity purchaseOrderEntity = this.getById(item.getPurchaseId());
            if (item.getStatus() == Constant.PurchaseOrderStatusEnum.HASERROR.getCode()) {
                flag = false;
                purchaseOrderEntity.setStatus(item.getStatus());
            } else {
                purchaseOrderEntity.setStatus(Constant.PurchaseOrderStatusEnum.FINISH.getCode());
                //3、将成功采购的进行入库
                wareStockService.addStock(purchaseOrderEntity.getSkuId(),purchaseOrderEntity.getWareId(),purchaseOrderEntity.getSkuNumber());
            }
            if (item.getRemark()!=null)
            {
                purchaseOrderEntity.setRemark(item.getRemark());
            }
            purchaseOrderEntity.setUpdateTime(new Date());
            //更新信息
            this.updateById(purchaseOrderEntity);
        }
    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void receive(Long[] ids) {
        List<Long> longs = Arrays.asList(ids);
        //确认是否新建或者分配
        List<PurchaseOrderEntity> purchaseEntities = longs.stream().map(item -> {
            PurchaseOrderEntity purchaseOrderEntity = this.getById(item);
            return purchaseOrderEntity;
        }).filter(purchaseOrderEntity -> {
            if (purchaseOrderEntity.getStatus() == Constant.PurchaseOrderStatusEnum.CREATED.getCode() ||
                    purchaseOrderEntity.getStatus() == Constant.PurchaseOrderStatusEnum.ASSIGNED.getCode()) {
                return true;
            }
            return false;
        }).map(purchaseEntity -> {
            purchaseEntity.setStatus(Constant.PurchaseOrderStatusEnum.BUYING.getCode());
            purchaseEntity.setUpdateTime(new Date());
            return purchaseEntity;
        }).collect(Collectors.toList());

        this.updateBatchById(purchaseEntities);
    }

    @Override
    public void saveByVo(PurchaseOrderVo purchaseOrderVo) {
        PurchaseOrderEntity purchaseOrderEntity = new PurchaseOrderEntity();
        BeanUtils.copyProperties(purchaseOrderVo,purchaseOrderEntity);
        purchaseOrderEntity.setCreateTime(new Date());
        purchaseOrderEntity.setUpdateTime(new Date());
        this.save(purchaseOrderEntity);
    }


}