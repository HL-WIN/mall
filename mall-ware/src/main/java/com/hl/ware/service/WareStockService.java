package com.hl.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.ware.entity.WareStockEntity;

import java.util.Map;

/**
 * 商品库存
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
public interface WareStockService extends IService<WareStockEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNumber);
}

