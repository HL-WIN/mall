package com.hl.ware.dao;

import com.hl.ware.entity.WareStockEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@Mapper
public interface WareStockDao extends BaseMapper<WareStockEntity> {

    void addStock(@Param("skuId") Long skuId,@Param("wareId") Long wareId, @Param("skuNumber")Integer skuNumber);
}
