package com.hl.ware.dao;

import com.hl.ware.entity.PurchaseOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@Mapper
public interface PurchaseOrderDao extends BaseMapper<PurchaseOrderEntity> {
	
}
