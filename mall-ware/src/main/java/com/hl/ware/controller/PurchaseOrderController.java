package com.hl.ware.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hl.ware.openFeign.ProductOpenFeign;
import com.hl.ware.vo.MergePurchaseOrderVo;
import com.hl.ware.vo.PurchaseOrderDoneVo;
import com.hl.ware.vo.PurchaseOrderVo;
import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.ware.entity.PurchaseOrderEntity;
import com.hl.ware.service.PurchaseOrderService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import javax.annotation.Resource;


/**
 * 采购信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@RestController
@RequestMapping("ware/purchaseorder")
public class PurchaseOrderController {
    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Resource
    ProductOpenFeign productOpenFeign;

    @GetMapping("test")
    public CommonResult test()
    {
        CommonResult commonResult = productOpenFeign.info(1L);
        Map<String,String > map= (Map<String, String>) commonResult.getData();
        System.out.println(commonResult.getData());
        System.out.println(map);
        String skuName = (String) map.get("skuName");
        System.out.println(skuName);
        return CommonResult.success();
    }
    /**
     * 列表
     * params请求参数
     * {
     *    page: 1,//当前页码
     *    limit: 10,//每页记录数
     *    sidx: 'id',//排序字段
     *    order: 'asc/desc',//排序方式
     *    key: '华为',//检索关键字
     *    status: 0,//状态
     *    wareId: 1,//仓库id
     * }
     */
    @ApiOperation(value = "查询采购需求")
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseOrderService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }
    /**
     * 查询未领取的采购单
     * @param params
     * @return
     */
    @ApiOperation(value = "查询未领取的采购单")
    @GetMapping("/unreceive/list")
    public CommonResult unReList(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseOrderService.queryPageUnReList(params);
        return new CommonResult(200,"查询未领取的采购单",page);
    }
    /**
     * 完成采购单
     * @param purchaseOrderDoneVos
     * @return
     */
    @ApiOperation(value = "完成采购单")
    @PostMapping(value = "/done")
    public CommonResult finish(@RequestBody PurchaseOrderDoneVo[] purchaseOrderDoneVos) {
        purchaseOrderService.done(purchaseOrderDoneVos);
        return CommonResult.success();
    }
    /**
     * 员工领取采购单
     * @param ids
     * @return
     */
    @PostMapping("/received")
    public CommonResult receive(Long[] ids)
    {
        purchaseOrderService.receive(ids);
        return CommonResult.success();
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		PurchaseOrderEntity purchaseOrder = purchaseOrderService.getById(id);
        HashMap<String, PurchaseOrderEntity> hashMap=new HashMap<>();
        hashMap.put("purchaseOrder", purchaseOrder);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 新建采购单
     */
    @ApiOperation(value = "新建采购单")
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody PurchaseOrderVo purchaseOrderVo){
		purchaseOrderService.saveByVo(purchaseOrderVo);

        return CommonResult.success("新建采购单成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class) @RequestBody PurchaseOrderEntity purchaseOrder){
		purchaseOrderService.updateById(purchaseOrder);

        return CommonResult.success("修改purchaseOrder成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		purchaseOrderService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除purchaseOrder成功！");
    }

}
