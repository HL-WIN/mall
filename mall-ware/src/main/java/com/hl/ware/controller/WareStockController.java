package com.hl.ware.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.ware.entity.WareStockEntity;
import com.hl.ware.service.WareStockService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品库存
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@RestController
@RequestMapping("ware/warestock")
public class WareStockController {
    @Autowired
    private WareStockService wareStockService;

    /**
     * 列表
     */
    @ApiOperation("查询商品库存 可传入参数 wareId,skuId")
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = wareStockService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		WareStockEntity wareStock = wareStockService.getById(id);
        HashMap<String, WareStockEntity> hashMap=new HashMap<>();
        hashMap.put("wareStock", wareStock);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody WareStockEntity wareStock){
		wareStockService.save(wareStock);

        return CommonResult.success("保存wareStock成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class) @RequestBody WareStockEntity wareStock){
		wareStockService.updateById(wareStock);

        return CommonResult.success("修改wareStock成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		wareStockService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除wareStock成功！");
    }

}
