package com.hl.ware.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;

import com.mall.commons.valid.AddGroup;
import com.mall.commons.valid.UpdateGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hl.ware.entity.WareInfoEntity;
import com.hl.ware.service.WareInfoService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 仓库信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-25 15:44:25
 */
@RestController
@RequestMapping("ware/wareinfo")
public class WareInfoController {
    @Autowired
    private WareInfoService wareInfoService;

    /**
     * 列表
     */
    @ApiOperation(value = "仓库列表 可传入参数 page(当前页码) limit（每页记录数）sidx: 'id'（//排序字段）order: 'asc/desc'（排序方式）key:（检索关键字）")
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = wareInfoService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		WareInfoEntity wareInfo = wareInfoService.getById(id);
        HashMap<String, WareInfoEntity> hashMap=new HashMap<>();
        hashMap.put("wareInfo", wareInfo);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@Validated(AddGroup.class) @RequestBody WareInfoEntity wareInfo){
		wareInfoService.save(wareInfo);

        return CommonResult.success("保存wareInfo成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@Validated(UpdateGroup.class) @RequestBody WareInfoEntity wareInfo){
		wareInfoService.updateById(wareInfo);

        return CommonResult.success("修改wareInfo成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		wareInfoService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除wareInfo成功！");
    }

}
