import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hl.ware.openFeign.ProductOpenFeign;
import com.mall.commons.utils.CommonResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/25/17:51
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class test {
    @Resource
    ProductOpenFeign productOpenFeign;
    @Test
    public  void test()
    {
        CommonResult commonResult = productOpenFeign.info(1L);
        JSONObject jsonObject= JSON.parseObject(String.valueOf(commonResult.getData()));
        System.out.println(jsonObject);
        String skuName = (String) jsonObject.get("skuName");
        System.out.println(skuName);
    }

}
