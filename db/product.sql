-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 39.99.148.87    Database: product
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attr_attrgroup_relation`
--

DROP TABLE IF EXISTS `attr_attrgroup_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attr_attrgroup_relation` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `attribute_id` bigint DEFAULT NULL COMMENT '属性id',
  `attribute_group_id` bigint DEFAULT NULL COMMENT '属性分组id',
  `sort` int DEFAULT NULL COMMENT '属性组内排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='属性&属性分组关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attr_attrgroup_relation`
--

LOCK TABLES `attr_attrgroup_relation` WRITE;
/*!40000 ALTER TABLE `attr_attrgroup_relation` DISABLE KEYS */;
INSERT INTO `attr_attrgroup_relation` VALUES (1,8,2,1),(2,9,2,2),(3,1,2,NULL),(5,1,1,NULL);
/*!40000 ALTER TABLE `attr_attrgroup_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_brand_relation`
--

DROP TABLE IF EXISTS `category_brand_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_brand_relation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_id` bigint DEFAULT NULL COMMENT '品牌id',
  `category_id` bigint DEFAULT NULL COMMENT '分类id',
  `brand_name` varchar(255) DEFAULT NULL COMMENT '品牌名称',
  `category_name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COMMENT='品牌分类关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_brand_relation`
--

LOCK TABLES `category_brand_relation` WRITE;
/*!40000 ALTER TABLE `category_brand_relation` DISABLE KEYS */;
INSERT INTO `category_brand_relation` VALUES (1,1,225,'华为','国产手机'),(2,2,225,'小米','国产手机'),(5,2,1,'小米','图书、音像、电子书刊'),(6,1,2,'华为','手机');
/*!40000 ALTER TABLE `category_brand_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_replay_relation`
--

DROP TABLE IF EXISTS `comment_replay_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment_replay_relation` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `comment_id` bigint DEFAULT NULL COMMENT '评论id',
  `reply_id` bigint DEFAULT NULL COMMENT '回复id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='商品评价回复关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_replay_relation`
--

LOCK TABLES `comment_replay_relation` WRITE;
/*!40000 ALTER TABLE `comment_replay_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_replay_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attribute`
--

DROP TABLE IF EXISTS `product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_attribute` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '属性id',
  `attribute_name` char(30) DEFAULT NULL COMMENT '属性名',
  `search_type` tinyint DEFAULT NULL COMMENT '是否需要检索[0-不需要，1-需要]',
  `attribute_icon` varchar(255) DEFAULT NULL COMMENT '属性图标',
  `attribute_value` char(255) DEFAULT NULL COMMENT '可选值列表[用逗号分隔]',
  `attribute_type` tinyint DEFAULT NULL COMMENT '属性类型[0-销售属性，1-基本属性，2-既是销售属性又是基本属性]',
  `enable_status` bigint DEFAULT NULL COMMENT '启用状态[0 - 禁用，1 - 启用]',
  `quick_show` tinyint DEFAULT NULL COMMENT '快速展示【是否展示在介绍上；0-否 1-是】',
  `category_id` bigint DEFAULT NULL COMMENT '所属分类',
  `group_id` bigint DEFAULT NULL COMMENT '规格分组id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='商品属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attribute`
--

LOCK TABLES `product_attribute` WRITE;
/*!40000 ALTER TABLE `product_attribute` DISABLE KEYS */;
INSERT INTO `product_attribute` VALUES (1,'上市',1,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-19/6043d1ac-bb54-42a3-8218-d2917ccb8452_eda8c4c43653595f.jpg','2019',1,1,0,225,2),(2,'产品名称',0,NULL,'小米10,HUAWEI Mate 30,红米k20Pro',1,1,1,225,1),(3,'机身颜色',0,NULL,'黑色,金色,白色',0,1,1,225,2),(4,'运行内存',1,NULL,'6G,8G,12G',0,1,1,225,4),(5,'机身存储',1,NULL,'128G,256G,512G',0,1,1,225,4),(6,'CPU品牌',1,NULL,'麒麟,骁龙',1,1,1,225,3),(7,'CPU型号',0,NULL,'麒麟990 5G,骁龙865',1,1,1,225,3),(8,'分辨率',1,NULL,'FHD+ 2340×1080 像素,2340*1080',1,1,1,225,5),(9,'屏幕尺寸',1,'','5,6,7',1,1,1,225,5);
/*!40000 ALTER TABLE `product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attribute_group`
--

DROP TABLE IF EXISTS `product_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_attribute_group` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '分组id',
  `group_name` char(20) DEFAULT NULL COMMENT '组名',
  `sort` int DEFAULT NULL COMMENT '排序',
  `icon` varchar(255) DEFAULT NULL COMMENT '组图标',
  `category_id` bigint DEFAULT NULL COMMENT '所属分类id',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3 COMMENT='属性分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attribute_group`
--

LOCK TABLES `product_attribute_group` WRITE;
/*!40000 ALTER TABLE `product_attribute_group` DISABLE KEYS */;
INSERT INTO `product_attribute_group` VALUES (1,'主体',1,NULL,225,''),(2,'基本信息',2,NULL,225,NULL),(3,'主芯片',3,NULL,225,NULL),(4,'存储',4,NULL,225,NULL),(5,'屏幕',5,NULL,225,'yyy'),(6,'后置摄像头',6,NULL,225,NULL),(7,'前置摄像头',7,NULL,225,NULL),(8,'电池信息',8,NULL,225,NULL),(9,'操作系统',9,NULL,225,NULL),(10,'主体',1,NULL,449,NULL),(11,'操作系统',2,NULL,449,NULL);
/*!40000 ALTER TABLE `product_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_brand`
--

DROP TABLE IF EXISTS `product_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_brand` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '品牌id',
  `brand_name` char(50) DEFAULT NULL COMMENT '品牌名',
  `icon` varchar(2000) DEFAULT NULL COMMENT '品牌图标',
  `show_status` tinyint DEFAULT NULL COMMENT '显示状态[0-不显示；1-显示]',
  `retrieval_first_letter` char(1) DEFAULT NULL COMMENT '检索首字母',
  `sort` int DEFAULT NULL COMMENT '排序',
  `remarks` longtext COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='品牌';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_brand`
--

LOCK TABLES `product_brand` WRITE;
/*!40000 ALTER TABLE `product_brand` DISABLE KEYS */;
INSERT INTO `product_brand` VALUES (1,'华为','http://www-file.huawei.com/-/media/corporate/images/home/logo/huawei_logo.png',1,'H',2,'jk'),(2,'小米','http://s02.mifile.cn/assets/static/image/mi-logo.png',1,'X',3,'jk');
/*!40000 ALTER TABLE `product_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_category` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` char(50) DEFAULT NULL COMMENT '分类名称',
  `parent_id` bigint DEFAULT NULL COMMENT '父分类id',
  `show_status` tinyint DEFAULT NULL COMMENT '是否显示[0-不显示，1显示]',
  `sort` int DEFAULT NULL COMMENT '排序',
  `icon` char(255) DEFAULT NULL COMMENT '图标地址',
  `unit` char(50) DEFAULT NULL COMMENT '计量单位',
  PRIMARY KEY (`id`),
  KEY `idx_product_category_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1433 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品三级分类';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,'图书、音像、电子书刊',0,1,0,NULL,'类'),(2,'手机',0,1,0,NULL,NULL),(3,'家用电器',0,1,0,NULL,NULL),(4,'数码',0,1,0,NULL,NULL),(5,'家居家装',0,1,0,NULL,NULL),(6,'电脑办公',0,1,0,NULL,NULL),(7,'厨具',0,1,0,NULL,NULL),(8,'个护化妆',0,1,0,NULL,NULL),(9,'服饰内衣',0,1,0,NULL,NULL),(10,'钟表',0,1,0,NULL,NULL),(11,'鞋靴',0,1,0,NULL,NULL),(12,'母婴',0,1,0,NULL,NULL),(13,'礼品箱包',0,1,0,NULL,NULL),(14,'食品饮料、保健食品',0,1,0,NULL,NULL),(15,'珠宝',0,1,0,NULL,NULL),(16,'汽车用品',0,1,0,NULL,NULL),(17,'运动健康',0,1,0,NULL,NULL),(18,'玩具乐器',0,1,0,NULL,NULL),(19,'彩票、旅行、充值、票务',0,1,0,NULL,NULL),(20,'生鲜',0,1,0,NULL,NULL),(21,'整车',0,1,0,NULL,NULL),(22,'电子书刊',1,1,0,NULL,NULL),(23,'音像',1,1,0,NULL,NULL),(24,'英文原版',1,1,0,NULL,NULL),(25,'文艺',1,1,0,NULL,NULL),(26,'少儿',1,1,0,NULL,NULL),(27,'人文社科',1,1,0,NULL,NULL),(28,'经管励志',1,1,0,NULL,NULL),(29,'生活',1,1,0,NULL,NULL),(30,'科技',1,1,0,NULL,NULL),(31,'教育',1,1,0,NULL,NULL),(32,'港台图书',1,1,0,NULL,NULL),(33,'其他',1,1,0,NULL,NULL),(34,'手机通讯',2,1,0,NULL,NULL),(35,'运营商',2,1,0,NULL,NULL),(36,'手机配件',2,1,0,NULL,NULL),(37,'大 家 电',3,1,0,NULL,NULL),(38,'厨卫大电',3,1,0,NULL,NULL),(39,'厨房小电',3,1,0,NULL,NULL),(40,'生活电器',3,1,0,NULL,NULL),(41,'个护健康',3,1,0,NULL,NULL),(42,'五金家装',3,1,0,NULL,NULL),(43,'摄影摄像',4,1,0,NULL,NULL),(44,'数码配件',4,1,0,NULL,NULL),(45,'智能设备',4,1,0,NULL,NULL),(46,'影音娱乐',4,1,0,NULL,NULL),(47,'电子教育',4,1,0,NULL,NULL),(48,'虚拟商品',4,1,0,NULL,NULL),(49,'家纺',5,1,0,NULL,NULL),(50,'灯具',5,1,0,NULL,NULL),(51,'生活日用',5,1,0,NULL,NULL),(52,'家装软饰',5,1,0,NULL,NULL),(53,'宠物生活',5,1,0,NULL,NULL),(54,'电脑整机',6,1,0,NULL,NULL),(55,'电脑配件',6,1,0,NULL,NULL),(56,'外设产品',6,1,0,NULL,NULL),(57,'游戏设备',6,1,0,NULL,NULL),(58,'网络产品',6,1,0,NULL,NULL),(59,'办公设备',6,1,0,NULL,NULL),(60,'文具/耗材',6,1,0,NULL,NULL),(61,'服务产品',6,1,0,NULL,NULL),(62,'烹饪锅具',7,1,0,NULL,NULL),(63,'刀剪菜板',7,1,0,NULL,NULL),(64,'厨房配件',7,1,0,NULL,NULL),(65,'水具酒具',7,1,0,NULL,NULL),(66,'餐具',7,1,0,NULL,NULL),(67,'酒店用品',7,1,0,NULL,NULL),(68,'茶具/咖啡具',7,1,0,NULL,NULL),(69,'清洁用品',8,1,0,NULL,NULL),(70,'面部护肤',8,1,0,NULL,NULL),(71,'身体护理',8,1,0,NULL,NULL),(72,'口腔护理',8,1,0,NULL,NULL),(73,'女性护理',8,1,0,NULL,NULL),(74,'洗发护发',8,1,0,NULL,NULL),(75,'香水彩妆',8,1,0,NULL,NULL),(76,'女装',9,1,0,NULL,NULL),(77,'男装',9,1,0,NULL,NULL),(78,'内衣',9,1,0,NULL,NULL),(79,'洗衣服务',9,1,0,NULL,NULL),(80,'服饰配件',9,1,0,NULL,NULL),(81,'钟表',10,1,0,NULL,NULL),(82,'流行男鞋',11,1,0,NULL,NULL),(83,'时尚女鞋',11,1,0,NULL,NULL),(84,'奶粉',12,1,0,NULL,NULL),(85,'营养辅食',12,1,0,NULL,NULL),(86,'尿裤湿巾',12,1,0,NULL,NULL),(87,'喂养用品',12,1,0,NULL,NULL),(88,'洗护用品',12,1,0,NULL,NULL),(89,'童车童床',12,1,0,NULL,NULL),(90,'寝居服饰',12,1,0,NULL,NULL),(91,'妈妈专区',12,1,0,NULL,NULL),(92,'童装童鞋',12,1,0,NULL,NULL),(93,'安全座椅',12,1,0,NULL,NULL),(94,'潮流女包',13,1,0,NULL,NULL),(95,'精品男包',13,1,0,NULL,NULL),(96,'功能箱包',13,1,0,NULL,NULL),(97,'礼品',13,1,0,NULL,NULL),(98,'奢侈品',13,1,0,NULL,NULL),(99,'婚庆',13,1,0,NULL,NULL),(100,'进口食品',14,1,0,NULL,NULL),(101,'地方特产',14,1,0,NULL,NULL),(102,'休闲食品',14,1,0,NULL,NULL),(103,'粮油调味',14,1,0,NULL,NULL),(104,'饮料冲调',14,1,0,NULL,NULL),(105,'食品礼券',14,1,0,NULL,NULL),(106,'茗茶',14,1,0,NULL,NULL),(107,'时尚饰品',15,1,0,NULL,NULL),(108,'黄金',15,1,0,NULL,NULL),(109,'K金饰品',15,1,0,NULL,NULL),(110,'金银投资',15,1,0,NULL,NULL),(111,'银饰',15,1,0,NULL,NULL),(112,'钻石',15,1,0,NULL,NULL),(113,'翡翠玉石',15,1,0,NULL,NULL),(114,'水晶玛瑙',15,1,0,NULL,NULL),(115,'彩宝',15,1,0,NULL,NULL),(116,'铂金',15,1,0,NULL,NULL),(117,'木手串/把件',15,1,0,NULL,NULL),(118,'珍珠',15,1,0,NULL,NULL),(119,'维修保养',16,1,0,NULL,NULL),(120,'车载电器',16,1,0,NULL,NULL),(121,'美容清洗',16,1,0,NULL,NULL),(122,'汽车装饰',16,1,0,NULL,NULL),(123,'安全自驾',16,1,0,NULL,NULL),(124,'汽车服务',16,1,0,NULL,NULL),(125,'赛事改装',16,1,0,NULL,NULL),(126,'运动鞋包',17,1,0,NULL,NULL),(127,'运动服饰',17,1,0,NULL,NULL),(128,'骑行运动',17,1,0,NULL,NULL),(129,'垂钓用品',17,1,0,NULL,NULL),(130,'游泳用品',17,1,0,NULL,NULL),(131,'户外鞋服',17,1,0,NULL,NULL),(132,'户外装备',17,1,0,NULL,NULL),(133,'健身训练',17,1,0,NULL,NULL),(134,'体育用品',17,1,0,NULL,NULL),(135,'适用年龄',18,1,0,NULL,NULL),(136,'遥控/电动',18,1,0,NULL,NULL),(137,'毛绒布艺',18,1,0,NULL,NULL),(138,'娃娃玩具',18,1,0,NULL,NULL),(139,'模型玩具',18,1,0,NULL,NULL),(140,'健身玩具',18,1,0,NULL,NULL),(141,'动漫玩具',18,1,0,NULL,NULL),(142,'益智玩具',18,1,0,NULL,NULL),(143,'积木拼插',18,1,0,NULL,NULL),(144,'DIY玩具',18,1,0,NULL,NULL),(145,'创意减压',18,1,0,NULL,NULL),(146,'乐器',18,1,0,NULL,NULL),(147,'彩票',19,1,0,NULL,NULL),(148,'机票',19,1,0,NULL,NULL),(149,'酒店',19,1,0,NULL,NULL),(150,'旅行',19,1,0,NULL,NULL),(151,'充值',19,1,0,NULL,NULL),(152,'游戏',19,1,0,NULL,NULL),(153,'票务',19,1,0,NULL,NULL),(154,'产地直供',20,1,0,NULL,NULL),(155,'水果',20,1,0,NULL,NULL),(156,'猪牛羊肉',20,1,0,NULL,NULL),(157,'海鲜水产',20,1,0,NULL,NULL),(158,'禽肉蛋品',20,1,0,NULL,NULL),(159,'冷冻食品',20,1,0,NULL,NULL),(160,'熟食腊味',20,1,0,NULL,NULL),(161,'饮品甜品',20,1,0,NULL,NULL),(162,'蔬菜',20,1,0,NULL,NULL),(163,'全新整车',21,1,0,NULL,NULL),(164,'二手车',21,1,0,NULL,NULL),(165,'电子书',22,1,0,NULL,NULL),(166,'网络原创',22,1,0,NULL,NULL),(167,'数字杂志',22,1,0,NULL,NULL),(168,'多媒体图书',22,1,0,NULL,NULL),(169,'音乐',23,1,0,NULL,NULL),(170,'影视',23,1,0,NULL,NULL),(171,'教育音像',23,1,0,NULL,NULL),(172,'少儿',24,1,0,NULL,NULL),(173,'商务投资',24,1,0,NULL,NULL),(174,'英语学习与考试',24,1,0,NULL,NULL),(175,'文学',24,1,0,NULL,NULL),(176,'传记',24,1,0,NULL,NULL),(177,'励志',24,1,0,NULL,NULL),(178,'小说',25,1,0,NULL,NULL),(179,'文学',25,1,0,NULL,NULL),(180,'青春文学',25,1,0,NULL,NULL),(181,'传记',25,1,0,NULL,NULL),(182,'艺术',25,1,0,NULL,NULL),(183,'少儿',26,1,0,NULL,NULL),(184,'0-2岁',26,1,0,NULL,NULL),(185,'3-6岁',26,1,0,NULL,NULL),(186,'7-10岁',26,1,0,NULL,NULL),(187,'11-14岁',26,1,0,NULL,NULL),(188,'历史',27,1,0,NULL,NULL),(189,'哲学',27,1,0,NULL,NULL),(190,'国学',27,1,0,NULL,NULL),(191,'政治/军事',27,1,0,NULL,NULL),(192,'法律',27,1,0,NULL,NULL),(193,'人文社科',27,1,0,NULL,NULL),(194,'心理学',27,1,0,NULL,NULL),(195,'文化',27,1,0,NULL,NULL),(196,'社会科学',27,1,0,NULL,NULL),(197,'经济',28,1,0,NULL,NULL),(198,'金融与投资',28,1,0,NULL,NULL),(199,'管理',28,1,0,NULL,NULL),(200,'励志与成功',28,1,0,NULL,NULL),(201,'生活',29,1,0,NULL,NULL),(202,'健身与保健',29,1,0,NULL,NULL),(203,'家庭与育儿',29,1,0,NULL,NULL),(204,'旅游',29,1,0,NULL,NULL),(205,'烹饪美食',29,1,0,NULL,NULL),(206,'工业技术',30,1,0,NULL,NULL),(207,'科普读物',30,1,0,NULL,NULL),(208,'建筑',30,1,0,NULL,NULL),(209,'医学',30,1,0,NULL,NULL),(210,'科学与自然',30,1,0,NULL,NULL),(211,'计算机与互联网',30,1,0,NULL,NULL),(212,'电子通信',30,1,0,NULL,NULL),(213,'中小学教辅',31,1,0,NULL,NULL),(214,'教育与考试',31,1,0,NULL,NULL),(215,'外语学习',31,1,0,NULL,NULL),(216,'大中专教材',31,1,0,NULL,NULL),(217,'字典词典',31,1,0,NULL,NULL),(218,'艺术/设计/收藏',32,1,0,NULL,NULL),(219,'经济管理',32,1,0,NULL,NULL),(220,'文化/学术',32,1,0,NULL,NULL),(221,'少儿',32,1,0,NULL,NULL),(222,'工具书',33,1,0,NULL,NULL),(223,'杂志/期刊',33,1,0,NULL,NULL),(224,'套装书',33,1,0,NULL,NULL),(225,'国产手机',34,1,0,NULL,NULL),(226,'对讲机',34,1,0,NULL,NULL),(227,'合约机',35,1,0,NULL,NULL),(228,'选号中心',35,1,0,NULL,NULL),(229,'装宽带',35,1,0,NULL,NULL),(230,'办套餐',35,1,0,NULL,NULL),(231,'移动电源',36,1,0,NULL,NULL),(232,'电池/移动电源',36,1,0,NULL,NULL),(233,'蓝牙耳机',36,1,0,NULL,NULL),(234,'充电器/数据线',36,1,0,NULL,NULL),(235,'苹果周边',36,1,0,NULL,NULL),(236,'手机耳机',36,1,0,NULL,NULL),(237,'手机贴膜',36,1,0,NULL,NULL),(238,'手机存储卡',36,1,0,NULL,NULL),(239,'充电器',36,1,0,NULL,NULL),(240,'数据线',36,1,0,NULL,NULL),(241,'手机保护套',36,1,0,NULL,NULL),(242,'车载配件',36,1,0,NULL,NULL),(243,'iPhone 配件',36,1,0,NULL,NULL),(244,'手机电池',36,1,0,NULL,NULL),(245,'创意配件',36,1,0,NULL,NULL),(246,'便携/无线音响',36,1,0,NULL,NULL),(247,'手机饰品',36,1,0,NULL,NULL),(248,'拍照配件',36,1,0,NULL,NULL),(249,'手机支架',36,1,0,NULL,NULL),(250,'平板电视',37,1,0,NULL,NULL),(251,'空调',37,1,0,NULL,NULL),(252,'冰箱',37,1,0,NULL,NULL),(253,'洗衣机',37,1,0,NULL,NULL),(254,'家庭影院',37,1,0,NULL,NULL),(255,'DVD/电视盒子',37,1,0,NULL,NULL),(256,'迷你音响',37,1,0,NULL,NULL),(257,'冷柜/冰吧',37,1,0,NULL,NULL),(258,'家电配件',37,1,0,NULL,NULL),(259,'功放',37,1,0,NULL,NULL),(260,'回音壁/Soundbar',37,1,0,NULL,NULL),(261,'Hi-Fi专区',37,1,0,NULL,NULL),(262,'电视盒子',37,1,0,NULL,NULL),(263,'酒柜',37,1,0,NULL,NULL),(264,'燃气灶',38,1,0,NULL,NULL),(265,'油烟机',38,1,0,NULL,NULL),(266,'热水器',38,1,0,NULL,NULL),(267,'消毒柜',38,1,0,NULL,NULL),(268,'洗碗机',38,1,0,NULL,NULL),(269,'料理机',39,1,0,NULL,NULL),(270,'榨汁机',39,1,0,NULL,NULL),(271,'电饭煲',39,1,0,NULL,NULL),(272,'电压力锅',39,1,0,NULL,NULL),(273,'豆浆机',39,1,0,NULL,NULL),(274,'咖啡机',39,1,0,NULL,NULL),(275,'微波炉',39,1,0,NULL,NULL),(276,'电烤箱',39,1,0,NULL,NULL),(277,'电磁炉',39,1,0,NULL,NULL),(278,'面包机',39,1,0,NULL,NULL),(279,'煮蛋器',39,1,0,NULL,NULL),(280,'酸奶机',39,1,0,NULL,NULL),(281,'电炖锅',39,1,0,NULL,NULL),(282,'电水壶/热水瓶',39,1,0,NULL,NULL),(283,'电饼铛',39,1,0,NULL,NULL),(284,'多用途锅',39,1,0,NULL,NULL),(285,'电烧烤炉',39,1,0,NULL,NULL),(286,'果蔬解毒机',39,1,0,NULL,NULL),(287,'其它厨房电器',39,1,0,NULL,NULL),(288,'养生壶/煎药壶',39,1,0,NULL,NULL),(289,'电热饭盒',39,1,0,NULL,NULL),(290,'取暖电器',40,1,0,NULL,NULL),(291,'净化器',40,1,0,NULL,NULL),(292,'加湿器',40,1,0,NULL,NULL),(293,'扫地机器人',40,1,0,NULL,NULL),(294,'吸尘器',40,1,0,NULL,NULL),(295,'挂烫机/熨斗',40,1,0,NULL,NULL),(296,'插座',40,1,0,NULL,NULL),(297,'电话机',40,1,0,NULL,NULL),(298,'清洁机',40,1,0,NULL,NULL),(299,'除湿机',40,1,0,NULL,NULL),(300,'干衣机',40,1,0,NULL,NULL),(301,'收录/音机',40,1,0,NULL,NULL),(302,'电风扇',40,1,0,NULL,NULL),(303,'冷风扇',40,1,0,NULL,NULL),(304,'其它生活电器',40,1,0,NULL,NULL),(305,'生活电器配件',40,1,0,NULL,NULL),(306,'净水器',40,1,0,NULL,NULL),(307,'饮水机',40,1,0,NULL,NULL),(308,'剃须刀',41,1,0,NULL,NULL),(309,'剃/脱毛器',41,1,0,NULL,NULL),(310,'口腔护理',41,1,0,NULL,NULL),(311,'电吹风',41,1,0,NULL,NULL),(312,'美容器',41,1,0,NULL,NULL),(313,'理发器',41,1,0,NULL,NULL),(314,'卷/直发器',41,1,0,NULL,NULL),(315,'按摩椅',41,1,0,NULL,NULL),(316,'按摩器',41,1,0,NULL,NULL),(317,'足浴盆',41,1,0,NULL,NULL),(318,'血压计',41,1,0,NULL,NULL),(319,'电子秤/厨房秤',41,1,0,NULL,NULL),(320,'血糖仪',41,1,0,NULL,NULL),(321,'体温计',41,1,0,NULL,NULL),(322,'其它健康电器',41,1,0,NULL,NULL),(323,'计步器/脂肪检测仪',41,1,0,NULL,NULL),(324,'电动工具',42,1,0,NULL,NULL),(325,'手动工具',42,1,0,NULL,NULL),(326,'仪器仪表',42,1,0,NULL,NULL),(327,'浴霸/排气扇',42,1,0,NULL,NULL),(328,'灯具',42,1,0,NULL,NULL),(329,'LED灯',42,1,0,NULL,NULL),(330,'洁身器',42,1,0,NULL,NULL),(331,'水槽',42,1,0,NULL,NULL),(332,'龙头',42,1,0,NULL,NULL),(333,'淋浴花洒',42,1,0,NULL,NULL),(334,'厨卫五金',42,1,0,NULL,NULL),(335,'家具五金',42,1,0,NULL,NULL),(336,'门铃',42,1,0,NULL,NULL),(337,'电气开关',42,1,0,NULL,NULL),(338,'插座',42,1,0,NULL,NULL),(339,'电工电料',42,1,0,NULL,NULL),(340,'监控安防',42,1,0,NULL,NULL),(341,'电线/线缆',42,1,0,NULL,NULL),(342,'数码相机',43,1,0,NULL,NULL),(343,'单电/微单相机',43,1,0,NULL,NULL),(344,'单反相机',43,1,0,NULL,NULL),(345,'摄像机',43,1,0,NULL,NULL),(346,'拍立得',43,1,0,NULL,NULL),(347,'运动相机',43,1,0,NULL,NULL),(348,'镜头',43,1,0,NULL,NULL),(349,'户外器材',43,1,0,NULL,NULL),(350,'影棚器材',43,1,0,NULL,NULL),(351,'冲印服务',43,1,0,NULL,NULL),(352,'数码相框',43,1,0,NULL,NULL),(353,'存储卡',44,1,0,NULL,NULL),(354,'读卡器',44,1,0,NULL,NULL),(355,'滤镜',44,1,0,NULL,NULL),(356,'闪光灯/手柄',44,1,0,NULL,NULL),(357,'相机包',44,1,0,NULL,NULL),(358,'三脚架/云台',44,1,0,NULL,NULL),(359,'相机清洁/贴膜',44,1,0,NULL,NULL),(360,'机身附件',44,1,0,NULL,NULL),(361,'镜头附件',44,1,0,NULL,NULL),(362,'电池/充电器',44,1,0,NULL,NULL),(363,'移动电源',44,1,0,NULL,NULL),(364,'数码支架',44,1,0,NULL,NULL),(365,'智能手环',45,1,0,NULL,NULL),(366,'智能手表',45,1,0,NULL,NULL),(367,'智能眼镜',45,1,0,NULL,NULL),(368,'运动跟踪器',45,1,0,NULL,NULL),(369,'健康监测',45,1,0,NULL,NULL),(370,'智能配饰',45,1,0,NULL,NULL),(371,'智能家居',45,1,0,NULL,NULL),(372,'体感车',45,1,0,NULL,NULL),(373,'其他配件',45,1,0,NULL,NULL),(374,'智能机器人',45,1,0,NULL,NULL),(375,'无人机',45,1,0,NULL,NULL),(376,'MP3/MP4',46,1,0,NULL,NULL),(377,'智能设备',46,1,0,NULL,NULL),(378,'耳机/耳麦',46,1,0,NULL,NULL),(379,'便携/无线音箱',46,1,0,NULL,NULL),(380,'音箱/音响',46,1,0,NULL,NULL),(381,'高清播放器',46,1,0,NULL,NULL),(382,'收音机',46,1,0,NULL,NULL),(383,'MP3/MP4配件',46,1,0,NULL,NULL),(384,'麦克风',46,1,0,NULL,NULL),(385,'专业音频',46,1,0,NULL,NULL),(386,'苹果配件',46,1,0,NULL,NULL),(387,'学生平板',47,1,0,NULL,NULL),(388,'点读机/笔',47,1,0,NULL,NULL),(389,'早教益智',47,1,0,NULL,NULL),(390,'录音笔',47,1,0,NULL,NULL),(391,'电纸书',47,1,0,NULL,NULL),(392,'电子词典',47,1,0,NULL,NULL),(393,'复读机',47,1,0,NULL,NULL),(394,'延保服务',48,1,0,NULL,NULL),(395,'杀毒软件',48,1,0,NULL,NULL),(396,'积分商品',48,1,0,NULL,NULL),(397,'桌布/罩件',49,1,0,NULL,NULL),(398,'地毯地垫',49,1,0,NULL,NULL),(399,'沙发垫套/椅垫',49,1,0,NULL,NULL),(400,'床品套件',49,1,0,NULL,NULL),(401,'被子',49,1,0,NULL,NULL),(402,'枕芯',49,1,0,NULL,NULL),(403,'床单被罩',49,1,0,NULL,NULL),(404,'毯子',49,1,0,NULL,NULL),(405,'床垫/床褥',49,1,0,NULL,NULL),(406,'蚊帐',49,1,0,NULL,NULL),(407,'抱枕靠垫',49,1,0,NULL,NULL),(408,'毛巾浴巾',49,1,0,NULL,NULL),(409,'电热毯',49,1,0,NULL,NULL),(410,'窗帘/窗纱',49,1,0,NULL,NULL),(411,'布艺软饰',49,1,0,NULL,NULL),(412,'凉席',49,1,0,NULL,NULL),(413,'台灯',50,1,0,NULL,NULL),(414,'节能灯',50,1,0,NULL,NULL),(415,'装饰灯',50,1,0,NULL,NULL),(416,'落地灯',50,1,0,NULL,NULL),(417,'应急灯/手电',50,1,0,NULL,NULL),(418,'LED灯',50,1,0,NULL,NULL),(419,'吸顶灯',50,1,0,NULL,NULL),(420,'五金电器',50,1,0,NULL,NULL),(421,'筒灯射灯',50,1,0,NULL,NULL),(422,'吊灯',50,1,0,NULL,NULL),(423,'氛围照明',50,1,0,NULL,NULL),(424,'保暖防护',51,1,0,NULL,NULL),(425,'收纳用品',51,1,0,NULL,NULL),(426,'雨伞雨具',51,1,0,NULL,NULL),(427,'浴室用品',51,1,0,NULL,NULL),(428,'缝纫/针织用品',51,1,0,NULL,NULL),(429,'洗晒/熨烫',51,1,0,NULL,NULL),(430,'净化除味',51,1,0,NULL,NULL),(431,'相框/照片墙',52,1,0,NULL,NULL),(432,'装饰字画',52,1,0,NULL,NULL),(433,'节庆饰品',52,1,0,NULL,NULL),(434,'手工/十字绣',52,1,0,NULL,NULL),(435,'装饰摆件',52,1,0,NULL,NULL),(436,'帘艺隔断',52,1,0,NULL,NULL),(437,'墙贴/装饰贴',52,1,0,NULL,NULL),(438,'钟饰',52,1,0,NULL,NULL),(439,'花瓶花艺',52,1,0,NULL,NULL),(440,'香薰蜡烛',52,1,0,NULL,NULL),(441,'创意家居',52,1,0,NULL,NULL),(442,'宠物主粮',53,1,0,NULL,NULL),(443,'宠物零食',53,1,0,NULL,NULL),(444,'医疗保健',53,1,0,NULL,NULL),(445,'家居日用',53,1,0,NULL,NULL),(446,'宠物玩具',53,1,0,NULL,NULL),(447,'出行装备',53,1,0,NULL,NULL),(448,'洗护美容',53,1,0,NULL,NULL),(449,'笔记本',54,1,0,NULL,NULL),(450,'超极本',54,1,0,NULL,NULL),(451,'游戏本',54,1,0,NULL,NULL),(452,'平板电脑',54,1,0,NULL,NULL),(453,'平板电脑配件',54,1,0,NULL,NULL),(454,'台式机',54,1,0,NULL,NULL),(455,'服务器/工作站',54,1,0,NULL,NULL),(456,'笔记本配件',54,1,0,NULL,NULL),(457,'一体机',54,1,0,NULL,NULL),(458,'CPU',55,1,0,NULL,NULL),(459,'主板',55,1,0,NULL,NULL),(460,'显卡',55,1,0,NULL,NULL),(461,'硬盘',55,1,0,NULL,NULL),(462,'SSD固态硬盘',55,1,0,NULL,NULL),(463,'内存',55,1,0,NULL,NULL),(464,'机箱',55,1,0,NULL,NULL),(465,'电源',55,1,0,NULL,NULL),(466,'显示器',55,1,0,NULL,NULL),(467,'刻录机/光驱',55,1,0,NULL,NULL),(468,'散热器',55,1,0,NULL,NULL),(469,'声卡/扩展卡',55,1,0,NULL,NULL),(470,'装机配件',55,1,0,NULL,NULL),(471,'组装电脑',55,1,0,NULL,NULL),(472,'移动硬盘',56,1,0,NULL,NULL),(473,'U盘',56,1,0,NULL,NULL),(474,'鼠标',56,1,0,NULL,NULL),(475,'键盘',56,1,0,NULL,NULL),(476,'鼠标垫',56,1,0,NULL,NULL),(477,'摄像头',56,1,0,NULL,NULL),(478,'手写板',56,1,0,NULL,NULL),(479,'硬盘盒',56,1,0,NULL,NULL),(480,'插座',56,1,0,NULL,NULL),(481,'线缆',56,1,0,NULL,NULL),(482,'UPS电源',56,1,0,NULL,NULL),(483,'电脑工具',56,1,0,NULL,NULL),(484,'游戏设备',56,1,0,NULL,NULL),(485,'电玩',56,1,0,NULL,NULL),(486,'电脑清洁',56,1,0,NULL,NULL),(487,'网络仪表仪器',56,1,0,NULL,NULL),(488,'游戏机',57,1,0,NULL,NULL),(489,'游戏耳机',57,1,0,NULL,NULL),(490,'手柄/方向盘',57,1,0,NULL,NULL),(491,'游戏软件',57,1,0,NULL,NULL),(492,'游戏周边',57,1,0,NULL,NULL),(493,'路由器',58,1,0,NULL,NULL),(494,'网卡',58,1,0,NULL,NULL),(495,'交换机',58,1,0,NULL,NULL),(496,'网络存储',58,1,0,NULL,NULL),(497,'4G/3G上网',58,1,0,NULL,NULL),(498,'网络盒子',58,1,0,NULL,NULL),(499,'网络配件',58,1,0,NULL,NULL),(500,'投影机',59,1,0,NULL,NULL),(501,'投影配件',59,1,0,NULL,NULL),(502,'多功能一体机',59,1,0,NULL,NULL),(503,'打印机',59,1,0,NULL,NULL),(504,'传真设备',59,1,0,NULL,NULL),(505,'验钞/点钞机',59,1,0,NULL,NULL),(506,'扫描设备',59,1,0,NULL,NULL),(507,'复合机',59,1,0,NULL,NULL),(508,'碎纸机',59,1,0,NULL,NULL),(509,'考勤机',59,1,0,NULL,NULL),(510,'收款/POS机',59,1,0,NULL,NULL),(511,'会议音频视频',59,1,0,NULL,NULL),(512,'保险柜',59,1,0,NULL,NULL),(513,'装订/封装机',59,1,0,NULL,NULL),(514,'安防监控',59,1,0,NULL,NULL),(515,'办公家具',59,1,0,NULL,NULL),(516,'白板',59,1,0,NULL,NULL),(517,'硒鼓/墨粉',60,1,0,NULL,NULL),(518,'墨盒',60,1,0,NULL,NULL),(519,'色带',60,1,0,NULL,NULL),(520,'纸类',60,1,0,NULL,NULL),(521,'办公文具',60,1,0,NULL,NULL),(522,'学生文具',60,1,0,NULL,NULL),(523,'财会用品',60,1,0,NULL,NULL),(524,'文件管理',60,1,0,NULL,NULL),(525,'本册/便签',60,1,0,NULL,NULL),(526,'计算器',60,1,0,NULL,NULL),(527,'笔类',60,1,0,NULL,NULL),(528,'画具画材',60,1,0,NULL,NULL),(529,'刻录碟片/附件',60,1,0,NULL,NULL),(530,'上门安装',61,1,0,NULL,NULL),(531,'延保服务',61,1,0,NULL,NULL),(532,'维修保养',61,1,0,NULL,NULL),(533,'电脑软件',61,1,0,NULL,NULL),(534,'京东服务',61,1,0,NULL,NULL),(535,'炒锅',62,1,0,NULL,NULL),(536,'煎锅',62,1,0,NULL,NULL),(537,'压力锅',62,1,0,NULL,NULL),(538,'蒸锅',62,1,0,NULL,NULL),(539,'汤锅',62,1,0,NULL,NULL),(540,'奶锅',62,1,0,NULL,NULL),(541,'锅具套装',62,1,0,NULL,NULL),(542,'煲类',62,1,0,NULL,NULL),(543,'水壶',62,1,0,NULL,NULL),(544,'火锅',62,1,0,NULL,NULL),(545,'菜刀',63,1,0,NULL,NULL),(546,'剪刀',63,1,0,NULL,NULL),(547,'刀具套装',63,1,0,NULL,NULL),(548,'砧板',63,1,0,NULL,NULL),(549,'瓜果刀/刨',63,1,0,NULL,NULL),(550,'多功能刀',63,1,0,NULL,NULL),(551,'保鲜盒',64,1,0,NULL,NULL),(552,'烘焙/烧烤',64,1,0,NULL,NULL),(553,'饭盒/提锅',64,1,0,NULL,NULL),(554,'储物/置物架',64,1,0,NULL,NULL),(555,'厨房DIY/小工具',64,1,0,NULL,NULL),(556,'塑料杯',65,1,0,NULL,NULL),(557,'运动水壶',65,1,0,NULL,NULL),(558,'玻璃杯',65,1,0,NULL,NULL),(559,'陶瓷/马克杯',65,1,0,NULL,NULL),(560,'保温杯',65,1,0,NULL,NULL),(561,'保温壶',65,1,0,NULL,NULL),(562,'酒杯/酒具',65,1,0,NULL,NULL),(563,'杯具套装',65,1,0,NULL,NULL),(564,'餐具套装',66,1,0,NULL,NULL),(565,'碗/碟/盘',66,1,0,NULL,NULL),(566,'筷勺/刀叉',66,1,0,NULL,NULL),(567,'一次性用品',66,1,0,NULL,NULL),(568,'果盘/果篮',66,1,0,NULL,NULL),(569,'自助餐炉',67,1,0,NULL,NULL),(570,'酒店餐具',67,1,0,NULL,NULL),(571,'酒店水具',67,1,0,NULL,NULL),(572,'整套茶具',68,1,0,NULL,NULL),(573,'茶杯',68,1,0,NULL,NULL),(574,'茶壶',68,1,0,NULL,NULL),(575,'茶盘茶托',68,1,0,NULL,NULL),(576,'茶叶罐',68,1,0,NULL,NULL),(577,'茶具配件',68,1,0,NULL,NULL),(578,'茶宠摆件',68,1,0,NULL,NULL),(579,'咖啡具',68,1,0,NULL,NULL),(580,'其他',68,1,0,NULL,NULL),(581,'纸品湿巾',69,1,0,NULL,NULL),(582,'衣物清洁',69,1,0,NULL,NULL),(583,'清洁工具',69,1,0,NULL,NULL),(584,'驱虫用品',69,1,0,NULL,NULL),(585,'家庭清洁',69,1,0,NULL,NULL),(586,'皮具护理',69,1,0,NULL,NULL),(587,'一次性用品',69,1,0,NULL,NULL),(588,'洁面',70,1,0,NULL,NULL),(589,'乳液面霜',70,1,0,NULL,NULL),(590,'面膜',70,1,0,NULL,NULL),(591,'剃须',70,1,0,NULL,NULL),(592,'套装',70,1,0,NULL,NULL),(593,'精华',70,1,0,NULL,NULL),(594,'眼霜',70,1,0,NULL,NULL),(595,'卸妆',70,1,0,NULL,NULL),(596,'防晒',70,1,0,NULL,NULL),(597,'防晒隔离',70,1,0,NULL,NULL),(598,'T区护理',70,1,0,NULL,NULL),(599,'眼部护理',70,1,0,NULL,NULL),(600,'精华露',70,1,0,NULL,NULL),(601,'爽肤水',70,1,0,NULL,NULL),(602,'沐浴',71,1,0,NULL,NULL),(603,'润肤',71,1,0,NULL,NULL),(604,'颈部',71,1,0,NULL,NULL),(605,'手足',71,1,0,NULL,NULL),(606,'纤体塑形',71,1,0,NULL,NULL),(607,'美胸',71,1,0,NULL,NULL),(608,'套装',71,1,0,NULL,NULL),(609,'精油',71,1,0,NULL,NULL),(610,'洗发护发',71,1,0,NULL,NULL),(611,'染发/造型',71,1,0,NULL,NULL),(612,'香薰精油',71,1,0,NULL,NULL),(613,'磨砂/浴盐',71,1,0,NULL,NULL),(614,'手工/香皂',71,1,0,NULL,NULL),(615,'洗发',71,1,0,NULL,NULL),(616,'护发',71,1,0,NULL,NULL),(617,'染发',71,1,0,NULL,NULL),(618,'磨砂膏',71,1,0,NULL,NULL),(619,'香皂',71,1,0,NULL,NULL),(620,'牙膏/牙粉',72,1,0,NULL,NULL),(621,'牙刷/牙线',72,1,0,NULL,NULL),(622,'漱口水',72,1,0,NULL,NULL),(623,'套装',72,1,0,NULL,NULL),(624,'卫生巾',73,1,0,NULL,NULL),(625,'卫生护垫',73,1,0,NULL,NULL),(626,'私密护理',73,1,0,NULL,NULL),(627,'脱毛膏',73,1,0,NULL,NULL),(628,'其他',73,1,0,NULL,NULL),(629,'洗发',74,1,0,NULL,NULL),(630,'护发',74,1,0,NULL,NULL),(631,'染发',74,1,0,NULL,NULL),(632,'造型',74,1,0,NULL,NULL),(633,'假发',74,1,0,NULL,NULL),(634,'套装',74,1,0,NULL,NULL),(635,'美发工具',74,1,0,NULL,NULL),(636,'脸部护理',74,1,0,NULL,NULL),(637,'香水',75,1,0,NULL,NULL),(638,'底妆',75,1,0,NULL,NULL),(639,'腮红',75,1,0,NULL,NULL),(640,'眼影',75,1,0,NULL,NULL),(641,'唇部',75,1,0,NULL,NULL),(642,'美甲',75,1,0,NULL,NULL),(643,'眼线',75,1,0,NULL,NULL),(644,'美妆工具',75,1,0,NULL,NULL),(645,'套装',75,1,0,NULL,NULL),(646,'防晒隔离',75,1,0,NULL,NULL),(647,'卸妆',75,1,0,NULL,NULL),(648,'眉笔',75,1,0,NULL,NULL),(649,'睫毛膏',75,1,0,NULL,NULL),(650,'T恤',76,1,0,NULL,NULL),(651,'衬衫',76,1,0,NULL,NULL),(652,'针织衫',76,1,0,NULL,NULL),(653,'雪纺衫',76,1,0,NULL,NULL),(654,'卫衣',76,1,0,NULL,NULL),(655,'马甲',76,1,0,NULL,NULL),(656,'连衣裙',76,1,0,NULL,NULL),(657,'半身裙',76,1,0,NULL,NULL),(658,'牛仔裤',76,1,0,NULL,NULL),(659,'休闲裤',76,1,0,NULL,NULL),(660,'打底裤',76,1,0,NULL,NULL),(661,'正装裤',76,1,0,NULL,NULL),(662,'小西装',76,1,0,NULL,NULL),(663,'短外套',76,1,0,NULL,NULL),(664,'风衣',76,1,0,NULL,NULL),(665,'毛呢大衣',76,1,0,NULL,NULL),(666,'真皮皮衣',76,1,0,NULL,NULL),(667,'棉服',76,1,0,NULL,NULL),(668,'羽绒服',76,1,0,NULL,NULL),(669,'大码女装',76,1,0,NULL,NULL),(670,'中老年女装',76,1,0,NULL,NULL),(671,'婚纱',76,1,0,NULL,NULL),(672,'打底衫',76,1,0,NULL,NULL),(673,'旗袍/唐装',76,1,0,NULL,NULL),(674,'加绒裤',76,1,0,NULL,NULL),(675,'吊带/背心',76,1,0,NULL,NULL),(676,'羊绒衫',76,1,0,NULL,NULL),(677,'短裤',76,1,0,NULL,NULL),(678,'皮草',76,1,0,NULL,NULL),(679,'礼服',76,1,0,NULL,NULL),(680,'仿皮皮衣',76,1,0,NULL,NULL),(681,'羊毛衫',76,1,0,NULL,NULL),(682,'设计师/潮牌',76,1,0,NULL,NULL),(683,'衬衫',77,1,0,NULL,NULL),(684,'T恤',77,1,0,NULL,NULL),(685,'POLO衫',77,1,0,NULL,NULL),(686,'针织衫',77,1,0,NULL,NULL),(687,'羊绒衫',77,1,0,NULL,NULL),(688,'卫衣',77,1,0,NULL,NULL),(689,'马甲/背心',77,1,0,NULL,NULL),(690,'夹克',77,1,0,NULL,NULL),(691,'风衣',77,1,0,NULL,NULL),(692,'毛呢大衣',77,1,0,NULL,NULL),(693,'仿皮皮衣',77,1,0,NULL,NULL),(694,'西服',77,1,0,NULL,NULL),(695,'棉服',77,1,0,NULL,NULL),(696,'羽绒服',77,1,0,NULL,NULL),(697,'牛仔裤',77,1,0,NULL,NULL),(698,'休闲裤',77,1,0,NULL,NULL),(699,'西裤',77,1,0,NULL,NULL),(700,'西服套装',77,1,0,NULL,NULL),(701,'大码男装',77,1,0,NULL,NULL),(702,'中老年男装',77,1,0,NULL,NULL),(703,'唐装/中山装',77,1,0,NULL,NULL),(704,'工装',77,1,0,NULL,NULL),(705,'真皮皮衣',77,1,0,NULL,NULL),(706,'加绒裤',77,1,0,NULL,NULL),(707,'卫裤/运动裤',77,1,0,NULL,NULL),(708,'短裤',77,1,0,NULL,NULL),(709,'设计师/潮牌',77,1,0,NULL,NULL),(710,'羊毛衫',77,1,0,NULL,NULL),(711,'文胸',78,1,0,NULL,NULL),(712,'女式内裤',78,1,0,NULL,NULL),(713,'男式内裤',78,1,0,NULL,NULL),(714,'睡衣/家居服',78,1,0,NULL,NULL),(715,'塑身美体',78,1,0,NULL,NULL),(716,'泳衣',78,1,0,NULL,NULL),(717,'吊带/背心',78,1,0,NULL,NULL),(718,'抹胸',78,1,0,NULL,NULL),(719,'连裤袜/丝袜',78,1,0,NULL,NULL),(720,'美腿袜',78,1,0,NULL,NULL),(721,'商务男袜',78,1,0,NULL,NULL),(722,'保暖内衣',78,1,0,NULL,NULL),(723,'情侣睡衣',78,1,0,NULL,NULL),(724,'文胸套装',78,1,0,NULL,NULL),(725,'少女文胸',78,1,0,NULL,NULL),(726,'休闲棉袜',78,1,0,NULL,NULL),(727,'大码内衣',78,1,0,NULL,NULL),(728,'内衣配件',78,1,0,NULL,NULL),(729,'打底裤袜',78,1,0,NULL,NULL),(730,'打底衫',78,1,0,NULL,NULL),(731,'秋衣秋裤',78,1,0,NULL,NULL),(732,'情趣内衣',78,1,0,NULL,NULL),(733,'服装洗护',79,1,0,NULL,NULL),(734,'太阳镜',80,1,0,NULL,NULL),(735,'光学镜架/镜片',80,1,0,NULL,NULL),(736,'围巾/手套/帽子套装',80,1,0,NULL,NULL),(737,'袖扣',80,1,0,NULL,NULL),(738,'棒球帽',80,1,0,NULL,NULL),(739,'毛线帽',80,1,0,NULL,NULL),(740,'遮阳帽',80,1,0,NULL,NULL),(741,'老花镜',80,1,0,NULL,NULL),(742,'装饰眼镜',80,1,0,NULL,NULL),(743,'防辐射眼镜',80,1,0,NULL,NULL),(744,'游泳镜',80,1,0,NULL,NULL),(745,'女士丝巾/围巾/披肩',80,1,0,NULL,NULL),(746,'男士丝巾/围巾',80,1,0,NULL,NULL),(747,'鸭舌帽',80,1,0,NULL,NULL),(748,'贝雷帽',80,1,0,NULL,NULL),(749,'礼帽',80,1,0,NULL,NULL),(750,'真皮手套',80,1,0,NULL,NULL),(751,'毛线手套',80,1,0,NULL,NULL),(752,'防晒手套',80,1,0,NULL,NULL),(753,'男士腰带/礼盒',80,1,0,NULL,NULL),(754,'女士腰带/礼盒',80,1,0,NULL,NULL),(755,'钥匙扣',80,1,0,NULL,NULL),(756,'遮阳伞/雨伞',80,1,0,NULL,NULL),(757,'口罩',80,1,0,NULL,NULL),(758,'耳罩/耳包',80,1,0,NULL,NULL),(759,'假领',80,1,0,NULL,NULL),(760,'毛线/布面料',80,1,0,NULL,NULL),(761,'领带/领结/领带夹',80,1,0,NULL,NULL),(762,'男表',81,1,0,NULL,NULL),(763,'瑞表',81,1,0,NULL,NULL),(764,'女表',81,1,0,NULL,NULL),(765,'国表',81,1,0,NULL,NULL),(766,'日韩表',81,1,0,NULL,NULL),(767,'欧美表',81,1,0,NULL,NULL),(768,'德表',81,1,0,NULL,NULL),(769,'儿童手表',81,1,0,NULL,NULL),(770,'智能手表',81,1,0,NULL,NULL),(771,'闹钟',81,1,0,NULL,NULL),(772,'座钟挂钟',81,1,0,NULL,NULL),(773,'钟表配件',81,1,0,NULL,NULL),(774,'商务休闲鞋',82,1,0,NULL,NULL),(775,'正装鞋',82,1,0,NULL,NULL),(776,'休闲鞋',82,1,0,NULL,NULL),(777,'凉鞋/沙滩鞋',82,1,0,NULL,NULL),(778,'男靴',82,1,0,NULL,NULL),(779,'功能鞋',82,1,0,NULL,NULL),(780,'拖鞋/人字拖',82,1,0,NULL,NULL),(781,'雨鞋/雨靴',82,1,0,NULL,NULL),(782,'传统布鞋',82,1,0,NULL,NULL),(783,'鞋配件',82,1,0,NULL,NULL),(784,'帆布鞋',82,1,0,NULL,NULL),(785,'增高鞋',82,1,0,NULL,NULL),(786,'工装鞋',82,1,0,NULL,NULL),(787,'定制鞋',82,1,0,NULL,NULL),(788,'高跟鞋',83,1,0,NULL,NULL),(789,'单鞋',83,1,0,NULL,NULL),(790,'休闲鞋',83,1,0,NULL,NULL),(791,'凉鞋',83,1,0,NULL,NULL),(792,'女靴',83,1,0,NULL,NULL),(793,'雪地靴',83,1,0,NULL,NULL),(794,'拖鞋/人字拖',83,1,0,NULL,NULL),(795,'踝靴',83,1,0,NULL,NULL),(796,'筒靴',83,1,0,NULL,NULL),(797,'帆布鞋',83,1,0,NULL,NULL),(798,'雨鞋/雨靴',83,1,0,NULL,NULL),(799,'妈妈鞋',83,1,0,NULL,NULL),(800,'鞋配件',83,1,0,NULL,NULL),(801,'特色鞋',83,1,0,NULL,NULL),(802,'鱼嘴鞋',83,1,0,NULL,NULL),(803,'布鞋/绣花鞋',83,1,0,NULL,NULL),(804,'马丁靴',83,1,0,NULL,NULL),(805,'坡跟鞋',83,1,0,NULL,NULL),(806,'松糕鞋',83,1,0,NULL,NULL),(807,'内增高',83,1,0,NULL,NULL),(808,'防水台',83,1,0,NULL,NULL),(809,'婴幼奶粉',84,1,0,NULL,NULL),(810,'孕妈奶粉',84,1,0,NULL,NULL),(811,'益生菌/初乳',85,1,0,NULL,NULL),(812,'米粉/菜粉',85,1,0,NULL,NULL),(813,'果泥/果汁',85,1,0,NULL,NULL),(814,'DHA',85,1,0,NULL,NULL),(815,'宝宝零食',85,1,0,NULL,NULL),(816,'钙铁锌/维生素',85,1,0,NULL,NULL),(817,'清火/开胃',85,1,0,NULL,NULL),(818,'面条/粥',85,1,0,NULL,NULL),(819,'婴儿尿裤',86,1,0,NULL,NULL),(820,'拉拉裤',86,1,0,NULL,NULL),(821,'婴儿湿巾',86,1,0,NULL,NULL),(822,'成人尿裤',86,1,0,NULL,NULL),(823,'奶瓶奶嘴',87,1,0,NULL,NULL),(824,'吸奶器',87,1,0,NULL,NULL),(825,'暖奶消毒',87,1,0,NULL,NULL),(826,'儿童餐具',87,1,0,NULL,NULL),(827,'水壶/水杯',87,1,0,NULL,NULL),(828,'牙胶安抚',87,1,0,NULL,NULL),(829,'围兜/防溅衣',87,1,0,NULL,NULL),(830,'辅食料理机',87,1,0,NULL,NULL),(831,'食物存储',87,1,0,NULL,NULL),(832,'宝宝护肤',88,1,0,NULL,NULL),(833,'洗发沐浴',88,1,0,NULL,NULL),(834,'奶瓶清洗',88,1,0,NULL,NULL),(835,'驱蚊防晒',88,1,0,NULL,NULL),(836,'理发器',88,1,0,NULL,NULL),(837,'洗澡用具',88,1,0,NULL,NULL),(838,'婴儿口腔清洁',88,1,0,NULL,NULL),(839,'洗衣液/皂',88,1,0,NULL,NULL),(840,'日常护理',88,1,0,NULL,NULL),(841,'座便器',88,1,0,NULL,NULL),(842,'婴儿推车',89,1,0,NULL,NULL),(843,'餐椅摇椅',89,1,0,NULL,NULL),(844,'婴儿床',89,1,0,NULL,NULL),(845,'学步车',89,1,0,NULL,NULL),(846,'三轮车',89,1,0,NULL,NULL),(847,'自行车',89,1,0,NULL,NULL),(848,'电动车',89,1,0,NULL,NULL),(849,'扭扭车',89,1,0,NULL,NULL),(850,'滑板车',89,1,0,NULL,NULL),(851,'婴儿床垫',89,1,0,NULL,NULL),(852,'婴儿外出服',90,1,0,NULL,NULL),(853,'婴儿内衣',90,1,0,NULL,NULL),(854,'婴儿礼盒',90,1,0,NULL,NULL),(855,'婴儿鞋帽袜',90,1,0,NULL,NULL),(856,'安全防护',90,1,0,NULL,NULL),(857,'家居床品',90,1,0,NULL,NULL),(858,'睡袋/抱被',90,1,0,NULL,NULL),(859,'爬行垫',90,1,0,NULL,NULL),(860,'妈咪包/背婴带',91,1,0,NULL,NULL),(861,'产后塑身',91,1,0,NULL,NULL),(862,'文胸/内裤',91,1,0,NULL,NULL),(863,'防辐射服',91,1,0,NULL,NULL),(864,'孕妈装',91,1,0,NULL,NULL),(865,'孕期营养',91,1,0,NULL,NULL),(866,'孕妇护肤',91,1,0,NULL,NULL),(867,'待产护理',91,1,0,NULL,NULL),(868,'月子装',91,1,0,NULL,NULL),(869,'防溢乳垫',91,1,0,NULL,NULL),(870,'套装',92,1,0,NULL,NULL),(871,'上衣',92,1,0,NULL,NULL),(872,'裤子',92,1,0,NULL,NULL),(873,'裙子',92,1,0,NULL,NULL),(874,'内衣/家居服',92,1,0,NULL,NULL),(875,'羽绒服/棉服',92,1,0,NULL,NULL),(876,'亲子装',92,1,0,NULL,NULL),(877,'儿童配饰',92,1,0,NULL,NULL),(878,'礼服/演出服',92,1,0,NULL,NULL),(879,'运动鞋',92,1,0,NULL,NULL),(880,'皮鞋/帆布鞋',92,1,0,NULL,NULL),(881,'靴子',92,1,0,NULL,NULL),(882,'凉鞋',92,1,0,NULL,NULL),(883,'功能鞋',92,1,0,NULL,NULL),(884,'户外/运动服',92,1,0,NULL,NULL),(885,'提篮式',93,1,0,NULL,NULL),(886,'安全座椅',93,1,0,NULL,NULL),(887,'增高垫',93,1,0,NULL,NULL),(888,'钱包',94,1,0,NULL,NULL),(889,'手拿包',94,1,0,NULL,NULL),(890,'单肩包',94,1,0,NULL,NULL),(891,'双肩包',94,1,0,NULL,NULL),(892,'手提包',94,1,0,NULL,NULL),(893,'斜挎包',94,1,0,NULL,NULL),(894,'钥匙包',94,1,0,NULL,NULL),(895,'卡包/零钱包',94,1,0,NULL,NULL),(896,'男士钱包',95,1,0,NULL,NULL),(897,'男士手包',95,1,0,NULL,NULL),(898,'卡包名片夹',95,1,0,NULL,NULL),(899,'商务公文包',95,1,0,NULL,NULL),(900,'双肩包',95,1,0,NULL,NULL),(901,'单肩/斜挎包',95,1,0,NULL,NULL),(902,'钥匙包',95,1,0,NULL,NULL),(903,'电脑包',96,1,0,NULL,NULL),(904,'拉杆箱',96,1,0,NULL,NULL),(905,'旅行包',96,1,0,NULL,NULL),(906,'旅行配件',96,1,0,NULL,NULL),(907,'休闲运动包',96,1,0,NULL,NULL),(908,'拉杆包',96,1,0,NULL,NULL),(909,'登山包',96,1,0,NULL,NULL),(910,'妈咪包',96,1,0,NULL,NULL),(911,'书包',96,1,0,NULL,NULL),(912,'相机包',96,1,0,NULL,NULL),(913,'腰包/胸包',96,1,0,NULL,NULL),(914,'火机烟具',97,1,0,NULL,NULL),(915,'礼品文具',97,1,0,NULL,NULL),(916,'军刀军具',97,1,0,NULL,NULL),(917,'收藏品',97,1,0,NULL,NULL),(918,'工艺礼品',97,1,0,NULL,NULL),(919,'创意礼品',97,1,0,NULL,NULL),(920,'礼盒礼券',97,1,0,NULL,NULL),(921,'鲜花绿植',97,1,0,NULL,NULL),(922,'婚庆节庆',97,1,0,NULL,NULL),(923,'京东卡',97,1,0,NULL,NULL),(924,'美妆礼品',97,1,0,NULL,NULL),(925,'礼品定制',97,1,0,NULL,NULL),(926,'京东福卡',97,1,0,NULL,NULL),(927,'古董文玩',97,1,0,NULL,NULL),(928,'箱包',98,1,0,NULL,NULL),(929,'钱包',98,1,0,NULL,NULL),(930,'服饰',98,1,0,NULL,NULL),(931,'腰带',98,1,0,NULL,NULL),(932,'太阳镜/眼镜框',98,1,0,NULL,NULL),(933,'配件',98,1,0,NULL,NULL),(934,'鞋靴',98,1,0,NULL,NULL),(935,'饰品',98,1,0,NULL,NULL),(936,'名品腕表',98,1,0,NULL,NULL),(937,'高档化妆品',98,1,0,NULL,NULL),(938,'婚嫁首饰',99,1,0,NULL,NULL),(939,'婚纱摄影',99,1,0,NULL,NULL),(940,'婚纱礼服',99,1,0,NULL,NULL),(941,'婚庆服务',99,1,0,NULL,NULL),(942,'婚庆礼品/用品',99,1,0,NULL,NULL),(943,'婚宴',99,1,0,NULL,NULL),(944,'饼干蛋糕',100,1,0,NULL,NULL),(945,'糖果/巧克力',100,1,0,NULL,NULL),(946,'休闲零食',100,1,0,NULL,NULL),(947,'冲调饮品',100,1,0,NULL,NULL),(948,'粮油调味',100,1,0,NULL,NULL),(949,'牛奶',100,1,0,NULL,NULL),(950,'其他特产',101,1,0,NULL,NULL),(951,'新疆',101,1,0,NULL,NULL),(952,'北京',101,1,0,NULL,NULL),(953,'山西',101,1,0,NULL,NULL),(954,'内蒙古',101,1,0,NULL,NULL),(955,'福建',101,1,0,NULL,NULL),(956,'湖南',101,1,0,NULL,NULL),(957,'四川',101,1,0,NULL,NULL),(958,'云南',101,1,0,NULL,NULL),(959,'东北',101,1,0,NULL,NULL),(960,'休闲零食',102,1,0,NULL,NULL),(961,'坚果炒货',102,1,0,NULL,NULL),(962,'肉干肉脯',102,1,0,NULL,NULL),(963,'蜜饯果干',102,1,0,NULL,NULL),(964,'糖果/巧克力',102,1,0,NULL,NULL),(965,'饼干蛋糕',102,1,0,NULL,NULL),(966,'无糖食品',102,1,0,NULL,NULL),(967,'米面杂粮',103,1,0,NULL,NULL),(968,'食用油',103,1,0,NULL,NULL),(969,'调味品',103,1,0,NULL,NULL),(970,'南北干货',103,1,0,NULL,NULL),(971,'方便食品',103,1,0,NULL,NULL),(972,'有机食品',103,1,0,NULL,NULL),(973,'饮用水',104,1,0,NULL,NULL),(974,'饮料',104,1,0,NULL,NULL),(975,'牛奶乳品',104,1,0,NULL,NULL),(976,'咖啡/奶茶',104,1,0,NULL,NULL),(977,'冲饮谷物',104,1,0,NULL,NULL),(978,'蜂蜜/柚子茶',104,1,0,NULL,NULL),(979,'成人奶粉',104,1,0,NULL,NULL),(980,'月饼',105,1,0,NULL,NULL),(981,'大闸蟹',105,1,0,NULL,NULL),(982,'粽子',105,1,0,NULL,NULL),(983,'卡券',105,1,0,NULL,NULL),(984,'铁观音',106,1,0,NULL,NULL),(985,'普洱',106,1,0,NULL,NULL),(986,'龙井',106,1,0,NULL,NULL),(987,'绿茶',106,1,0,NULL,NULL),(988,'红茶',106,1,0,NULL,NULL),(989,'乌龙茶',106,1,0,NULL,NULL),(990,'花草茶',106,1,0,NULL,NULL),(991,'花果茶',106,1,0,NULL,NULL),(992,'养生茶',106,1,0,NULL,NULL),(993,'黑茶',106,1,0,NULL,NULL),(994,'白茶',106,1,0,NULL,NULL),(995,'其它茶',106,1,0,NULL,NULL),(996,'项链',107,1,0,NULL,NULL),(997,'手链/脚链',107,1,0,NULL,NULL),(998,'戒指',107,1,0,NULL,NULL),(999,'耳饰',107,1,0,NULL,NULL),(1000,'毛衣链',107,1,0,NULL,NULL),(1001,'发饰/发卡',107,1,0,NULL,NULL),(1002,'胸针',107,1,0,NULL,NULL),(1003,'饰品配件',107,1,0,NULL,NULL),(1004,'婚庆饰品',107,1,0,NULL,NULL),(1005,'黄金吊坠',108,1,0,NULL,NULL),(1006,'黄金项链',108,1,0,NULL,NULL),(1007,'黄金转运珠',108,1,0,NULL,NULL),(1008,'黄金手镯/手链/脚链',108,1,0,NULL,NULL),(1009,'黄金耳饰',108,1,0,NULL,NULL),(1010,'黄金戒指',108,1,0,NULL,NULL),(1011,'K金吊坠',109,1,0,NULL,NULL),(1012,'K金项链',109,1,0,NULL,NULL),(1013,'K金手镯/手链/脚链',109,1,0,NULL,NULL),(1014,'K金戒指',109,1,0,NULL,NULL),(1015,'K金耳饰',109,1,0,NULL,NULL),(1016,'投资金',110,1,0,NULL,NULL),(1017,'投资银',110,1,0,NULL,NULL),(1018,'投资收藏',110,1,0,NULL,NULL),(1019,'银吊坠/项链',111,1,0,NULL,NULL),(1020,'银手镯/手链/脚链',111,1,0,NULL,NULL),(1021,'银戒指',111,1,0,NULL,NULL),(1022,'银耳饰',111,1,0,NULL,NULL),(1023,'足银手镯',111,1,0,NULL,NULL),(1024,'宝宝银饰',111,1,0,NULL,NULL),(1025,'裸钻',112,1,0,NULL,NULL),(1026,'钻戒',112,1,0,NULL,NULL),(1027,'钻石项链/吊坠',112,1,0,NULL,NULL),(1028,'钻石耳饰',112,1,0,NULL,NULL),(1029,'钻石手镯/手链',112,1,0,NULL,NULL),(1030,'项链/吊坠',113,1,0,NULL,NULL),(1031,'手镯/手串',113,1,0,NULL,NULL),(1032,'戒指',113,1,0,NULL,NULL),(1033,'耳饰',113,1,0,NULL,NULL),(1034,'挂件/摆件/把件',113,1,0,NULL,NULL),(1035,'玉石孤品',113,1,0,NULL,NULL),(1036,'项链/吊坠',114,1,0,NULL,NULL),(1037,'耳饰',114,1,0,NULL,NULL),(1038,'手镯/手链/脚链',114,1,0,NULL,NULL),(1039,'戒指',114,1,0,NULL,NULL),(1040,'头饰/胸针',114,1,0,NULL,NULL),(1041,'摆件/挂件',114,1,0,NULL,NULL),(1042,'琥珀/蜜蜡',115,1,0,NULL,NULL),(1043,'碧玺',115,1,0,NULL,NULL),(1044,'红宝石/蓝宝石',115,1,0,NULL,NULL),(1045,'坦桑石',115,1,0,NULL,NULL),(1046,'珊瑚',115,1,0,NULL,NULL),(1047,'祖母绿',115,1,0,NULL,NULL),(1048,'葡萄石',115,1,0,NULL,NULL),(1049,'其他天然宝石',115,1,0,NULL,NULL),(1050,'项链/吊坠',115,1,0,NULL,NULL),(1051,'耳饰',115,1,0,NULL,NULL),(1052,'手镯/手链',115,1,0,NULL,NULL),(1053,'戒指',115,1,0,NULL,NULL),(1054,'铂金项链/吊坠',116,1,0,NULL,NULL),(1055,'铂金手镯/手链/脚链',116,1,0,NULL,NULL),(1056,'铂金戒指',116,1,0,NULL,NULL),(1057,'铂金耳饰',116,1,0,NULL,NULL),(1058,'小叶紫檀',117,1,0,NULL,NULL),(1059,'黄花梨',117,1,0,NULL,NULL),(1060,'沉香木',117,1,0,NULL,NULL),(1061,'金丝楠',117,1,0,NULL,NULL),(1062,'菩提',117,1,0,NULL,NULL),(1063,'其他',117,1,0,NULL,NULL),(1064,'橄榄核/核桃',117,1,0,NULL,NULL),(1065,'檀香',117,1,0,NULL,NULL),(1066,'珍珠项链',118,1,0,NULL,NULL),(1067,'珍珠吊坠',118,1,0,NULL,NULL),(1068,'珍珠耳饰',118,1,0,NULL,NULL),(1069,'珍珠手链',118,1,0,NULL,NULL),(1070,'珍珠戒指',118,1,0,NULL,NULL),(1071,'珍珠胸针',118,1,0,NULL,NULL),(1072,'机油',119,1,0,NULL,NULL),(1073,'正时皮带',119,1,0,NULL,NULL),(1074,'添加剂',119,1,0,NULL,NULL),(1075,'汽车喇叭',119,1,0,NULL,NULL),(1076,'防冻液',119,1,0,NULL,NULL),(1077,'汽车玻璃',119,1,0,NULL,NULL),(1078,'滤清器',119,1,0,NULL,NULL),(1079,'火花塞',119,1,0,NULL,NULL),(1080,'减震器',119,1,0,NULL,NULL),(1081,'柴机油/辅助油',119,1,0,NULL,NULL),(1082,'雨刷',119,1,0,NULL,NULL),(1083,'车灯',119,1,0,NULL,NULL),(1084,'后视镜',119,1,0,NULL,NULL),(1085,'轮胎',119,1,0,NULL,NULL),(1086,'轮毂',119,1,0,NULL,NULL),(1087,'刹车片/盘',119,1,0,NULL,NULL),(1088,'维修配件',119,1,0,NULL,NULL),(1089,'蓄电池',119,1,0,NULL,NULL),(1090,'底盘装甲/护板',119,1,0,NULL,NULL),(1091,'贴膜',119,1,0,NULL,NULL),(1092,'汽修工具',119,1,0,NULL,NULL),(1093,'改装配件',119,1,0,NULL,NULL),(1094,'导航仪',120,1,0,NULL,NULL),(1095,'安全预警仪',120,1,0,NULL,NULL),(1096,'行车记录仪',120,1,0,NULL,NULL),(1097,'倒车雷达',120,1,0,NULL,NULL),(1098,'蓝牙设备',120,1,0,NULL,NULL),(1099,'车载影音',120,1,0,NULL,NULL),(1100,'净化器',120,1,0,NULL,NULL),(1101,'电源',120,1,0,NULL,NULL),(1102,'智能驾驶',120,1,0,NULL,NULL),(1103,'车载电台',120,1,0,NULL,NULL),(1104,'车载电器配件',120,1,0,NULL,NULL),(1105,'吸尘器',120,1,0,NULL,NULL),(1106,'智能车机',120,1,0,NULL,NULL),(1107,'冰箱',120,1,0,NULL,NULL),(1108,'汽车音响',120,1,0,NULL,NULL),(1109,'车载生活电器',120,1,0,NULL,NULL),(1110,'车蜡',121,1,0,NULL,NULL),(1111,'补漆笔',121,1,0,NULL,NULL),(1112,'玻璃水',121,1,0,NULL,NULL),(1113,'清洁剂',121,1,0,NULL,NULL),(1114,'洗车工具',121,1,0,NULL,NULL),(1115,'镀晶镀膜',121,1,0,NULL,NULL),(1116,'打蜡机',121,1,0,NULL,NULL),(1117,'洗车配件',121,1,0,NULL,NULL),(1118,'洗车机',121,1,0,NULL,NULL),(1119,'洗车水枪',121,1,0,NULL,NULL),(1120,'毛巾掸子',121,1,0,NULL,NULL),(1121,'脚垫',122,1,0,NULL,NULL),(1122,'座垫',122,1,0,NULL,NULL),(1123,'座套',122,1,0,NULL,NULL),(1124,'后备箱垫',122,1,0,NULL,NULL),(1125,'头枕腰靠',122,1,0,NULL,NULL),(1126,'方向盘套',122,1,0,NULL,NULL),(1127,'香水',122,1,0,NULL,NULL),(1128,'空气净化',122,1,0,NULL,NULL),(1129,'挂件摆件',122,1,0,NULL,NULL),(1130,'功能小件',122,1,0,NULL,NULL),(1131,'车身装饰件',122,1,0,NULL,NULL),(1132,'车衣',122,1,0,NULL,NULL),(1133,'安全座椅',123,1,0,NULL,NULL),(1134,'胎压监测',123,1,0,NULL,NULL),(1135,'防盗设备',123,1,0,NULL,NULL),(1136,'应急救援',123,1,0,NULL,NULL),(1137,'保温箱',123,1,0,NULL,NULL),(1138,'地锁',123,1,0,NULL,NULL),(1139,'摩托车',123,1,0,NULL,NULL),(1140,'充气泵',123,1,0,NULL,NULL),(1141,'储物箱',123,1,0,NULL,NULL),(1142,'自驾野营',123,1,0,NULL,NULL),(1143,'摩托车装备',123,1,0,NULL,NULL),(1144,'清洗美容',124,1,0,NULL,NULL),(1145,'功能升级',124,1,0,NULL,NULL),(1146,'保养维修',124,1,0,NULL,NULL),(1147,'油卡充值',124,1,0,NULL,NULL),(1148,'车险',124,1,0,NULL,NULL),(1149,'加油卡',124,1,0,NULL,NULL),(1150,'ETC',124,1,0,NULL,NULL),(1151,'驾驶培训',124,1,0,NULL,NULL),(1152,'赛事服装',125,1,0,NULL,NULL),(1153,'赛事用品',125,1,0,NULL,NULL),(1154,'制动系统',125,1,0,NULL,NULL),(1155,'悬挂系统',125,1,0,NULL,NULL),(1156,'进气系统',125,1,0,NULL,NULL),(1157,'排气系统',125,1,0,NULL,NULL),(1158,'电子管理',125,1,0,NULL,NULL),(1159,'车身强化',125,1,0,NULL,NULL),(1160,'赛事座椅',125,1,0,NULL,NULL),(1161,'跑步鞋',126,1,0,NULL,NULL),(1162,'休闲鞋',126,1,0,NULL,NULL),(1163,'篮球鞋',126,1,0,NULL,NULL),(1164,'板鞋',126,1,0,NULL,NULL),(1165,'帆布鞋',126,1,0,NULL,NULL),(1166,'足球鞋',126,1,0,NULL,NULL),(1167,'乒羽网鞋',126,1,0,NULL,NULL),(1168,'专项运动鞋',126,1,0,NULL,NULL),(1169,'训练鞋',126,1,0,NULL,NULL),(1170,'拖鞋',126,1,0,NULL,NULL),(1171,'运动包',126,1,0,NULL,NULL),(1172,'羽绒服',127,1,0,NULL,NULL),(1173,'棉服',127,1,0,NULL,NULL),(1174,'运动裤',127,1,0,NULL,NULL),(1175,'夹克/风衣',127,1,0,NULL,NULL),(1176,'卫衣/套头衫',127,1,0,NULL,NULL),(1177,'T恤',127,1,0,NULL,NULL),(1178,'套装',127,1,0,NULL,NULL),(1179,'乒羽网服',127,1,0,NULL,NULL),(1180,'健身服',127,1,0,NULL,NULL),(1181,'运动背心',127,1,0,NULL,NULL),(1182,'毛衫/线衫',127,1,0,NULL,NULL),(1183,'运动配饰',127,1,0,NULL,NULL),(1184,'折叠车',128,1,0,NULL,NULL),(1185,'山地车/公路车',128,1,0,NULL,NULL),(1186,'电动车',128,1,0,NULL,NULL),(1187,'其他整车',128,1,0,NULL,NULL),(1188,'骑行服',128,1,0,NULL,NULL),(1189,'骑行装备',128,1,0,NULL,NULL),(1190,'平衡车',128,1,0,NULL,NULL),(1191,'鱼竿鱼线',129,1,0,NULL,NULL),(1192,'浮漂鱼饵',129,1,0,NULL,NULL),(1193,'钓鱼桌椅',129,1,0,NULL,NULL),(1194,'钓鱼配件',129,1,0,NULL,NULL),(1195,'钓箱鱼包',129,1,0,NULL,NULL),(1196,'其它',129,1,0,NULL,NULL),(1197,'泳镜',130,1,0,NULL,NULL),(1198,'泳帽',130,1,0,NULL,NULL),(1199,'游泳包防水包',130,1,0,NULL,NULL),(1200,'女士泳衣',130,1,0,NULL,NULL),(1201,'男士泳衣',130,1,0,NULL,NULL),(1202,'比基尼',130,1,0,NULL,NULL),(1203,'其它',130,1,0,NULL,NULL),(1204,'冲锋衣裤',131,1,0,NULL,NULL),(1205,'速干衣裤',131,1,0,NULL,NULL),(1206,'滑雪服',131,1,0,NULL,NULL),(1207,'羽绒服/棉服',131,1,0,NULL,NULL),(1208,'休闲衣裤',131,1,0,NULL,NULL),(1209,'抓绒衣裤',131,1,0,NULL,NULL),(1210,'软壳衣裤',131,1,0,NULL,NULL),(1211,'T恤',131,1,0,NULL,NULL),(1212,'户外风衣',131,1,0,NULL,NULL),(1213,'功能内衣',131,1,0,NULL,NULL),(1214,'军迷服饰',131,1,0,NULL,NULL),(1215,'登山鞋',131,1,0,NULL,NULL),(1216,'雪地靴',131,1,0,NULL,NULL),(1217,'徒步鞋',131,1,0,NULL,NULL),(1218,'越野跑鞋',131,1,0,NULL,NULL),(1219,'休闲鞋',131,1,0,NULL,NULL),(1220,'工装鞋',131,1,0,NULL,NULL),(1221,'溯溪鞋',131,1,0,NULL,NULL),(1222,'沙滩/凉拖',131,1,0,NULL,NULL),(1223,'户外袜',131,1,0,NULL,NULL),(1224,'帐篷/垫子',132,1,0,NULL,NULL),(1225,'睡袋/吊床',132,1,0,NULL,NULL),(1226,'登山攀岩',132,1,0,NULL,NULL),(1227,'户外配饰',132,1,0,NULL,NULL),(1228,'背包',132,1,0,NULL,NULL),(1229,'户外照明',132,1,0,NULL,NULL),(1230,'户外仪表',132,1,0,NULL,NULL),(1231,'户外工具',132,1,0,NULL,NULL),(1232,'望远镜',132,1,0,NULL,NULL),(1233,'旅游用品',132,1,0,NULL,NULL),(1234,'便携桌椅床',132,1,0,NULL,NULL),(1235,'野餐烧烤',132,1,0,NULL,NULL),(1236,'军迷用品',132,1,0,NULL,NULL),(1237,'救援装备',132,1,0,NULL,NULL),(1238,'滑雪装备',132,1,0,NULL,NULL),(1239,'极限户外',132,1,0,NULL,NULL),(1240,'冲浪潜水',132,1,0,NULL,NULL),(1241,'综合训练器',133,1,0,NULL,NULL),(1242,'其他大型器械',133,1,0,NULL,NULL),(1243,'哑铃',133,1,0,NULL,NULL),(1244,'仰卧板/收腹机',133,1,0,NULL,NULL),(1245,'其他中小型器材',133,1,0,NULL,NULL),(1246,'瑜伽舞蹈',133,1,0,NULL,NULL),(1247,'甩脂机',133,1,0,NULL,NULL),(1248,'踏步机',133,1,0,NULL,NULL),(1249,'武术搏击',133,1,0,NULL,NULL),(1250,'健身车/动感单车',133,1,0,NULL,NULL),(1251,'跑步机',133,1,0,NULL,NULL),(1252,'运动护具',133,1,0,NULL,NULL),(1253,'羽毛球',134,1,0,NULL,NULL),(1254,'乒乓球',134,1,0,NULL,NULL),(1255,'篮球',134,1,0,NULL,NULL),(1256,'足球',134,1,0,NULL,NULL),(1257,'网球',134,1,0,NULL,NULL),(1258,'排球',134,1,0,NULL,NULL),(1259,'高尔夫',134,1,0,NULL,NULL),(1260,'台球',134,1,0,NULL,NULL),(1261,'棋牌麻将',134,1,0,NULL,NULL),(1262,'轮滑滑板',134,1,0,NULL,NULL),(1263,'其他',134,1,0,NULL,NULL),(1264,'0-6个月',135,1,0,NULL,NULL),(1265,'6-12个月',135,1,0,NULL,NULL),(1266,'1-3岁',135,1,0,NULL,NULL),(1267,'3-6岁',135,1,0,NULL,NULL),(1268,'6-14岁',135,1,0,NULL,NULL),(1269,'14岁以上',135,1,0,NULL,NULL),(1270,'遥控车',136,1,0,NULL,NULL),(1271,'遥控飞机',136,1,0,NULL,NULL),(1272,'遥控船',136,1,0,NULL,NULL),(1273,'机器人',136,1,0,NULL,NULL),(1274,'轨道/助力',136,1,0,NULL,NULL),(1275,'毛绒/布艺',137,1,0,NULL,NULL),(1276,'靠垫/抱枕',137,1,0,NULL,NULL),(1277,'芭比娃娃',138,1,0,NULL,NULL),(1278,'卡通娃娃',138,1,0,NULL,NULL),(1279,'智能娃娃',138,1,0,NULL,NULL),(1280,'仿真模型',139,1,0,NULL,NULL),(1281,'拼插模型',139,1,0,NULL,NULL),(1282,'收藏爱好',139,1,0,NULL,NULL),(1283,'炫舞毯',140,1,0,NULL,NULL),(1284,'爬行垫/毯',140,1,0,NULL,NULL),(1285,'户外玩具',140,1,0,NULL,NULL),(1286,'戏水玩具',140,1,0,NULL,NULL),(1287,'电影周边',141,1,0,NULL,NULL),(1288,'卡通周边',141,1,0,NULL,NULL),(1289,'网游周边',141,1,0,NULL,NULL),(1290,'摇铃/床铃',142,1,0,NULL,NULL),(1291,'健身架',142,1,0,NULL,NULL),(1292,'早教启智',142,1,0,NULL,NULL),(1293,'拖拉玩具',142,1,0,NULL,NULL),(1294,'积木',143,1,0,NULL,NULL),(1295,'拼图',143,1,0,NULL,NULL),(1296,'磁力棒',143,1,0,NULL,NULL),(1297,'立体拼插',143,1,0,NULL,NULL),(1298,'手工彩泥',144,1,0,NULL,NULL),(1299,'绘画工具',144,1,0,NULL,NULL),(1300,'情景玩具',144,1,0,NULL,NULL),(1301,'减压玩具',145,1,0,NULL,NULL),(1302,'创意玩具',145,1,0,NULL,NULL),(1303,'钢琴',146,1,0,NULL,NULL),(1304,'电子琴/电钢琴',146,1,0,NULL,NULL),(1305,'吉他/尤克里里',146,1,0,NULL,NULL),(1306,'打击乐器',146,1,0,NULL,NULL),(1307,'西洋管弦',146,1,0,NULL,NULL),(1308,'民族管弦乐器',146,1,0,NULL,NULL),(1309,'乐器配件',146,1,0,NULL,NULL),(1310,'电脑音乐',146,1,0,NULL,NULL),(1311,'工艺礼品乐器',146,1,0,NULL,NULL),(1312,'口琴/口风琴/竖笛',146,1,0,NULL,NULL),(1313,'手风琴',146,1,0,NULL,NULL),(1314,'双色球',147,1,0,NULL,NULL),(1315,'大乐透',147,1,0,NULL,NULL),(1316,'福彩3D',147,1,0,NULL,NULL),(1317,'排列三',147,1,0,NULL,NULL),(1318,'排列五',147,1,0,NULL,NULL),(1319,'七星彩',147,1,0,NULL,NULL),(1320,'七乐彩',147,1,0,NULL,NULL),(1321,'竞彩足球',147,1,0,NULL,NULL),(1322,'竞彩篮球',147,1,0,NULL,NULL),(1323,'新时时彩',147,1,0,NULL,NULL),(1324,'国内机票',148,1,0,NULL,NULL),(1325,'国内酒店',149,1,0,NULL,NULL),(1326,'酒店团购',149,1,0,NULL,NULL),(1327,'度假',150,1,0,NULL,NULL),(1328,'景点',150,1,0,NULL,NULL),(1329,'租车',150,1,0,NULL,NULL),(1330,'火车票',150,1,0,NULL,NULL),(1331,'旅游团购',150,1,0,NULL,NULL),(1332,'手机充值',151,1,0,NULL,NULL),(1333,'游戏点卡',152,1,0,NULL,NULL),(1334,'QQ充值',152,1,0,NULL,NULL),(1335,'电影票',153,1,0,NULL,NULL),(1336,'演唱会',153,1,0,NULL,NULL),(1337,'话剧歌剧',153,1,0,NULL,NULL),(1338,'音乐会',153,1,0,NULL,NULL),(1339,'体育赛事',153,1,0,NULL,NULL),(1340,'舞蹈芭蕾',153,1,0,NULL,NULL),(1341,'戏曲综艺',153,1,0,NULL,NULL),(1342,'东北',154,1,0,NULL,NULL),(1343,'华北',154,1,0,NULL,NULL),(1344,'西北',154,1,0,NULL,NULL),(1345,'华中',154,1,0,NULL,NULL),(1346,'华东',154,1,0,NULL,NULL),(1347,'华南',154,1,0,NULL,NULL),(1348,'西南',154,1,0,NULL,NULL),(1349,'苹果',155,1,0,NULL,NULL),(1350,'橙子',155,1,0,NULL,NULL),(1351,'奇异果/猕猴桃',155,1,0,NULL,NULL),(1352,'车厘子/樱桃',155,1,0,NULL,NULL),(1353,'芒果',155,1,0,NULL,NULL),(1354,'蓝莓',155,1,0,NULL,NULL),(1355,'火龙果',155,1,0,NULL,NULL),(1356,'葡萄/提子',155,1,0,NULL,NULL),(1357,'柚子',155,1,0,NULL,NULL),(1358,'香蕉',155,1,0,NULL,NULL),(1359,'牛油果',155,1,0,NULL,NULL),(1360,'梨',155,1,0,NULL,NULL),(1361,'菠萝/凤梨',155,1,0,NULL,NULL),(1362,'桔/橘',155,1,0,NULL,NULL),(1363,'柠檬',155,1,0,NULL,NULL),(1364,'草莓',155,1,0,NULL,NULL),(1365,'桃/李/杏',155,1,0,NULL,NULL),(1366,'更多水果',155,1,0,NULL,NULL),(1367,'水果礼盒/券',155,1,0,NULL,NULL),(1368,'牛肉',156,1,0,NULL,NULL),(1369,'羊肉',156,1,0,NULL,NULL),(1370,'猪肉',156,1,0,NULL,NULL),(1371,'内脏类',156,1,0,NULL,NULL),(1372,'鱼类',157,1,0,NULL,NULL),(1373,'虾类',157,1,0,NULL,NULL),(1374,'蟹类',157,1,0,NULL,NULL),(1375,'贝类',157,1,0,NULL,NULL),(1376,'海参',157,1,0,NULL,NULL),(1377,'海产干货',157,1,0,NULL,NULL),(1378,'其他水产',157,1,0,NULL,NULL),(1379,'海产礼盒',157,1,0,NULL,NULL),(1380,'鸡肉',158,1,0,NULL,NULL),(1381,'鸭肉',158,1,0,NULL,NULL),(1382,'蛋类',158,1,0,NULL,NULL),(1383,'其他禽类',158,1,0,NULL,NULL),(1384,'水饺/馄饨',159,1,0,NULL,NULL),(1385,'汤圆/元宵',159,1,0,NULL,NULL),(1386,'面点',159,1,0,NULL,NULL),(1387,'火锅丸串',159,1,0,NULL,NULL),(1388,'速冻半成品',159,1,0,NULL,NULL),(1389,'奶酪黄油',159,1,0,NULL,NULL),(1390,'熟食',160,1,0,NULL,NULL),(1391,'腊肠/腊肉',160,1,0,NULL,NULL),(1392,'火腿',160,1,0,NULL,NULL),(1393,'糕点',160,1,0,NULL,NULL),(1394,'礼品卡券',160,1,0,NULL,NULL),(1395,'冷藏果蔬汁',161,1,0,NULL,NULL),(1396,'冰激凌',161,1,0,NULL,NULL),(1397,'其他',161,1,0,NULL,NULL),(1398,'叶菜类',162,1,0,NULL,NULL),(1399,'茄果瓜类',162,1,0,NULL,NULL),(1400,'根茎类',162,1,0,NULL,NULL),(1401,'鲜菌菇',162,1,0,NULL,NULL),(1402,'葱姜蒜椒',162,1,0,NULL,NULL),(1403,'半加工蔬菜',162,1,0,NULL,NULL),(1404,'微型车',163,1,0,NULL,NULL),(1405,'小型车',163,1,0,NULL,NULL),(1406,'紧凑型车',163,1,0,NULL,NULL),(1407,'中型车',163,1,0,NULL,NULL),(1408,'中大型车',163,1,0,NULL,NULL),(1409,'豪华车',163,1,0,NULL,NULL),(1410,'MPV',163,1,0,NULL,NULL),(1411,'SUV',163,1,0,NULL,NULL),(1412,'跑车',163,1,0,NULL,NULL),(1413,'微型车（二手）',164,1,0,NULL,NULL),(1414,'小型车（二手）',164,1,0,NULL,NULL),(1415,'紧凑型车（二手）',164,1,0,NULL,NULL),(1416,'中型车（二手）',164,1,0,NULL,NULL),(1417,'中大型车（二手）',164,1,0,NULL,NULL),(1418,'豪华车（二手）',164,1,0,NULL,NULL),(1419,'MPV（二手）',164,1,0,NULL,NULL),(1420,'SUV（二手）',164,1,0,NULL,NULL),(1421,'跑车（二手）',164,1,0,NULL,NULL),(1422,'皮卡（二手）',164,1,0,NULL,NULL),(1423,'面包车（二手）',164,1,0,NULL,NULL);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_comment`
--

DROP TABLE IF EXISTS `product_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_comment` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint DEFAULT NULL COMMENT 'sku_id',
  `spu_id` bigint DEFAULT NULL COMMENT 'spu_id',
  `product_name` varchar(255) DEFAULT NULL COMMENT '商品名字',
  `user_nick_name` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `user_ip` varchar(64) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `show_status` tinyint(1) DEFAULT NULL COMMENT '显示状态[0-不显示，1-显示]',
  `spu_attributes` varchar(255) DEFAULT NULL COMMENT '购买时属性组合',
  `endorse_count` int DEFAULT NULL COMMENT '点赞数',
  `reply_count` int DEFAULT NULL COMMENT '回复数',
  `resources` varchar(1000) DEFAULT NULL COMMENT '评论图片/视频[json数据；[{type:文件类型,url:资源路径}]]',
  `content` text COMMENT '内容',
  `icon` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `comment_type` tinyint DEFAULT NULL COMMENT '评论类型[0 - 对商品的直接评论，1 - 对评论的回复]',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='商品评价';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_comment`
--

LOCK TABLES `product_comment` WRITE;
/*!40000 ALTER TABLE `product_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_sku_info`
--

DROP TABLE IF EXISTS `product_sku_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_sku_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'skuId',
  `spu_id` bigint DEFAULT NULL COMMENT 'spuId',
  `sku_name` varchar(255) DEFAULT NULL COMMENT 'sku名称',
  `catagory_id` bigint DEFAULT NULL COMMENT '所属分类id',
  `brand_id` bigint DEFAULT NULL COMMENT '品牌id',
  `default_image` varchar(255) DEFAULT NULL COMMENT '默认图片',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `subtitle` varchar(2000) DEFAULT NULL COMMENT '副标题',
  `price` decimal(18,4) DEFAULT NULL COMMENT '价格',
  `weight` decimal(18,4) DEFAULT NULL COMMENT '重量（克）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COMMENT='sku信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_sku_info`
--

LOCK TABLES `product_sku_info` WRITE;
/*!40000 ALTER TABLE `product_sku_info` DISABLE KEYS */;
INSERT INTO `product_sku_info` VALUES (1,7,'华为mate30pro 黑色,8G,128G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+128GB亮黑色5G全网通游戏手机','【直降500元！到手价4499元！享12期免息！低至12.3元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',5000.0000,500.0000),(2,7,'华为mate30pro 黑色,8G,256G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+256GB亮黑色5G全网通游戏手机','【直降500元！到手价4999元！享12期免息！低至13.7元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',6000.0000,510.0000),(3,8,'华为mate30pro 黑色,8G,128G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+128GB亮黑色5G全网通游戏手机','【直降500元！到手价4499元！享12期免息！低至12.3元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',5000.0000,500.0000),(4,8,'华为mate30pro 黑色,8G,256G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+256GB亮黑色5G全网通游戏手机','【直降500元！到手价4999元！享12期免息！低至13.7元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',6000.0000,510.0000),(5,9,'华为mate30pro 黑色,8G,128G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+128GB亮黑色5G全网通游戏手机','【直降500元！到手价4499元！享12期免息！低至12.3元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',5000.0000,500.0000),(6,9,'华为mate30pro 黑色,8G,256G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+256GB亮黑色5G全网通游戏手机','【直降500元！到手价4999元！享12期免息！低至13.7元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',6000.0000,510.0000),(7,10,'华为mate30pro 黑色,8G,128G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+128GB亮黑色5G全网通游戏手机','【直降500元！到手价4499元！享12期免息！低至12.3元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',5000.0000,500.0000),(8,10,'华为mate30pro 黑色,8G,256G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+256GB亮黑色5G全网通游戏手机','【直降500元！到手价4999元！享12期免息！低至13.7元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',6000.0000,510.0000),(9,11,'华为mate30pro 黑色,8G,128G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+128GB亮黑色5G全网通游戏手机','【直降500元！到手价4499元！享12期免息！低至12.3元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',5000.0000,500.0000),(10,11,'华为mate30pro 黑色,8G,256G',225,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg','华为 HUAWEI Mate 30 5G 麒麟990 4000万超感光徕卡影像双超级快充8GB+256GB亮黑色5G全网通游戏手机','【直降500元！到手价4999元！享12期免息！低至13.7元/天】双4000万徕卡电影四摄；Mate30直降500元享12期》',6000.0000,510.0000),(11,12,'红米k20Pro 尊享版',225,2,'https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg','RedMi','string',3200.0000,NULL),(12,13,'红米k20Pro 尊享版',225,2,'https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg','RedMi','新春折扣',3200.0000,NULL);
/*!40000 ALTER TABLE `product_sku_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_spu_info`
--

DROP TABLE IF EXISTS `product_spu_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_spu_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'spuid',
  `name` varchar(200) DEFAULT NULL COMMENT '商品名称',
  `category_id` bigint DEFAULT NULL COMMENT '所属分类id',
  `brand_id` bigint DEFAULT NULL COMMENT '品牌id',
  `shelf_status` tinyint DEFAULT NULL COMMENT '上架状态[0 - 下架，1 - 上架]',
  `decript` longtext COMMENT '商品介绍',
  `image` varchar(225) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COMMENT='spu信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_spu_info`
--

LOCK TABLES `product_spu_info` WRITE;
/*!40000 ALTER TABLE `product_spu_info` DISABLE KEYS */;
INSERT INTO `product_spu_info` VALUES (7,'华为mate30pro',225,2,2,'','https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/0c6c1f13-641b-45ce-851d-359bc6687522_e9ad9735fc3f0686.jpg','2020-03-21 00:54:21','2022-02-28 20:04:51'),(8,'华为mate30pro',225,2,1,'','https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/0c6c1f13-641b-45ce-851d-359bc6687522_e9ad9735fc3f0686.jpg','2020-03-21 12:19:34','2020-03-21 12:19:34'),(9,'华为mate30pro',225,2,1,'','https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/0c6c1f13-641b-45ce-851d-359bc6687522_e9ad9735fc3f0686.jpg','2020-03-21 12:34:56','2020-03-21 12:34:56'),(10,'华为mate30pro7',225,2,1,'','https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/0c6c1f13-641b-45ce-851d-359bc6687522_e9ad9735fc3f0686.jpg','2020-03-21 12:46:59','2020-03-21 12:46:59'),(11,'华为mate30pro7',250,2,1,'','https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/0c6c1f13-641b-45ce-851d-359bc6687522_e9ad9735fc3f0686.jpg','2020-03-21 13:07:59','2020-03-21 13:07:59'),(12,'红米K20',NULL,2,1,'极致性价比','https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg','2022-02-24 19:29:23','2022-02-24 19:29:23'),(13,'红米K20',NULL,2,1,'极致性价比','https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg','2022-02-24 19:37:35','2022-02-24 19:37:35');
/*!40000 ALTER TABLE `product_spu_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_order`
--

DROP TABLE IF EXISTS `purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_order` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '采购单id',
  `purchaser_id` bigint DEFAULT NULL COMMENT '采购人id',
  `purchaser_name` varchar(255) DEFAULT NULL COMMENT '采购人名',
  `purchaser_phone` char(13) DEFAULT NULL COMMENT '联系方式',
  `status` int DEFAULT NULL COMMENT '状态[0新建，1已分配，2正在采购，3已完成，4采购失败]',
  `ware_id` bigint DEFAULT NULL COMMENT '仓库id',
  `expenditure` decimal(18,4) DEFAULT NULL COMMENT '总金额',
  `sku_id` bigint DEFAULT NULL COMMENT '采购商品id',
  `sku_number` int DEFAULT NULL COMMENT '采购数量',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='采购信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order`
--

LOCK TABLES `purchase_order` WRITE;
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sku_attribute_value`
--

DROP TABLE IF EXISTS `sku_attribute_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sku_attribute_value` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint DEFAULT NULL COMMENT 'sku_id',
  `attribute_id` bigint DEFAULT NULL COMMENT 'attr_id',
  `attribute_name` varchar(200) DEFAULT NULL COMMENT '销售属性名',
  `attribute_value` varchar(200) DEFAULT NULL COMMENT '销售属性值',
  `sort` int DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3 COMMENT='sku销售属性&值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sku_attribute_value`
--

LOCK TABLES `sku_attribute_value` WRITE;
/*!40000 ALTER TABLE `sku_attribute_value` DISABLE KEYS */;
INSERT INTO `sku_attribute_value` VALUES (1,1,3,'机身颜色','黑色',0),(2,1,4,'运行内存','8G',0),(3,1,5,'机身存储','128G',0),(4,2,3,'机身颜色','黑色',0),(5,2,4,'运行内存','8G',0),(6,2,5,'机身存储','256G',0),(7,3,3,'机身颜色','黑色',0),(8,3,4,'运行内存','8G',0),(9,3,5,'机身存储','128G',0),(10,4,3,'机身颜色','黑色',0),(11,4,4,'运行内存','8G',0),(12,4,5,'机身存储','256G',0),(13,5,3,'机身颜色','黑色',0),(14,5,4,'运行内存','8G',0),(15,5,5,'机身存储','128G',0),(16,6,3,'机身颜色','黑色',0),(17,6,4,'运行内存','8G',0),(18,6,5,'机身存储','256G',0),(19,7,3,'机身颜色','黑色',0),(20,7,4,'运行内存','8G',0),(21,7,5,'机身存储','128G',0),(22,8,3,'机身颜色','黑色',0),(23,8,4,'运行内存','8G',0),(24,8,5,'机身存储','256G',0),(25,9,3,'机身颜色','黑色',0),(26,9,4,'运行内存','8G',0),(27,9,5,'机身存储','128G',0),(28,10,3,'机身颜色','黑色',0),(29,10,4,'运行内存','8G',0),(30,10,5,'机身存储','256G',0),(32,12,3,'机身颜色','白色',NULL);
/*!40000 ALTER TABLE `sku_attribute_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sku_images`
--

DROP TABLE IF EXISTS `sku_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sku_images` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint DEFAULT NULL COMMENT 'sku_id',
  `url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `sort` int DEFAULT NULL COMMENT '排序',
  `default_status` int DEFAULT NULL COMMENT '默认图[0 - 不是默认图，1 - 是默认图]',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COMMENT='sku图片';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sku_images`
--

LOCK TABLES `sku_images` WRITE;
/*!40000 ALTER TABLE `sku_images` DISABLE KEYS */;
INSERT INTO `sku_images` VALUES (1,1,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg',0,1),(2,1,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/9abdcf3c-d5cc-4349-8fe0-d47f23909708_eda8c4c43653595f (1).jpg',0,0),(3,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg',0,1),(4,2,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/fe551480-e2d9-47f2-873c-6ecaaeb2dd27_eda8c4c43653595f (1).jpg',0,0),(5,3,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg',0,1),(6,3,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/9abdcf3c-d5cc-4349-8fe0-d47f23909708_eda8c4c43653595f (1).jpg',0,0),(7,4,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg',0,1),(8,4,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/fe551480-e2d9-47f2-873c-6ecaaeb2dd27_eda8c4c43653595f (1).jpg',0,0),(9,5,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg',0,1),(10,5,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/9abdcf3c-d5cc-4349-8fe0-d47f23909708_eda8c4c43653595f (1).jpg',0,0),(11,6,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg',0,1),(12,6,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/fe551480-e2d9-47f2-873c-6ecaaeb2dd27_eda8c4c43653595f (1).jpg',0,0),(13,7,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg',0,1),(14,7,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/9abdcf3c-d5cc-4349-8fe0-d47f23909708_eda8c4c43653595f (1).jpg',0,0),(15,8,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg',0,1),(16,8,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/fe551480-e2d9-47f2-873c-6ecaaeb2dd27_eda8c4c43653595f (1).jpg',0,0),(17,9,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/a3a0a224-caad-4af2-8eec-f62c0e49a51f_e9ad9735fc3f0686.jpg',0,1),(18,9,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/9abdcf3c-d5cc-4349-8fe0-d47f23909708_eda8c4c43653595f (1).jpg',0,0),(19,10,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/f053e068-e43d-46e7-8d67-4964abb240eb_802254cca298ae79 (1).jpg',0,1),(20,10,'https://ggmall.oss-cn-shanghai.aliyuncs.com/2020-03-21/fe551480-e2d9-47f2-873c-6ecaaeb2dd27_eda8c4c43653595f (1).jpg',0,0),(21,11,'https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg',NULL,1),(22,12,'https://mall-hl.oss-cn-guangzhou.aliyuncs.com/2022-02-12/870eead1-a489-4db8-8438-e5c461d104fe_下载 (3).jpg',NULL,1);
/*!40000 ALTER TABLE `sku_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spu_attribute_value`
--

DROP TABLE IF EXISTS `spu_attribute_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spu_attribute_value` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `spu_id` bigint DEFAULT NULL COMMENT '商品id',
  `attribute_id` bigint DEFAULT NULL COMMENT '属性id',
  `attribute_name` varchar(200) DEFAULT NULL COMMENT '属性名',
  `attribute_value` varchar(200) DEFAULT NULL COMMENT '属性值',
  `sort` int DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb3 COMMENT='spu属性值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spu_attribute_value`
--

LOCK TABLES `spu_attribute_value` WRITE;
/*!40000 ALTER TABLE `spu_attribute_value` DISABLE KEYS */;
INSERT INTO `spu_attribute_value` VALUES (7,7,1,'上市','2019',0),(8,7,2,'产品名称','HUAWEI Mate 30',0),(9,7,6,'CPU品牌','麒麟',0),(10,7,7,'CPU型号','麒麟990 5G',0),(11,7,8,'分辨率','FHD+ 2340×1080 像素',0),(12,7,9,'屏幕尺寸','7',0),(13,8,1,'上市','2019',0),(14,8,2,'产品名称','HUAWEI Mate 30',0),(15,8,6,'CPU品牌','麒麟',0),(16,8,7,'CPU型号','麒麟990 5G',0),(17,8,8,'分辨率','FHD+ 2340×1080 像素',0),(18,8,9,'屏幕尺寸','7',0),(19,9,1,'上市','2019',0),(20,9,2,'产品名称','HUAWEI Mate 30',0),(21,9,6,'CPU品牌','麒麟',0),(22,9,7,'CPU型号','麒麟990 5G',0),(23,9,8,'分辨率','FHD+ 2340×1080 像素',0),(24,9,9,'屏幕尺寸','7',0),(25,10,1,'上市','2019',0),(26,10,2,'产品名称','HUAWEI Mate 30',0),(27,10,6,'CPU品牌','麒麟',0),(28,10,7,'CPU型号','麒麟990 5G',0),(29,10,8,'分辨率','FHD+ 2340×1080 像素',0),(30,10,9,'屏幕尺寸','7',0),(31,11,1,'上市','2019',0),(32,11,2,'产品名称','HUAWEI Mate 30',0),(33,11,6,'CPU品牌','麒麟',0),(34,11,7,'CPU型号','麒麟990 5G',0),(35,11,8,'分辨率','FHD+ 2340×1080 像素',0),(36,11,9,'屏幕尺寸','7',0),(41,13,1,'上市','2019',NULL);
/*!40000 ALTER TABLE `spu_attribute_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-14 12:53:31
