-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 39.99.148.87    Database: ware
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `purchase_order`
--

DROP TABLE IF EXISTS `purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_order` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '采购单id',
  `purchaser_id` bigint DEFAULT NULL COMMENT '采购人id',
  `purchaser_name` varchar(255) DEFAULT NULL COMMENT '采购人名',
  `purchaser_phone` char(13) DEFAULT NULL COMMENT '联系方式',
  `status` int DEFAULT NULL COMMENT '状态[0新建，1已分配，2正在采购，3已完成，4采购失败]',
  `ware_id` bigint DEFAULT NULL COMMENT '仓库id',
  `expenditure` decimal(18,4) DEFAULT NULL COMMENT '总金额',
  `sku_id` bigint DEFAULT NULL COMMENT '采购商品id',
  `sku_number` int DEFAULT NULL COMMENT '采购数量',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='采购信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order`
--

LOCK TABLES `purchase_order` WRITE;
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;
INSERT INTO `purchase_order` VALUES (1,1,'张三','1456789',3,1,10000.0000,12,5,'已经完成了','2022-02-25 18:34:07','2022-02-25 18:38:03');
/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_info`
--

DROP TABLE IF EXISTS `ware_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ware_name` varchar(255) DEFAULT NULL COMMENT '仓库名',
  `ware_address` varchar(255) DEFAULT NULL COMMENT '仓库地址',
  `ware_areacode` varchar(20) DEFAULT NULL COMMENT '区域编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='仓库信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_info`
--

LOCK TABLES `ware_info` WRITE;
/*!40000 ALTER TABLE `ware_info` DISABLE KEYS */;
INSERT INTO `ware_info` VALUES (1,'北京仓','昌平区宏福科技园综合楼5层','200'),(2,'上海仓','松江区大江商厦6层','100'),(3,'深圳仓','宝安区西部硅谷大厦B座','300');
/*!40000 ALTER TABLE `ware_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_stock`
--

DROP TABLE IF EXISTS `ware_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_stock` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` bigint DEFAULT NULL COMMENT 'sku_id',
  `ware_id` bigint DEFAULT NULL COMMENT '仓库id',
  `stock` int DEFAULT NULL COMMENT '库存数',
  `sku_name` varchar(200) DEFAULT NULL COMMENT 'sku_name',
  `stock_locked` int DEFAULT NULL COMMENT '锁定库存',
  `sale_count` bigint DEFAULT NULL COMMENT '销量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='商品库存';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_stock`
--

LOCK TABLES `ware_stock` WRITE;
/*!40000 ALTER TABLE `ware_stock` DISABLE KEYS */;
INSERT INTO `ware_stock` VALUES (1,3,1,100,'华为mate30pro 黑色,8G,128G',0,10),(2,3,2,200,'华为mate30pro 黑色,8G,128G',0,20),(3,12,1,5,'红米k20Pro 尊享版',0,NULL);
/*!40000 ALTER TABLE `ware_stock` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-14 12:54:00
