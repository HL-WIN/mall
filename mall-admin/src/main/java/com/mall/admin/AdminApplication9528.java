package com.mall.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author jzz
 * @create 2022/2/25
 */
@SpringBootApplication
@MapperScan("com.mall.admin.mapper")
public class AdminApplication9528 {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication9528.class, args);
    }
}
