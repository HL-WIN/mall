package com.mall.admin.service;

import com.mall.admin.core.service.CurdService;
import com.mall.admin.model.SysConfig;

import java.util.List;

/**
 * @author jzz
 * @create 2022/2/27
 */
public interface SysConfigService extends CurdService<SysConfig> {
    List<SysConfig> findByLabel(String label);
}
