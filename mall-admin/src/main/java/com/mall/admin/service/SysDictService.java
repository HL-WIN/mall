package com.mall.admin.service;

import com.mall.admin.core.service.CurdService;
import com.mall.admin.model.SysDict;

import java.util.List;

/**
 * @author jzz
 * @create 2022/2/27
 */
public interface SysDictService extends CurdService<SysDict> {
    /**
     * 根据label名进行查询
     *
     * @param label 标签名
     * @return 字典对象列表
     */
    List<SysDict> findByLabel(String label);
}
