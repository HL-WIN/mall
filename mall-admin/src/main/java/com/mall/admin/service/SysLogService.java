package com.mall.admin.service;

import com.mall.admin.core.service.CurdService;
import com.mall.admin.model.SysLog;

/**
 * @author jzz
 * @create 2022/2/27
 */
public interface SysLogService extends CurdService<SysLog> {
}
