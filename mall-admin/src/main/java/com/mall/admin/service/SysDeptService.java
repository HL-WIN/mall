package com.mall.admin.service;

import com.mall.admin.core.service.CurdService;
import com.mall.admin.model.SysDept;

import java.util.List;

/**
 * @author jzz
 * @create 2022/2/27
 */
public interface SysDeptService extends CurdService<SysDept> {
    /**
     * 查询部门树
     *
     * @return 部门树
     */
    List<SysDept> findTree();
}
