package com.mall.admin.service;

import com.mall.admin.core.service.CurdService;
import com.mall.admin.model.SysLoginLog;

/**
 * @author jzz
 * @create 2022/2/27
 */
public interface SysLoginLogService extends CurdService<SysLoginLog> {
    /**
     * 记录登录日志
     *
     * @param username 登录用户名
     * @param ipAddr   登录者的ip
     * @return 执行成功与否的错误码
     */
    int writeLoginLog(String username, String ipAddr);
}
