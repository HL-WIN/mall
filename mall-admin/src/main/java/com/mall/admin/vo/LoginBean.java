package com.mall.admin.vo;

import lombok.Data;

/**
 * @author jzz
 * @create 2022/3/6
 */
@Data
public class LoginBean {
    String account;
    String password;
    String captcha;
}
