package com.mall.admin.controller;

import com.mall.admin.model.SysUser;
import com.mall.admin.service.Impl.SysUserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jzz
 * @create 2022/3/6
 */
@RestController
@Slf4j
public class TestController {

    @Autowired
    SysUserServiceImpl sysUserService;

    @GetMapping("/find")
    public List<SysUser> findAll(){
        log.info("................................");
        return sysUserService.findAll();
    }
}
