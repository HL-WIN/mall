package com.mall.admin.constant;

/**
 * @author jzz
 * @create 2022/3/6
 */
public interface SysConstants {
    /**
     * 系统管理员用户名
     */
    String ADMIN = "admin";
}
