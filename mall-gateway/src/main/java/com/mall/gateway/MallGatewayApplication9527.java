package com.mall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/01/22/20:53
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MallGatewayApplication9527 {
    public static void main(String[] args) {
        SpringApplication.run(MallGatewayApplication9527.class,args);
    }
}
