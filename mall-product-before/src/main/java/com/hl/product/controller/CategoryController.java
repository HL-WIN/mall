package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.CategoryService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品三级分类
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:53
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 查出所有分类以及子分类，以树形结构组装
     */
    @GetMapping("/list/tree")
    public CommonResult list()
    {
        List<CategoryEntity> entities=categoryService.listWithTree();
        if (entities.size()!=0)
        {
            return new CommonResult(HttpStatus.SC_OK,"查询分类树形结构成功！",entities);
        }
       else {
            return CommonResult.error("查询分类树形结构失败！");
        }
    }
//    public CommonResult list(@RequestParam Map<String, Object> params){
//        PageUtils page = categoryService.queryPage(params);
//        HashMap<String, PageUtils> hashMap=new HashMap<>();
//        hashMap.put("page",page);
//        return new CommonResult(200,"hashMap",hashMap);
//    }


    /**
     * 信息
     */
    @GetMapping("/info/{catId}")
    public CommonResult info(@PathVariable("catId") Long catId){
		CategoryEntity category = categoryService.getById(catId);
        HashMap<String, CategoryEntity> hashMap=new HashMap<>();
        hashMap.put("category", category);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody CategoryEntity category){
		categoryService.save(category);

        return CommonResult.success("保存category成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody CategoryEntity category){
//		categoryService.updateById(category);
        categoryService.updateRelation(category);
        return CommonResult.success("修改category成功！");
    }

    /**
     * 批量修改
     */
    @PostMapping("/update/sort")
    public CommonResult updateSort(@RequestBody CategoryEntity[] categoryEntities)
    {
        boolean result=categoryService.updateBatchById(Arrays.asList(categoryEntities));
        if (result) {
            return CommonResult.success("批量修改成功!");
        }
        else
        {
            return CommonResult.error("批量修改失败！");
        }
    }
    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] catIds){
		//categoryService.removeByIds(Arrays.asList(catIds));
        categoryService.removeMenusByIds(Arrays.asList(catIds));
        return CommonResult.success("删除category成功！");
    }

}
