package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.hl.product.entity.ProductAttrValueEntity;
import com.hl.product.service.ProductAttrValueService;
import com.hl.product.vo.AttrRespVo;
import com.hl.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.service.AttrService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品属性
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;
    @Autowired
    private ProductAttrValueService productAttrValueService;


    /**
     *  获取spu规格
     * @return
     */
    @GetMapping("/base/listforspu/{spuId}")
    public CommonResult baseAttrlistforspu(@PathVariable("spuId") Long spuId){

        List<ProductAttrValueEntity> entities = productAttrValueService.baseAttrListforspu(spuId);

        return new CommonResult(200,"获取spu规格",entities);
    }
    @GetMapping("/{attrType}/list/{categoryId}")
    public CommonResult baseList(@RequestParam Map<String, Object> params,@PathVariable("categoryId") Long categoryId,
    @PathVariable("attrType")String type){

        PageUtils page = attrService.queryBasePage(params,categoryId,type);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{attrId}")
    public CommonResult info(@PathVariable("attrId") Long attrId){
//		AttrEntity attr = attrService.getById(attrId);
        AttrRespVo attrRespVo=attrService.getInfoVo(attrId);
        return new CommonResult(200,"信息",attrRespVo);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody AttrVo attr){
		attrService.saveAttr(attr);
        return CommonResult.success("保存attr成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody AttrVo attr){
		attrService.updateAttrVo(attr);
        return CommonResult.success("修改attr成功！");
    }

    @PostMapping("/update/{spuId}")
    public CommonResult updateSpuAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> entities){
        productAttrValueService.updateSpuAttr(spuId,entities);

        return CommonResult.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return CommonResult.success("删除attr成功！");
    }

}
