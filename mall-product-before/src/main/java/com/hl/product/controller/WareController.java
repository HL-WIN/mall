package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import com.hl.product.entity.WareEntity;
import com.hl.product.service.WareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 仓库信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@RestController
@RequestMapping("product/ware")
public class WareController {
    @Autowired
    private WareService wareService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = wareService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		WareEntity ware = wareService.getById(id);
        HashMap<String, WareEntity> hashMap=new HashMap<>();
        hashMap.put("ware", ware);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody WareEntity ware){
		wareService.save(ware);

        return CommonResult.success("保存ware成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody WareEntity ware){
		wareService.updateById(ware);

        return CommonResult.success("修改ware成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		wareService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除ware成功！");
    }

}
