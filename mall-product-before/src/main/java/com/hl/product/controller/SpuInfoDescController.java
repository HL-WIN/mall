package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.product.entity.SpuInfoDescEntity;
import com.hl.product.service.SpuInfoDescService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * spu信息介绍
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@RestController
@RequestMapping("product/spuinfodesc")
public class SpuInfoDescController {
    @Autowired
    private SpuInfoDescService spuInfoDescService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = spuInfoDescService.queryPage(params);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{spuId}")
    public CommonResult info(@PathVariable("spuId") Long spuId){
		SpuInfoDescEntity spuInfoDesc = spuInfoDescService.getById(spuId);
        HashMap<String, SpuInfoDescEntity> hashMap=new HashMap<>();
        hashMap.put("spuInfoDesc", spuInfoDesc);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody SpuInfoDescEntity spuInfoDesc){
		spuInfoDescService.save(spuInfoDesc);

        return CommonResult.success("保存spuInfoDesc成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody SpuInfoDescEntity spuInfoDesc){
		spuInfoDescService.updateById(spuInfoDesc);

        return CommonResult.success("修改spuInfoDesc成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] spuIds){
		spuInfoDescService.removeByIds(Arrays.asList(spuIds));

        return CommonResult.success("删除spuInfoDesc成功！");
    }

}
