package com.hl.product.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hl.product.entity.BrandEntity;
import com.hl.product.service.impl.BrandServiceImpl;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/01/27/16:26
 */
@RestController
@Slf4j
@RefreshScope//Nacos动态刷新配置
public class TestController {
    @Resource
    public BrandServiceImpl brandService;
//    @Value("${spring.cache.redis.time-to-live}")
//    private String configinfo;
    @GetMapping("/test1")
    public CommonResult getBarnd()
    {
        List<BrandEntity> brandEntities= brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        return new CommonResult<List<BrandEntity>>(HttpStatus.SC_OK, Constant.MY_SEARCH_SUCCESS,brandEntities);
    }
//    @GetMapping("/test2")
//    public CommonResult getConfig()
//    {
//        return new CommonResult(HttpStatus.SC_OK, Constant.MY_SEARCH_SUCCESS,configinfo);
//    }
}
