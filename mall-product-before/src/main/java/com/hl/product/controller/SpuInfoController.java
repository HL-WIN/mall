package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import com.hl.product.vo.SpuSaveVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.SpuInfoEntity;
import com.hl.product.service.SpuInfoService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * spu信息
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@RestController
@RequestMapping("product/spuinfo")
public class SpuInfoController {
    @Autowired
    private SpuInfoService spuInfoService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = spuInfoService.queryPage(params);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		SpuInfoEntity spuInfo = spuInfoService.getById(id);
        HashMap<String, SpuInfoEntity> hashMap=new HashMap<>();
        hashMap.put("spuInfo", spuInfo);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 新增商品
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody SpuSaveVo spuSaveVo){
//		spuInfoService.save(spuInfo);
        Boolean result = spuInfoService.saveSpuSaveVo(spuSaveVo);
        if (result) {
            return CommonResult.success("新增商品成功！");
        }
        else {
            return CommonResult.error("新增商品失败！");
        }
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody SpuInfoEntity spuInfo){
		spuInfoService.updateById(spuInfo);

        return CommonResult.success("修改spuInfo成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		spuInfoService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除spuInfo成功！");
    }

}
