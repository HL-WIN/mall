package com.hl.product.controller;

import java.util.*;
import java.util.Map;

import com.hl.product.entity.PurchaseEntity;
import com.hl.product.service.PurchaseService;
import com.hl.product.vo.MergeVo;
import com.hl.product.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 采购信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@RestController
@RequestMapping("product/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 完成采购单
     * @param
     * @return
     */
    @PostMapping(value = "/done")
    public CommonResult finish(@RequestBody PurchaseDoneVo purchaseOrderDoneVo) {

        purchaseService.done(purchaseOrderDoneVo);

        return CommonResult.success();
    }

    /**
     * 员工领取采购单
     * @param ids
     * @return
     */
    @PostMapping("/received")
    public CommonResult receive(Long[] ids)
    {
        purchaseService.receive(ids);
        return CommonResult.success();
    }

    /**
     * 合并采购需求
      * @param mergeVo
     * @return
     */
    @PostMapping("/merge")
    public CommonResult merge(@RequestBody MergeVo mergeVo){
        String result=purchaseService.merge(mergeVo);
        return CommonResult.success(result);
    }
    /**
     * 查询未领取的采购单
     * @param params
     * @return
     */
    @GetMapping("/unreceive/list")
    public CommonResult unReList(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPageUnReList(params);
        return new CommonResult(200,"查询未领取的采购单",page);
    }
    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);
        HashMap<String, PurchaseEntity> hashMap=new HashMap<>();
        hashMap.put("purchase", purchase);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody PurchaseEntity purchase){
        purchase.setUpdateTime(new Date());
        purchase.setCreateTime(new Date());
		purchaseService.save(purchase);
        return CommonResult.success("保存purchase成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return CommonResult.success("修改purchase成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除purchase成功！");
    }

}
