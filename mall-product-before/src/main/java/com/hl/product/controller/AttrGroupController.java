package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.hl.product.entity.AttrEntity;
import com.hl.product.service.AttrAttrgroupRelationService;
import com.hl.product.service.AttrService;
import com.hl.product.service.CategoryService;
import com.hl.product.vo.AttrGroupRelationVo;
import com.hl.product.vo.AttrGroupWithAttrsVo;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.AttrGroupEntity;
import com.hl.product.service.AttrGroupService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import javax.annotation.Resource;


/**
 * 属性分组
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:53
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Resource
    private AttrGroupService attrGroupService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private AttrService attrService;

    @Resource
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    /**
     * 获取属性分组的关联的所有属性
     */

    @GetMapping("/{attrgroupId}/attr/relation")
    public CommonResult getAttrRelation(@PathVariable("attrgroupId")Long attrGroupId)
    {
        List<AttrEntity> attrEntities=attrService.getAttrRelation(attrGroupId);
        return new CommonResult(200,"获取属性分组的关联的所有属性",attrEntities);
    }
    /**
     * 列表
     */
    @GetMapping("/list/{catelogId}")
    public CommonResult list(@RequestParam Map<String, Object> params,@PathVariable("catelogId") Long catelogId){
        PageUtils page = attrGroupService.queryPage(params,catelogId);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"分页信息",hashMap);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{attrGroupId}")
    public CommonResult info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long catelogId = attrGroup.getCatelogId();
        Long [] path =categoryService.findCatelogPath(catelogId);
        attrGroup.setCatelogPath(path);
        HashMap<String, AttrGroupEntity> hashMap=new HashMap<>();
        hashMap.put("attrGroup", attrGroup);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return CommonResult.success("保存attrGroup成功！");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return CommonResult.success("修改attrGroup成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return CommonResult.success("删除attrGroup成功！");
    }

    /**
     * 删除属性与分组的关联关系
     * @param attrGroupRelationVos
     * @return
     */
    @PostMapping("/attr/relation/delete")
    public CommonResult deleteRelation(@RequestBody AttrGroupRelationVo[] attrGroupRelationVos)
    {
        attrGroupService.deleteRelation(attrGroupRelationVos);
        return CommonResult.success("删除属性与分组的关联关系成功");
    }

    /**
     * 获取属性分组没有关联的其他属性
     * @param params
     * @return
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public CommonResult getNoattrRelation(@RequestParam Map<String, Object> params,
                                          @PathVariable("attrgroupId")Long attrGroupId)
    {
        PageUtils page=attrGroupService.getNoattrRelation(params,attrGroupId);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"获取属性分组没有关联的其他属性",hashMap);
    }

    /**
     * 添加属性与分组关联关系
     * @param attrGroupRelationVos
     * @return
     */
    @PostMapping("/attr/relation")
    public CommonResult addRelation(@RequestBody List<AttrGroupRelationVo> attrGroupRelationVos)
    {
        attrAttrgroupRelationService.saveBatch(attrGroupRelationVos);
        return CommonResult.success("添加属性与分组关联关系");
    }
//    /product/attrgroup/{catelogId}/withattr

    /**
     * 获取分类下所有分组&关联属性
     * @param catelogId
     * @return
     */
    @GetMapping("{catelogId}/withattr")
    public CommonResult getCatelogIdWithAttr(@PathVariable("catelogId")Long catelogId)
    {
        //查所有属性分组
        //查分组下所有属性
        List<AttrGroupWithAttrsVo> attrGroupWithAttrsVos=attrGroupService.getCatelogIdWithAttr(catelogId);
        return new CommonResult(200,"获取分类下所有分组&关联属性",attrGroupWithAttrsVos);
    }
}
