package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import com.hl.product.entity.WareSkuEntity;
import com.hl.product.service.WareSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品库存
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@RestController
@RequestMapping("product/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);
        HashMap<String, WareSkuEntity> hashMap=new HashMap<>();
        hashMap.put("wareSku", wareSku);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return CommonResult.success("保存wareSku成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return CommonResult.success("修改wareSku成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除wareSku成功！");
    }

}
