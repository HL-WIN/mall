package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hl.product.entity.CommentReplayEntity;
import com.hl.product.service.CommentReplayService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 商品评价回复关系
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@RestController
@RequestMapping("product/commentreplay")
public class CommentReplayController {
    @Autowired
    private CommentReplayService commentReplayService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = commentReplayService.queryPage(params);

        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CommentReplayEntity commentReplay = commentReplayService.getById(id);
        HashMap<String, CommentReplayEntity> hashMap=new HashMap<>();
        hashMap.put("commentReplay", commentReplay);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody CommentReplayEntity commentReplay){
		commentReplayService.save(commentReplay);

        return CommonResult.success("保存commentReplay成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody CommentReplayEntity commentReplay){
		commentReplayService.updateById(commentReplay);

        return CommonResult.success("修改commentReplay成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		commentReplayService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除commentReplay成功！");
    }

}
