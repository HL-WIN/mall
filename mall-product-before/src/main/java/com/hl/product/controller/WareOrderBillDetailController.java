package com.hl.product.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import com.hl.product.entity.WareOrderBillDetailEntity;
import com.hl.product.service.WareOrderBillDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 库存工作单
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@RestController
@RequestMapping("product/wareorderbilldetail")
public class WareOrderBillDetailController {
    @Autowired
    private WareOrderBillDetailService wareOrderBillDetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = wareOrderBillDetailService.queryPage(params);
        HashMap<String, PageUtils> hashMap=new HashMap<>();
        hashMap.put("page",page);
        return new CommonResult(200,"hashMap",hashMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		WareOrderBillDetailEntity wareOrderBillDetail = wareOrderBillDetailService.getById(id);
        HashMap<String, WareOrderBillDetailEntity> hashMap=new HashMap<>();
        hashMap.put("wareOrderBillDetail", wareOrderBillDetail);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody WareOrderBillDetailEntity wareOrderBillDetail){
		wareOrderBillDetailService.save(wareOrderBillDetail);

        return CommonResult.success("保存wareOrderBillDetail成功！");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody WareOrderBillDetailEntity wareOrderBillDetail){
		wareOrderBillDetailService.updateById(wareOrderBillDetail);

        return CommonResult.success("修改wareOrderBillDetail成功！");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		wareOrderBillDetailService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除wareOrderBillDetail成功！");
    }

}
