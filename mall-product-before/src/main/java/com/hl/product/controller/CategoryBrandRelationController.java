package com.hl.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hl.product.entity.BrandEntity;
import com.hl.product.vo.BrandVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hl.product.entity.CategoryBrandRelationEntity;
import com.hl.product.service.CategoryBrandRelationService;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;



/**
 * 品牌分类关联
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public CommonResult list(@RequestParam("brandId")Long brandId ){
        List<CategoryBrandRelationEntity> data = categoryBrandRelationService.list(
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id", brandId)
        );
        return new CommonResult(200,"查询品牌分类关联信息成功",data);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);
        HashMap<String, CategoryBrandRelationEntity> hashMap=new HashMap<>();
        hashMap.put("categoryBrandRelation", categoryBrandRelation);
        return new CommonResult(200,"信息",hashMap);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public CommonResult save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
        if (categoryBrandRelationService.save(categoryBrandRelationService.searchName(categoryBrandRelation))) {
            return CommonResult.success("保存categoryBrandRelation成功！");
        }
        else {
            return CommonResult.error("保存categoryBrandRelation失败！");
        }
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public CommonResult update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return CommonResult.success("修改categoryBrandRelation成功！");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return CommonResult.success("删除categoryBrandRelation成功！");
    }

//    /product/categorybrandrelation/brands/list

    /**
     *获取分类关联的品牌
     * @return
     */
    @GetMapping("/brands/list")
    public CommonResult brandsList(@RequestParam(value = "catId",required = true) Long catId)
    {
        List<BrandEntity> brandEntities= categoryBrandRelationService.getBrandsListByCatId(catId);
        List<Object> vos = brandEntities.stream().map(brandEntity -> {
            BrandVo brandVo = new BrandVo();
            brandVo.setBrandId(brandEntity.getBrandId());
            brandVo.setBrandName(brandEntity.getName());
            return brandVo;
        }).collect(Collectors.toList());
        return new CommonResult(200,"获取分类关联的品牌",vos);
    }
}
