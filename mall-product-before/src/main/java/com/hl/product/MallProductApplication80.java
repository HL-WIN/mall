package com.hl.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@SpringBootApplication
@EnableDiscoveryClient
//@EnableFeignClients
public class MallProductApplication80 {
    public static void main(String[] args) {
        SpringApplication.run(MallProductApplication80.class, args);
    }
}
