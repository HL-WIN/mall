package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.SkusVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SkuInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * sku信息
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean saveFromProductAdd(List<SkusVo> skus, Long brandId, Long catalogId, Long spuId);
}

