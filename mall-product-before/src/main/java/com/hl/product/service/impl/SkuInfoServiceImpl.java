package com.hl.product.service.impl;

import com.hl.product.entity.SkuImagesEntity;
import com.hl.product.entity.SkuSaleAttrValueEntity;
import com.hl.product.service.SkuImagesService;
import com.hl.product.service.SkuSaleAttrValueService;
import com.hl.product.vo.ImagesVo;
import com.hl.product.vo.SkusVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import com.hl.product.dao.SkuInfoDao;
import com.hl.product.entity.SkuInfoEntity;
import com.hl.product.service.SkuInfoService;
import com.mall.commons.utils.Query;

import javax.annotation.Resource;

@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Resource
    SkuImagesService skuImagesService;
    @Resource
    SkuSaleAttrValueService skuSaleAttrValueService;

    /**
     * key: 检索关键字
     * catelogId: 0,
     * brandId: 0,
     * min: 0,
     * max: 0
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SkuInfoEntity> skuInfoEntityQueryWrapper = new QueryWrapper<>();

        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key))
        {
            skuInfoEntityQueryWrapper.and(w->{
                w.eq("sku_id",key).or().like("sku_name",key);
            });
        }
        String catelogId= (String) params.get("catelogId");
        if (!StringUtils.isEmpty(catelogId)&& !"0".equalsIgnoreCase(catelogId))
        {
            skuInfoEntityQueryWrapper.eq("catelog_id",catelogId);
        }
        String brandId= (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(brandId))
        {
            skuInfoEntityQueryWrapper.eq("brand_id",brandId);
        }
        String min= (String) params.get("min");
        if (!StringUtils.isEmpty(min))
        {
            skuInfoEntityQueryWrapper.ge("price",min);
        }
        String max= (String) params.get("max");
        if (!StringUtils.isEmpty(max))
        {
            try {
                BigDecimal bigDecimal = new BigDecimal(max);
                if (bigDecimal.compareTo(new BigDecimal("0"))==1) {
                    skuInfoEntityQueryWrapper.le("price",max);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                skuInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public boolean saveFromProductAdd(List<SkusVo> skus, Long brandId, Long catalogId, Long spuId) {
        boolean result=false;
        if (skus.size()!=0)
        {
            skus.forEach(skusVo -> {
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(skusVo,skuInfoEntity);
                skuInfoEntity.setBrandId(brandId);
                skuInfoEntity.setCatalogId(catalogId);
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setSpuId(spuId);
                for (ImagesVo images:skusVo.getImages()) {
                    if (images.getDefaultImg()==1)
                    {
                        skuInfoEntity.setSkuDefaultImg(images.getImgUrl());
                    }
                }
                this.save(skuInfoEntity);
                //保存sku图片信息
                List<SkuImagesEntity> collect = skusVo.getImages().stream().map(imagesVo -> {
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                    skuImagesEntity.setSkuId(skuInfoEntity.getSkuId());
                    skuImagesEntity.setImgUrl(imagesVo.getImgUrl());
                    skuImagesEntity.setDefaultImg(imagesVo.getDefaultImg());
                    return skuImagesEntity;
                }).collect(Collectors.toList());
                skuImagesService.saveBatch(collect);
                //保存sku销售信息
                List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = skusVo.getAttr().stream().map(attrVo -> {
                    SkuSaleAttrValueEntity skuSaleAttrValueEntity = new SkuSaleAttrValueEntity();
                    BeanUtils.copyProperties(attrVo, skuSaleAttrValueEntity);
                    skuSaleAttrValueEntity.setSkuId(skuInfoEntity.getSkuId());
                    return skuSaleAttrValueEntity;
                }).collect(Collectors.toList());
                skuSaleAttrValueService.saveBatch(skuSaleAttrValueEntities);
            });

        }
        return true;
    }

}