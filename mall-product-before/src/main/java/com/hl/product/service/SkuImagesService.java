package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SkuImagesEntity;

import java.util.Map;

/**
 * sku图片
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

