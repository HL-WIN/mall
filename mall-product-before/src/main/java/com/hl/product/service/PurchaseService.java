package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.PurchaseEntity;
import com.hl.product.vo.MergeVo;
import com.hl.product.vo.PurchaseDoneVo;
import com.mall.commons.utils.PageUtils;


import java.util.Map;

/**
 * 采购信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnReList(Map<String, Object> params);

    String merge(MergeVo params);

    void receive(Long[] ids);

    void done(PurchaseDoneVo doneVo);
}

