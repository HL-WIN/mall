package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.WareOrderBillEntity;
import com.mall.commons.utils.PageUtils;


import java.util.Map;

/**
 * 库存工作单
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
public interface WareOrderBillService extends IService<WareOrderBillEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

