package com.hl.product.service.impl;

import com.hl.product.dao.WareDao;
import com.hl.product.entity.WareEntity;
import com.hl.product.service.WareService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;



@Service("wareService")
public class WareServiceImpl extends ServiceImpl<WareDao, WareEntity> implements WareService {


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareEntity> queryWrapper=new QueryWrapper<WareEntity>();
        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.eq("id",key).or().like("name",key)
                    .or().like("address",key)
                    .or().like("areacode",key);
        }
        IPage<WareEntity> page = this.page(
                new Query<WareEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}