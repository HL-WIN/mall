package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.AttrRespVo;
import com.hl.product.vo.AttrVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBasePage(Map<String, Object> params, Long categoryId, String type);

    AttrRespVo getInfoVo(Long attrId);

    void updateAttrVo(AttrVo attr);

    List<AttrEntity> getAttrRelation(Long attrGroupId);
}

