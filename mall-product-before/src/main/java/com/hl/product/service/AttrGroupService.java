package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.AttrGroupRelationVo;
import com.hl.product.vo.AttrGroupWithAttrsVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.AttrGroupEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:53
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long categoryId);

    void deleteRelation(AttrGroupRelationVo[] attrGroupRelationVos);

    PageUtils getNoattrRelation(Map<String, Object> params, Long attrGroupId);

    List<AttrGroupWithAttrsVo> getCatelogIdWithAttr(Long catelogId);
}

