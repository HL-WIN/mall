package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.WareSkuEntity;
import com.mall.commons.utils.PageUtils;


import java.util.Map;

/**
 * 商品库存
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);
}

