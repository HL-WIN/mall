package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.PurchaseDetailEntity;
import com.mall.commons.utils.PageUtils;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<PurchaseDetailEntity> getDetailPurchase(Long purchaseEntity);
}

