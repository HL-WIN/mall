package com.hl.product.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.commons.utils.PageUtils;

import com.hl.product.dao.SpuImagesDao;
import com.hl.product.entity.SpuImagesEntity;
import com.hl.product.service.SpuImagesService;
import com.mall.commons.utils.Query;

@Service("spuImagesService")
public class SpuImagesServiceImpl extends ServiceImpl<SpuImagesDao, SpuImagesEntity> implements SpuImagesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuImagesEntity> page = this.page(
                new Query<SpuImagesEntity>().getPage(params),
                new QueryWrapper<SpuImagesEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public boolean saveFromProductAdd(List<String> images, Long spuId) {
        if (images.size() == 0)
        {
            return true;
        }else
        {
            List<SpuImagesEntity> spuImagesEntities = images.stream().map(image -> {
                SpuImagesEntity spuImagesEntity = new SpuImagesEntity();
                spuImagesEntity.setSpuId(spuId);
                spuImagesEntity.setImgUrl(image);
                return spuImagesEntity;
            }).filter(spuImagesEntity -> {
                //返回fasle则剔除该项
                return !StringUtils.isEmpty(spuImagesEntity.getImgUrl());
            }).collect(Collectors.toList());
            return this.saveBatch(spuImagesEntities);
        }
    }

}