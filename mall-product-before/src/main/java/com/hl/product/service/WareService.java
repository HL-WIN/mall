package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.WareEntity;
import com.mall.commons.utils.PageUtils;


import java.util.Map;

/**
 * 仓库信息
 *
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
public interface WareService extends IService<WareEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

