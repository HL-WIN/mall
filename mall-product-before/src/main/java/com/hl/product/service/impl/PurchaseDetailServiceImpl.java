package com.hl.product.service.impl;

import com.hl.product.dao.PurchaseDetailDao;
import com.hl.product.entity.PurchaseDetailEntity;
import com.hl.product.service.PurchaseDetailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mall.commons.utils.Query;
import com.mall.commons.utils.PageUtils;



@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<PurchaseDetailEntity> purchaseDetailEntityQueryWrapper = new QueryWrapper<>();
        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {

            purchaseDetailEntityQueryWrapper.and(purchaseDetailEntityQueryWrapper1 -> {
                purchaseDetailEntityQueryWrapper1.eq("purchase_id",key).or().eq("sku_id",key);
            });
        }
        String status= (String) params.get("status");
        if (!StringUtils.isEmpty(status)) {

            purchaseDetailEntityQueryWrapper.eq("status",status);
        }
        String wareId= (String) params.get("wareId");
        if (!StringUtils.isEmpty(status)) {

            purchaseDetailEntityQueryWrapper.eq("ware_id",wareId);
        }
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                purchaseDetailEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<PurchaseDetailEntity> getDetailPurchase(Long id) {
        return this.list(new QueryWrapper<PurchaseDetailEntity>().eq("purchase_id", id));
    }

}