package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.entity.BrandEntity;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //查出分类名和品牌名再返回，避免过多的关联查询
    CategoryBrandRelationEntity searchName(CategoryBrandRelationEntity categoryBrandRelation);

    void updateFromBrand(Long brandId, String name);

    void updateFromCategory(Long catId, String name);

    List<BrandEntity> getBrandsListByCatId(Long catId);
}

