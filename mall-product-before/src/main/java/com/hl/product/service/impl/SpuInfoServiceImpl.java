package com.hl.product.service.impl;

import com.hl.product.entity.SpuImagesEntity;
import com.hl.product.entity.SpuInfoDescEntity;
import com.hl.product.service.*;
import com.hl.product.vo.SpuSaveVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.commons.utils.PageUtils;

import com.hl.product.dao.SpuInfoDao;
import com.hl.product.entity.SpuInfoEntity;
import com.mall.commons.utils.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Resource
    SpuInfoDescService spuInfoDescService;

    @Resource
    SpuImagesService spuImagesService;

    @Resource
    ProductAttrValueService productAttrValueService;

    @Resource
    SkuInfoService skuInfoService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> spuInfoEntityQueryWrapper = new QueryWrapper<>();

        String key= (String) params.get("key");
        if (!StringUtils.isEmpty(key))
        {
            spuInfoEntityQueryWrapper.and(w->{
               w.eq("id",key).or().like("spu_name",key);
            });
        }
        String catelogId= (String) params.get("catelogId");
        if (!StringUtils.isEmpty(catelogId)&&!"0".equalsIgnoreCase(catelogId))
        {
            spuInfoEntityQueryWrapper.eq("catelog_id",catelogId);
        }
        String brandId= (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(brandId))
        {
            spuInfoEntityQueryWrapper.eq("brand_id",brandId);
        }
        String status= (String) params.get("status");
        if (!StringUtils.isEmpty(status))
        {
            spuInfoEntityQueryWrapper.eq("publish_status",status);
        }
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                spuInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveSpuSaveVo(SpuSaveVo spuSaveVo) {
        boolean result=false;
        boolean saveInfoDesc=false;
        boolean saveImage=false;
        boolean saveBaseInfo=false;
        boolean saveSku=false;
        //保存spu基本信息
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuSaveVo,spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        boolean save = this.save(spuInfoEntity);

        Long spuId=spuInfoEntity.getId();
        //保存spu图片，前提是保存成功
        if (save)
        {
            saveInfoDesc=spuInfoDescService.saveFromProductAdd(spuSaveVo.getDecript(),spuId);
        }
        //保存图片
        if (saveInfoDesc)
        {
            saveImage=spuImagesService.saveFromProductAdd(spuSaveVo.getImages(),spuId);
        }
        //保存规格参数
        if (saveImage)
        {
            saveBaseInfo=productAttrValueService.saveFromProductAdd(spuSaveVo.getBaseAttrs(),spuId);
        }
        //保存sku
        if (saveBaseInfo)
        {
            saveSku=skuInfoService.saveFromProductAdd(spuSaveVo.getSkus(),spuSaveVo.getBrandId(),spuSaveVo.getCatalogId(),spuId);
        }
        if (saveSku)
        {
            result=true;
        }
        return result;
    }
}