package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

