package com.hl.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hl.product.vo.SpuSaveVo;
import com.mall.commons.utils.PageUtils;
import com.hl.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Boolean saveSpuSaveVo(SpuSaveVo spuSaveVo);
}

