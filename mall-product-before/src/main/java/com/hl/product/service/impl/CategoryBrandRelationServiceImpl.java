package com.hl.product.service.impl;

import com.hl.product.dao.BrandDao;
import com.hl.product.dao.CategoryDao;
import com.hl.product.entity.BrandEntity;
import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.BrandService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import com.hl.product.dao.CategoryBrandRelationDao;
import com.hl.product.entity.CategoryBrandRelationEntity;
import com.hl.product.service.CategoryBrandRelationService;
import com.mall.commons.utils.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {

    @Resource
    BrandDao brandDao;
    @Resource
    CategoryDao categoryDao;
    @Resource
    CategoryBrandRelationDao categoryBrandRelationDao;
    @Resource
    BrandService brandService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public CategoryBrandRelationEntity searchName(CategoryBrandRelationEntity categoryBrandRelation) {
        Long brandId = categoryBrandRelation.getBrandId();
        Long catelogId = categoryBrandRelation.getCatelogId();
        BrandEntity brandEntity = brandDao.selectById(brandId);
        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        categoryBrandRelation.setBrandName(brandEntity.getName());
        categoryBrandRelation.setCatelogName(categoryEntity.getName());
        return categoryBrandRelation;
    }

    @Override
    public void updateFromBrand(Long brandId, String name) {
        CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
        categoryBrandRelationEntity.setBrandId(brandId);
        categoryBrandRelationEntity.setBrandName(name);
        this.update(categoryBrandRelationEntity,
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id",brandId));
    }

    //级联更新
    @Override
    public void updateFromCategory(Long catId, String name) {
        this.baseMapper.updateAllCategory(catId,name);
    }

    @Override
    public List<BrandEntity> getBrandsListByCatId(Long catId) {
        List<BrandEntity> brandEntities = null;
        List<CategoryBrandRelationEntity> categoryBrandRelationEntities = categoryBrandRelationDao.selectList(new QueryWrapper<CategoryBrandRelationEntity>().eq("catelog_id", catId));
        brandEntities = categoryBrandRelationEntities.stream().map(categoryBrandRelationEntity -> {
            Long brandId=categoryBrandRelationEntity.getBrandId();
            BrandEntity brandEntity=brandService.getById(brandId);
            return brandEntity;
        }).collect(Collectors.toList());
        return brandEntities;
    }

}