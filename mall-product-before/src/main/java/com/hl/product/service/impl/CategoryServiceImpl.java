package com.hl.product.service.impl;

import com.hl.product.service.CategoryBrandRelationService;
import lombok.experimental.var;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.commons.utils.CommonResult;
import com.mall.commons.utils.PageUtils;

import com.hl.product.dao.CategoryDao;
import com.hl.product.entity.CategoryEntity;
import com.hl.product.service.CategoryService;
import com.mall.commons.utils.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static java.awt.SystemColor.menu;

@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {
    @Resource
    CategoryBrandRelationService categoryBrandRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //查询所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        //组装成父子结构
            //1级别分类
        List<CategoryEntity> level1Category = entities.stream().filter(categoryEntity -> categoryEntity.getParentCid() == 0)
                .map((menu)->{menu.setListChildrenCategory(getListChildren(menu,entities));
                return menu;}).sorted((menu1,menu2)->{
                    return (menu1.getSort()==null?0:menu1.getSort())-(menu2.getSort()==null?0:menu2.getSort());
                }).collect(Collectors.toList());
        return level1Category;
    }

    @Override
    public void removeMenusByIds(List<Long> idList) {
        //TODO 检测引用
        baseMapper.deleteBatchIds(idList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> objects = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId,objects);
        Collections.reverse(parentPath);
        return  parentPath.toArray(new Long[parentPath.size()]);
    }
    @Transactional
    @Override
    public void updateRelation(CategoryEntity category) {
        //先更新自己
        this.updateById(category);
        if (!StringUtils.isEmpty(category.getName())) {
            //如果有更新名字,同步更新
            categoryBrandRelationService.updateFromCategory(category.getCatId(),category.getName());
        }
    }

    private List<Long> findParentPath(Long catelogId, List<Long> objects) {
        objects.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid()!=0)
        {
            findParentPath(byId.getParentCid(),objects);
        }
        return objects;
    }

    //递归查找所有分类的子分类
    private List<CategoryEntity> getListChildren(CategoryEntity menu, List<CategoryEntity> entities) {
        List<CategoryEntity> childrenCategory = entities.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(menu.getCatId());
        }).map(categoryEntity -> {
            //找子菜单
            categoryEntity.setListChildrenCategory(getListChildren(categoryEntity,entities));
            return categoryEntity;
        }).sorted((menu1,menu2)->{
            return (menu1.getSort()==null?0:menu1.getSort())-(menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());

        return childrenCategory;
    }

}