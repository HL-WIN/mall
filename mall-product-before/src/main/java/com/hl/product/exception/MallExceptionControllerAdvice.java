package com.hl.product.exception;

import com.mall.commons.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLSyntaxErrorException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/09/21:21
 */
@Slf4j
//@ResponseBody
//@ControllerAdvice(basePackages = "com.hl.product.controller")//统一处理异常
@RestControllerAdvice(basePackages = "com.hl.product.controller")
public class MallExceptionControllerAdvice {
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult handlerValidException(MethodArgumentNotValidException e)
    {
//        log.error(e.getMessage());
        BindingResult bindingResult = e.getBindingResult();
        Map<String ,String> map=new HashMap<>();
        bindingResult.getFieldErrors().forEach((item)->{
            String defaultMessage = item.getDefaultMessage();
            String field = item.getField();
            map.put(field,defaultMessage);
        });
        return CommonResult.error("提交数据不合法",map);
    }

    @ExceptionHandler(value = BadSqlGrammarException.class)
    public CommonResult handlerExceptionSql(BadSqlGrammarException e)
    {
        return CommonResult.error("RestControllerAdvice:数据库BadSqlGrammarException");
    }

    @ExceptionHandler(value = Throwable.class)
    public CommonResult handlerValidExceptionAll(Throwable e)
    {
        return CommonResult.error("RestControllerAdvice:handlerValidExceptionAll##"+e.getCause()+e.getClass());
    }
}
