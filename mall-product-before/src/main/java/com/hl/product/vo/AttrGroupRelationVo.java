package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/13/19:29
 */
@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
