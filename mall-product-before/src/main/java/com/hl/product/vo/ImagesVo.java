package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/15/15:40
 */
@Data
public class ImagesVo {
    private String imgUrl;
    private int defaultImg;
}
