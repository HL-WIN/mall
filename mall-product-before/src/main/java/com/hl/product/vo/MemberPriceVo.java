package com.hl.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/15/15:40
 */
@Data
public class MemberPriceVo {

    private Long id;
    private String name;
    private BigDecimal price;
}
