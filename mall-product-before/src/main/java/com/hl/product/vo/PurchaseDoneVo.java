package com.hl.product.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/16/18:47
 */
@Data
public class PurchaseDoneVo {
    @NotNull(message = "id不允许为空")
    private Long id;

    private List<PurchaseItemDoneVo> items;
}
