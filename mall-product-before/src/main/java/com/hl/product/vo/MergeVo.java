package com.hl.product.vo;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/16/17:11
 */
@Data
public class MergeVo {
    private Long purchaseId;

    private List<Long> items;
}
