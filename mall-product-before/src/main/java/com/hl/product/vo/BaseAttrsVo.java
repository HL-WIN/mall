package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/15/15:36
 */
@Data
public class BaseAttrsVo {
    private Long attrId;
    private String attrValues;
    private int showDesc;
}
