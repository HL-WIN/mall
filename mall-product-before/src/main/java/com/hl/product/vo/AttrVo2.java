package com.hl.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: hl
 * @Date: 2022/02/15/15:35
 */
@Data
public class AttrVo2 {
    private Long attrId;
    private String attrName;
    private String attrValue;
}
