package com.hl.product.dao;

import com.hl.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
