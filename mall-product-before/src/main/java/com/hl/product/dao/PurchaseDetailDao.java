package com.hl.product.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.entity.PurchaseDetailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
