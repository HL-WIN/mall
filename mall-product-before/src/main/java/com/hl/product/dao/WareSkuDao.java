package com.hl.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.entity.WareSkuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    void addStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);
}
