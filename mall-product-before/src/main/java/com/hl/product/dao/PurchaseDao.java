package com.hl.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.entity.PurchaseEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
