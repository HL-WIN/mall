package com.hl.product.dao;

import com.hl.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
