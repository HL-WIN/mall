package com.hl.product.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.entity.WareEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@Mapper
public interface WareDao extends BaseMapper<WareEntity> {
	
}
