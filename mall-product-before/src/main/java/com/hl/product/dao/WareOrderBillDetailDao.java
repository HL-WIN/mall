package com.hl.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hl.product.entity.WareOrderBillDetailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author hl
 * @email ${email}
 * @date 2022-02-16 11:56:09
 */
@Mapper
public interface WareOrderBillDetailDao extends BaseMapper<WareOrderBillDetailEntity> {
	
}
