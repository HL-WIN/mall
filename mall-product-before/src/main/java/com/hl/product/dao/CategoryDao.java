package com.hl.product.dao;

import com.hl.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:53
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
