package com.hl.product.dao;

import com.hl.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author hl
 * @email ""
 * @date 2022-01-27 15:35:54
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    void updateAllCategory(@Param("catId") Long catId,@Param("name") String name);
}
